<?php
// @codingStandardsIgnoreFile

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Connection;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'user', function (Blueprint $table) {
                $table->increments('id');
                $table->string('username', 45)->unique();
                $table->string('email', 255)->unique();
                $table->string('name', 45);
                $table->string('lastname', 45);
                $table->enum('role', ['administrador','editor','usuario', 'inativo']);
                $table->string('password', 255);
                $table->rememberToken();
                $table->timestamps();
            }
        );

        DB::table('user')->insert([
            'username' => 'ricardo', 
            'email' => 'ricardo@codezone.com.br', 
            'name' => 'Ricardo',
            'lastname' => 'Moraleida',
            'role' => 'administrador',
            'password' => bcrypt('123456')
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
