<?php
// @codingStandardsIgnoreFile

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('campaign', 50);
            $table->longtext('extra_emails');
            $table->dateTime('processed');
            $table->string('mailchimp_id', 100)->nullable();
            $table->string('mailchimp_web_id', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('list', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('list', 50);
            $table->enum('rules', ['todas','qualquer']);
            $table->longtext('filters');
            $table->string('mailchimp_id', 100)->nullable();
            $table->timestamps();
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('user');
        });

        Schema::create('segment', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('segment', 50);
            $table->enum('rules', ['todas','qualquer']);
            $table->longtext('filters');
            $table->string('mailchimp_id', 100)->nullable();
            $table->timestamps();
            $table->integer('id_list')->unsigned();
            $table->foreign('id_list')->references('id')->on('list');
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('user');
        });

        Schema::create('campaign_has_list', function(Blueprint $table)
        {
            $table->integer('id_campaign')->unsigned();
            $table->foreign('id_campaign')->references('id')->on('campaign');
            $table->integer('id_list')->unsigned();
            $table->foreign('id_list')->references('id')->on('list');
        });

        Schema::create('campaign_has_segment', function(Blueprint $table)
        {
            $table->integer('id_campaign')->unsigned();
            $table->foreign('id_campaign')->references('id')->on('campaign');
            $table->integer('id_segment')->unsigned();
            $table->foreign('id_segment')->references('id')->on('segment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('campaign');
        Schema::dropIfExists('list');
        Schema::dropIfExists('segment');
        Schema::dropIfExists('campaign_has_list');
        Schema::dropIfExists('campaign_has_segment');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
