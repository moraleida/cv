<?php
// @codingStandardsIgnoreFile
/**
 * Create Client and related tables
 */
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Connection;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /*
         * Foreign Keys em CLIENT (tabela principal)
         */
        Schema::create('origin', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('description', 45);
        });

        DB::table('origin')->insert([
            'name' => 'Cadastro Site',
            'description' => 'Formulário de cadastro preenchido no site',
            ]);

        Schema::create('entry_method', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('description', 45);
        });

        DB::table('entry_method')->insert([
            'name' => 'Manual',
            'description' => 'Formulário de cadastro preenchido no site',
            ]);


        Schema::create('carreer', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 100);
        });

        Schema::create('company', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 100);
        });

        Schema::create('department', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('description', 255);
        });

        Schema::create('function', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('description', 255);
            $table->integer('id_department')->unsigned()->nullable();
            $table->foreign('id_department')->references('id')->on('department');
        });

        Schema::create('email_domain', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('domain', 50);
            $table->enum('type', ['pessoal','comercial']);
        });

        Schema::create('interest_area', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('description', 45);
        });

        Schema::create('interest_group', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('description', 45);
        });

        Schema::create('social_network', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 50);
        });

        /*
         * Address-related tables
         */
        Schema::create('country', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('safe', 50);
            $table->string('country', 50);
        });

        Schema::create('state', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('safe', 50);
            $table->string('state', 50);
            $table->integer('id_country')->unsigned();
            $table->foreign('id_country')->references('id')->on('country');
        });

        Schema::create('city', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('city', 100);
            $table->integer('id_state')->unsigned();
            $table->foreign('id_state')->references('id')->on('state');
        });

        Schema::create('neighborhood', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('neighborhood', 255);
            $table->integer('id_city')->unsigned();
            $table->foreign('id_city')->references('id')->on('city');
        });

        Schema::create('street', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('street', 255);
            $table->integer('id_neighborhood')->unsigned();
            $table->foreign('id_neighborhood')->references('id')->on('neighborhood');
        });


        Schema::create('intl_state', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('state', 50);
            $table->integer('id_country')->unsigned();
            $table->foreign('id_country')->references('id')->on('country');
        });

        Schema::create('intl_city', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('city', 50);
            $table->integer('id_country')->unsigned();
            $table->foreign('id_country')->references('id')->on('country');
        });

        /*
         * Table CLIENT
         */
        Schema::create('client', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('middlename', 50)->nullable();
            $table->string('lastname', 50);
            $table->date('birthday')->nullable();
            $table->enum('type', ['nao-associado','patrimonial',
                                  'cooperador','coletivo',
                                  'entidade','emerito',
                                  'individual'])->default('nao-associado');
            $table->string('associate_code', 45)->nullable(); // codigo_socio
            $table->string('identification'); // documento identidade
            $table->enum('identification_type', ['rg','cpf','cnh','passaporte','social-security','outro']); // codigo_socio
            $table->enum('language', ['pt','en'])->default('pt'); 
            $table->boolean('foreigner')->default(false);
            $table->string('password', 255)->nullable(); // documento identidade
            $table->string('image', 255); // foto
            $table->boolean('vip')->default(false);
            
            // Foreign keys
            $table->integer('id_entry_method')->unsigned();
            $table->integer('id_origin')->unsigned();
            $table->integer('id_client')->unsigned()->nullable();
            $table->foreign('id_entry_method')->references('id')->on('entry_method');
            $table->foreign('id_origin')->references('id')->on('origin');
            $table->foreign('id_client')->references('id')->on('client');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('client_history', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->longtext('general')->nullable();
            $table->longtext('address')->nullable();
            $table->longtext('work')->nullable();
            $table->longtext('interests')->nullable();

            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->timestamps();
        });

        Schema::create('email', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('email', 50);
            $table->enum('type', ['pessoal','comercial']);
            $table->string('mailchimp_id', 100)->nullable();
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
        });

        Schema::create('phone', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('country_code', 5);
            $table->string('area_code', 3)->nullable();
            $table->string('phone', 20);
            $table->string('extension', 12)->nullable();
            $table->enum('type', ['fixo','celular']);
            $table->enum('relation', ['pessoal','comercial']);
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
        });

        Schema::create('log', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('action', 50);
            $table->string('data', 50);
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->timestamps();
        });

        Schema::create('address', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('number', 10);
            $table->string('complement', 100);
            $table->string('zip', 8);
            $table->enum('type', ['residencial','comercial']);
            
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->integer('id_street')->unsigned();
            $table->integer('id_neighborhood')->unsigned()->nullable();
            $table->integer('id_city')->unsigned()->nullable();
            $table->integer('id_state')->unsigned()->nullable();
            $table->integer('id_country')->unsigned()->nullable();

            $table->foreign('id_street')->references('id')->on('street');
            $table->foreign('id_neighborhood')->references('id')->on('neighborhood');
            $table->foreign('id_city')->references('id')->on('city');
            $table->foreign('id_state')->references('id')->on('state');
            $table->foreign('id_country')->references('id')->on('country');
        });

        Schema::create('intl_address', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('street', 255);
            $table->string('complement', 255);
            $table->string('zip', 15);
            $table->enum('type', ['residencial','comercial']);

            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->integer('id_intl_city')->unsigned();
            $table->foreign('id_intl_city')->references('id')->on('intl_city');
            $table->integer('id_intl_state')->unsigned()->nullable();
            $table->foreign('id_intl_state')->references('id')->on('intl_state');
            $table->integer('id_country')->unsigned();
            $table->foreign('id_country')->references('id')->on('country');
        });

        /*
         * Relation-tables
         */
        Schema::create('intl_state_has_intl_city', function(Blueprint $table) 
        {
            $table->integer('id_intl_state')->unsigned();
            $table->foreign('id_intl_state')->references('id')->on('intl_state');
            $table->integer('id_intl_city')->unsigned();
            $table->foreign('id_intl_city')->references('id')->on('intl_city');
        });

        Schema::create('client_has_function', function(Blueprint $table) 
        {
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->integer('id_function')->unsigned();
            $table->foreign('id_function')->references('id')->on('function');
            $table->integer('id_company')->unsigned();
            $table->foreign('id_company')->references('id')->on('company');
        });

        Schema::create('client_has_carreer', function(Blueprint $table) 
        {
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->integer('id_carreer')->unsigned();
            $table->foreign('id_carreer')->references('id')->on('carreer');
        });

        Schema::create('client_has_company', function(Blueprint $table) 
        {
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->integer('id_company')->unsigned();
            $table->foreign('id_company')->references('id')->on('company');
        });

        Schema::create('client_has_interest_area', function(Blueprint $table) 
        {
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->integer('id_interest_area')->unsigned();
            $table->foreign('id_interest_area')->references('id')->on('interest_area');
        });

        Schema::create('client_has_interest_group', function(Blueprint $table) 
        {
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->integer('id_interest_group')->unsigned();
            $table->foreign('id_interest_group')->references('id')->on('interest_group');
        });

        Schema::create('client_has_social_network', function(Blueprint $table) 
        {
            $table->string('social_network_code', 50);
            $table->integer('id_client')->unsigned();
            $table->foreign('id_client')->references('id')->on('client');
            $table->integer('id_social_network')->unsigned();
            $table->foreign('id_social_network')->references('id')->on('social_network');
        });

        Schema::create('interest_area_has_interest_group', function(Blueprint $table) 
        {
            $table->integer('id_interest_area')->unsigned();
            $table->foreign('id_interest_area')->references('id')->on('interest_area');
            $table->integer('id_interest_group')->unsigned();
            $table->foreign('id_interest_group')->references('id')->on('interest_group');
        });


        // Preenche tabela de formacoes
        $interest = explode("\n", file_get_contents('public/codes/formacoes.txt'));
        foreach($interest as $int) {
            if(!empty($int))
                $formacoes[] = array('name' => $int);
        }
        DB::table('carreer')->insert($formacoes);

        // Preenche tabela de cargos
        $interest = explode("\n", file_get_contents('public/codes/cargos.txt'));
        foreach($interest as $int) {
            if(!empty($int))
                $cargos[] = array('name' => $int);
        }
        DB::table('function')->insert($cargos);

        // Preenche tabela de departamentos
        $interest = explode("\n", file_get_contents('public/codes/departamentos.txt'));
        foreach($interest as $int) {
            if(!empty($int))
                $departamentos[] = array('name' => $int);
        }
        DB::table('department')->insert($departamentos);

        // Preenche tabela de empresas
        $interest = explode("\n", file_get_contents('public/codes/empresas.txt'));
        foreach($interest as $int) {
            if(!empty($int))
                $empresas[] = array('name' => $int);
        }
        DB::table('company')->insert($empresas);

        // Preenche tabela de Interest Groups
        $interest = explode("\n", file_get_contents('public/codes/interest_groups.txt'));
        foreach($interest as $int) {
            if(!empty($int))
                $interest_groups[] = array('name' => $int);
        }
        DB::table('interest_group')->insert($interest_groups);

        // Preenche tabela de Países
        $countries = json_decode(file_get_contents('public/codes/countries.txt'), true);
        $cc = DB::table('country')->insert($countries);
        //
        // Preenche tabelas de Estados
        $country_code = '';
        $country_id = '';
        $states = [];
        $estados = [];
        foreach(file('public/codes/sub.csv') as $file) {
            $line = explode(',', $file);
            $line = str_replace('"','',$line);

            if($line[0] != $country_code) {
                $country = DB::table('country')->where('safe', $line[0])->first();

                if(!$country)
                    continue;

                $country_code = $line[0];
                $country_id = $country->id;
            }
            if($country_id == 1) {
                $estados[] = ['state' => $line[2], 'safe' => $line[1], 'id_country' => $country_id];
            } else {
                $states[] = ['state' => $line[2], 'id_country' => $country_id];
            }

            if(count($states) > 100) {
                DB::table('state')->insert($estados);
                DB::table('intl_state')->insert($states);
                $estados = array();
                $states = array();
            }
        }

        if(!empty($estados))
            DB::table('state')->insert($estados);
        if(!empty($states))
            DB::table('intl_state')->insert($states);
        //
        // Preenche tabelas de Cidades
        $country_code = '';
        $country_id = '';
        $state_id = '';
        $cidades = [];
        $cities = [];
        $files = ['p1.csv', 'p2.csv', 'p3.csv'];

        foreach($files as $fname) {
            foreach(file('public/codes/'.$fname) as $file) {
                $line = explode(',', $file);
                $line = str_replace('"','',$line);

                if(empty($line[2]))
                    continue;

                if($line[1] != $country_code) {
                    $country = DB::table('country')->where('safe', $line[1])->first();

                    if(!$country)
                        continue;

                    $country_code = $line[1];
                    $country_id = $country->id;
                }

                if($country_id == 1) {
                    var_dump($line[5]);
                    $state = false;
                    $state = DB::table('state')->where('safe', $line[5])->first();
                    if($state)
                        $cidades[] = ['city' => $line[3], 'id_state' => $state->id];
                } else {
                    $cities[] = ['city' => $line[3], 'id_country' => $country_id];
                }

                if(count($cities) > 100) {
                    DB::table('intl_city')->insert($cities);
                    DB::table('city')->insert($cidades);
                    $cities = array();
                    $cidades = array();
                }
            }
        }

        if(!empty($cities))
            DB::table('intl_city')->insert($cities);
        if(!empty($cidades))
            DB::table('city')->insert($cidades);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('origin');
        Schema::dropIfExists('entry_method');
        Schema::dropIfExists('client');
        Schema::dropIfExists('client_history');
        Schema::dropIfExists('email');
        Schema::dropIfExists('phone');
        Schema::dropIfExists('log');
        Schema::dropIfExists('carreer');
        Schema::dropIfExists('company');
        Schema::dropIfExists('department');
        Schema::dropIfExists('function');
        Schema::dropIfExists('email_domain');
        Schema::dropIfExists('interest_area');
        Schema::dropIfExists('interest_group');
        Schema::dropIfExists('social_network');
        Schema::dropIfExists('country');
        Schema::dropIfExists('state');
        Schema::dropIfExists('city');
        Schema::dropIfExists('neighborhood');
        Schema::dropIfExists('street');
        Schema::dropIfExists('address');
        Schema::dropIfExists('intl_state');
        Schema::dropIfExists('intl_city');
        Schema::dropIfExists('intl_address');
        Schema::dropIfExists('intl_state_has_intl_city');
        Schema::dropIfExists('client_has_function');
        Schema::dropIfExists('client_has_carreer');
        Schema::dropIfExists('client_has_company');
        Schema::dropIfExists('client_has_interest_area');
        Schema::dropIfExists('client_has_interest_group');
        Schema::dropIfExists('client_has_social_network');
        Schema::dropIfExists('interest_area_has_interest_group');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

    }
}
