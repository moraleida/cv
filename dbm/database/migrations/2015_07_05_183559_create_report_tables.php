<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'report_campaign', function (Blueprint $table) {
                $table->increments('id');
                $table->string('mailchimp_id', 100);
                $table->integer('emails_sent');
                $table->integer('abuse_reports');
                $table->integer('unsubscribed');
                $table->datetime('send_time');
                $table->string('bounces', 255);
                $table->string('forwards', 255);
                $table->string('opens', 255);
                $table->string('clicks', 255);
                $table->string('facebook_likes', 255);
                $table->string('industry_stats', 255);
                $table->string('list_stats', 255);
                $table->longText('timeseries');

                $table->integer('id_campaign')->unsigned();
                $table->foreign('id_campaign')->references('id')->on('campaign');
                $table->timestamps();
            }
        );

        /*
        Schema::create(
            'report_user', function (Blueprint $table) {
                $table->increments('id');
                $table->string('username', 45)->unique();
                $table->string('email', 255)->unique();
                $table->string('name', 45);
                $table->string('lastname', 45);
                $table->enum('role', ['administrador','editor','usuario', 'inativo']);
                $table->string('password', 255);
                $table->rememberToken();
                $table->timestamps();

                $table->integer('id_campaign')->unsigned();
                $table->foreign('id_campaign')->references('id')->on('campaign');
                $table->integer('id_client')->unsigned();
                $table->foreign('id_client')->references('id')->on('client');
            }
        );
         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_campaign');
    }
}
