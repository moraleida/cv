<?php
// @codingStandardsIgnoreFile

use Illuminate\Database\Connection;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Address::class, function ($faker) {
    $client = DB::table('client')->lists('id');
    $street = DB::table('street')->lists('id');
    $neighborhood = DB::table('neighborhood')->lists('id');
    $city = DB::table('city')->lists('id');
    $state = DB::table('state')->lists('id');
    $intl_state = DB::table('intl_state')->lists('id');
    $intl_city = DB::table('intl_city')->lists('id');
    $country = DB::table('country')->lists('id');
    return [
        'number' => $faker->buildingNumber,
        'complement' => $faker->secondaryAddress,
        'zip' => $faker->postcode,
        'type' => $faker->randomElement(array('residencial','comercial')),
        'id_client' => $faker->randomElement($client),
        'id_street' => $faker->randomElement($street),
        'id_neighborhood' => $faker->randomElement($neighborhood),
        'id_city' => $faker->randomElement($city),
        'id_state' => $faker->randomElement($state),
        'id_country' => $faker->randomElement($country),
    ];
});

$factory->define(App\Campaign::class, function ($faker) {
    $num = $faker->numberBetween(0,30);
    $emails = [];
    while($num > 0) {
        $emails[] = $faker->companyEmail;
        $num--;
    } 
    return [
        'name' => $faker->word,
        'extra_emails' => json_encode($emails),
        'processed' => $faker->dateTimeThisYear,
    ];
});

$factory->define(App\CampaignHasList::class, function ($faker) {
    $campaign = DB::table('campaign')->lists('id');
    $list = DB::table('list')->lists('id');
    return [
        'id_campaign' => $faker->randomElement($campaign),
        'id_list' => $faker->randomElement($list),
    ];
});

$factory->define(App\CampaignHasSegment::class, function ($faker) {
    $campaign = DB::table('campaign')->lists('id');
    $segment = DB::table('segment')->lists('id');
    return [
        'id_campaign' => $faker->randomElement($campaign),
        'id_segment' => $faker->randomElement($segment),
    ];
});

$factory->define(App\City::class, function ($faker) {
    $state = DB::table('state')->lists('id');
    return [
        'city' => $faker->city,
        'id_state' => $faker->randomElement($state),
    ];
});

$factory->define(App\Client::class, function ($faker) {
    $origins = DB::table('origin')->lists('id');
    $methods = DB::table('entry_method')->lists('id');
    $image = str_replace('public', '', $faker->image('public/img', '100', '100', 'people'));
    return [
        'name' => $faker->firstName,
        'middlename' => $faker->lastName,
        'lastname' => $faker->lastName,
        'birthday' => $faker->date(),
        'type' => $faker->randomElement(array('nao-associado','patrimonial','cooperador','coletivo','entidade','emerito','individual')),
        'associate_code' => $faker->randomNumber(),
        'identification' => str_random(20),
        'identification_type' => $faker->randomElement(array('rg','cpf','cnh','passaporte','social-security','outro')),
        'language' => $faker->randomElement(array('pt','en')),
        'foreigner' => $faker->boolean(),
        'image' => $image,
        'vip' => $faker->numberBetween(0,1),
        'id_origin' => $faker->randomElement($origins),
        'id_entry_method' => $faker->randomElement($methods),
    ];
});

$factory->define(App\ClientHasCompany::class, function ($faker) {
    $company = DB::table('company')->lists('id');
    $client = DB::table('client')->lists('id');
    return [
        'id_client' => $faker->randomElement($client),
        'id_company' => $faker->randomElement($company),
    ];
});

$factory->define(App\ClientHasFunction::class, function ($faker) {
    $client = DB::table('client')->lists('id');
    $function = DB::table('function')->lists('id');
    $company = DB::table('company')->lists('id');
    return [
        'id_client' => $faker->randomElement($client),
        'id_function' => $faker->randomElement($function),
        'id_company' => $faker->randomElement($company),
    ];
});

$factory->define(App\ClientHasInterestArea::class, function ($faker) {
    $client = DB::table('client')->lists('id');
    $interest_area = DB::table('interest_area')->lists('id');
    return [
        'id_client' => $faker->randomElement($client),
        'id_interest_area' => $faker->randomElement($interest_area),
    ];
});

$factory->define(App\ClientHasInterestGroup::class, function ($faker) {
    $client = DB::table('client')->lists('id');
    $interest_group = DB::table('interest_group')->lists('id');
    return [
        'id_client' => $faker->randomElement($client),
        'id_interest_group' => $faker->randomElement($interest_group),
    ];
});

$factory->define(App\ClientHasSocialNetwork::class, function ($faker) {
    $client = DB::table('client')->lists('id');
    $social_network = DB::table('social_network')->lists('id');
    return [
        'id_client' => $faker->randomElement($client),
        'id_social_network' => $faker->randomElement($social_network),
        'social_network_code' => $faker->url,
    ];
});

$factory->define(App\Company::class, function ($faker) {
    return [
        'name' => $faker->company,
    ];
});

$factory->define(App\Country::class, function ($faker) {
    return [
        'country' => $faker->country,
    ];
});

$factory->define(App\Department::class, function ($faker) {
    return [
        'name' => $faker->catchPhrase,
        'description' => $faker->text(40),
    ];
});

$factory->define(App\Email::class, function ($faker) {
    $clients = DB::table('client')->lists('id');
    return [
        'email' => $faker->email,
        'type' => $faker->randomElement(array('pessoal','comercial')),
        'mailchimp_id' => str_random(25),
        'id_client' => $faker->randomElement($clients),
    ];
});

$factory->define(App\EmailDomain::class, function ($faker) {
    return [
        'domain' => $faker->domainName,
        'type' => $faker->randomElement(array('pessoal','comercial')),
    ];
});

$factory->define(App\Entry_method::class, function ($faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text(40),
    ];
});

$factory->define(App\Functions::class, function ($faker) {
    $department = DB::table('department')->lists('id');
    return [
        'name' => $faker->word,
        'description' => $faker->text(250),
        'id_department' => $faker->randomElement($department),
    ];
});

$factory->define(App\InterestArea::class, function ($faker) {
    return [
        'name' => $faker->catchPhrase,
        'description' => $faker->text(40),
    ];
});

$factory->define(App\InterestGroup::class, function ($faker) {
    return [
        'name' => $faker->catchPhrase,
        'description' => $faker->text(40),
    ];
});

$factory->define(App\IntlAddress::class, function ($faker) {
    $client = DB::table('client')->lists('id');
    $intl_city = DB::table('intl_city')->lists('id');
    return [
        'street' => $faker->streetAddress,
        'complement' => $faker->secondaryAddress,
        'zip' => $faker->postcode,
        'type' => $faker->randomElement(array('residencial','comercial')),
        'id_client' => $faker->randomElement($client),
        'id_intl_city' => $faker->randomElement($intl_city),
    ];
});

$factory->define(App\IntlCity::class, function ($faker) {
    $country = DB::table('country')->lists('id');
    return [
        'city' => $faker->city,
        'id_country' => $faker->randomElement($country),
    ];
});

$factory->define(App\IntlState::class, function ($faker) {
    $country = DB::table('country')->lists('id');
    return [
        'state' => $faker->state,
        'id_country' => $faker->randomElement($country),
    ];
});

$factory->define(App\IntlStateHasIntlCity::class, function ($faker) {
    $intl_state = DB::table('intl_state')->lists('id');
    $intl_city = DB::table('intl_city')->lists('id');
    return [
        'id_intl_state' => $faker->randomElement($intl_state),
        'id_intl_city' => $faker->randomElement($intl_city),
    ];
});

$factory->define(App\Lists::class, function ($faker) {
    $users = DB::table('user')->lists('id');
    return [
        'name' => $faker->word,
        'rules' => $faker->randomElement(array('todas','qualquer')),
        'id_user' => $faker->randomElement($users),
    ];
});

$factory->define(App\Neighborhood::class, function ($faker) {
    $city = DB::table('city')->lists('id');
    return [
        'neighborhood' => $faker->company,
        'id_city' => $faker->randomElement($city),
    ];
});

$factory->define(App\Origin::class, function ($faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text(40),
    ];
});

$factory->define(App\Phone::class, function ($faker) {
    $client = DB::table('client')->lists('id');
    return [
        'country_code' => $faker->numberBetween(1,999),
        'area_code' => $faker->numberBetween(1,999),
        'phone' => $faker->numberBetween(20000000,999999999),
        'extension' => $faker->numberBetween(1,999),
        'type' => $faker->randomElement(array('fixo','celular')),
        'relation' => $faker->randomElement(array('pessoal','comercial')),
        'id_client' => $faker->randomElement($client),
    ];
});

$factory->define(App\Segment::class, function ($faker) {
    $users = DB::table('user')->lists('id');
    $lists = DB::table('list')->lists('id');
    return [
        'name' => $faker->word,
        'rules' => $faker->randomElement(array('todas','qualquer')),
        'id_user' => $faker->randomElement($users),
        'id_list' => $faker->randomElement($lists),
    ];
});

$factory->define(App\SocialNetwork::class, function ($faker) {
    return [
        'name' => $faker->domainWord,
    ];
});

$factory->define(App\State::class, function ($faker) {
    $country = DB::table('country')->lists('id');
    return [
        'state' => $faker->state,
        'id_country' => $faker->randomElement($country),
    ];
});

$factory->define(App\Street::class, function ($faker) {
    $neighborhood = DB::table('neighborhood')->lists('id');
    return [
        'street' => $faker->company,
        'id_neighborhood' => $faker->randomElement($neighborhood),
    ];
});

$factory->define(App\Support::class, function ($faker) {
    $users = DB::table('user')->lists('id');
    return [
        'subject' => $faker->catchPhrase,
        'message' => $faker->text(400),
        'id_user' => $faker->randomElement($users),
    ];
});

$factory->define(App\User::class, function ($faker) {
    return [
        'username' => $faker->word,
        'name' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->email,
        'role' => $faker->randomElement(array('administrador','editor','usuario','inativo')),
        'password' => str_random(10),
    ];
});
