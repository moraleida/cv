<?php
// @codingStandardsIgnoreFile

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Connection;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
/*
        factory('App\User', 15)->create();
        factory('App\Support', 35)->create();            // foreign user
 */
//        factory('App\Origin', 25)->create();
//        factory('App\Entry_method', 5)->create();
        factory('App\Client', 300)->create();
        factory('App\Phone', 500)->create();             // foreign client
        factory('App\SocialNetwork', 5)->create();
   //     factory('App\InterestGroup', 5)->create();
 //       factory('App\InterestArea', 5)->create();
//        factory('App\Department', 25)->create();
  //      factory('App\Functions', 50)->create();          // foreign department
        factory('App\Email', 600)->create();
//        factory('App\EmailDomain', 300)->create();
    //    factory('App\Company', 30)->create();
        factory('App\ClientHasSocialNetwork', 200)->create();
        factory('App\ClientHasInterestGroup', 600)->create();
  //      factory('App\ClientHasInterestArea', 100)->create();
        factory('App\ClientHasCompany', 450)->create();
        factory('App\ClientHasFunction', 450)->create();
/*
        factory('App\Campaign', 45)->create();
        factory('App\Lists', 45)->create();              // foreign user
        factory('App\Segment', 45)->create();            // foreign list
        factory('App\CampaignHasList', 45)->create();    // foreign campaign,list
        factory('App\CampaignHasSegment', 45)->create(); // foreign campaign,segment
/*
        factory('App\Country', 10)->create();
        factory('App\State', 20)->create();              // foreign country
        factory('App\City', 40)->create();               // foreign state
*/
        factory('App\Neighborhood', 80)->create();       // foreign city
        factory('App\Street', 80)->create();             // foreign neighborhood
        factory('App\Address', 250)->create();           // foreign client,street
/*
        factory('App\IntlState', 20)->create();              // foreign country
        factory('App\IntlCity', 40)->create();               // foreign country
        factory('App\IntlStateHasIntlCity', 80)->create();       // foreign intl_state,intl_city
*/
        factory('App\IntlAddress', 50)->create();           // foreign client,intl_city

        // create
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Model::reguard();
    }
}
