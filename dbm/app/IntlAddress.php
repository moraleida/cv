<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntlAddress extends Model
{
    public $timestamps = false;
    protected $table = 'intl_address';
    //
}
