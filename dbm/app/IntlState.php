<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntlState extends Model
{
    public $timestamps = false;
    protected $table = 'intl_state';
    //
}
