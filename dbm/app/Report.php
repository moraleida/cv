<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'report';
    protected $fillable = ['name'];

    private static $pagetitle = 'relatórios';
    private static $headerbtn = false;
    private static $headerbtntext = 'novo relatório';
    private static $headerbtnroute = 'client.register';

    public static function getIdentifiers()
    {
        
        return  ['pagetitle' => self::$pagetitle, 
                 'headerbtn' => self::$headerbtn,
                 'headerbtntext' => self::$headerbtntext,
                 'headerbtnroute' => self::$headerbtnroute];
    }
}
