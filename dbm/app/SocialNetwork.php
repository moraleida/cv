<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialNetwork extends Model
{
    public $timestamps = false;
    protected $table = 'social_network';
    //
}
