<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
    public $timestamps = false;
    protected $table = 'origin';
}
