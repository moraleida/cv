<?php

namespace App;

use DB;
use App\Lib\Mailc;
use App\Lists;
use App\Lib\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Log;

class Campaign extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'campaign';
    protected $fillable = ['name', 'extra_emails', 'processed', 'mailchimp_id'];

    private static $pagetitle = 'campanhas';
    private static $headerbtn = true;
    private static $headerbtntext = 'nova campanha';
    private static $headerbtnroute = 'campaign.create';

    public static function getIdentifiers()
    {
        
        return  ['pagetitle' => self::$pagetitle, 
                 'headerbtn' => self::$headerbtn,
                 'headerbtntext' => self::$headerbtntext,
                 'headerbtnroute' => self::$headerbtnroute];
    }

    public function storeNewCampaign($input)
    {
        $campaign = new Campaign;
        if(!isset($input['name'])) {
            return false;
        }

        $emails = '';
        if(isset($input['emails'])) {
            $emails = serialize($input['emails']);
        }

        $result = $this->_createRemoteCampaign($input);
        $campaign->campaign = $input['name'];
        $campaign->extra_emails = $emails;
        $campaign->mailchimp_id = $result['id'];
        $campaign->mailchimp_web_id = $result['web_id'];
        $campaign->save();

        if(isset($input['lists'])) {
            foreach($input['lists'] as $list) {
                $campaignHasList = DB::table('campaign_has_list')
                    ->where('id_campaign', $campaign->id)
                    ->where('id_list', $list)->first();

                if(empty($campaignHasList))
                    DB::table('campaign_has_list')->insert(
                        ['id_campaign' => $campaign->id,
                         'id_list' => $list]
                     );
            }
        }

        if(isset($input['segments'])) {
            foreach($input['segments'] as $segment) {
                $campaignHasSegment = DB::table('campaign_has_segment')
                    ->where('id_campaign', $campaign->id)
                    ->where('id_segment', $segment)->first();

                if(empty($campaignHasSegment))
                    DB::table('campaign_has_segment')->insert(
                        ['id_campaign' => $campaign->id,
                         'id_segment' => $segment]
                     );
            }
        }


        return $result;
    }


    /**
     * _createRemoteCampaign
     * Busca os emails da campanha e faz a criação remota no Mailchimp
     *
     * @param array $input
     * @access private
     * @return array $campaign
     */
    private function _createRemoteCampaign(array $input)
    {
        $lists = new Lists;
        $emails = array_unique(Helpers::flattenArray($lists->getCampaignEmails($input)));
        Log::info(array_values($emails));

        $Mailchimp = new Mailc;

        $campaign = $Mailchimp->campaignInsert($input['name'], 'Assunto', $emails);
        
        return $campaign;
    }
}
