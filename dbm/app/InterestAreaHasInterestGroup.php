<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterestAreaHasInterestGroup extends Model
{
    public $timestamps = false;
    protected $table = 'interest_area_has_interest_group';
    //
}
