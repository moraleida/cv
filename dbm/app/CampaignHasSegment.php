<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignHasSegment extends Model
{
    public $timestamps = false;
    protected $table = 'campaign_has_segment';
    //
}
