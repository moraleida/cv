<?php

namespace App;

use DB;
use App\Lists;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'client';
    protected $fillable = ['name', 'middlename', 'lastname', 'birthday', 
                            'type', 'associate_code', 'identification', 
                            'identification_type', 'password', 'id_entry_method', 
                            'id_origin', 'id_client'];
    protected $dates = ['deleted_at'];

    public $altPageTitle;

    private static $pagetitle = 'contatos';
    private static $headerbtn = true;
    private static $headerbtntext = 'novo contato';
    private static $headerbtnroute = 'client.add';

    public static function getIdentifiers()
    {
        
        return  ['pagetitle' => self::$pagetitle, 
                 'headerbtn' => self::$headerbtn,
                 'headerbtntext' => self::$headerbtntext,
                 'headerbtnroute' => self::$headerbtnroute];
    }

    public function getClientList(Request $request, $paginate = 15)
    {
        
        $segments = $request->segments();

        // Se estamos em uma URL composta, a busca é segmentada por lista
        if (count($segments) > 1 && ($segments[1] == 'list' || $segments[1] == 'segment')) {
            // TODO: finalizar busca de contatos por lista
            $list = DB::table($segments[1])->where('id', $segments[2])->first();
            $l = new Lists;
            $clients = $l->getFilteredResultsFromDBObject($list, $paginate);
            $unique = [];
            foreach ($clients as $c) {
                if (!in_array($c->id, $unique)) {
                    $unique[] = $c->id;
                    $objects[] = $c;
                }
            }
            $unique = collect($objects);
            $current = $request->query('page');
            $current = ($current ? $request->query('page') : 1);
            $clients = new LengthAwarePaginator(
                $unique->forPage($current, $paginate), $unique->count(), 
                $paginate, $current, ['path' => '/'. join('/', $segments)]
            );

            $this->altPageTitle = self::$pagetitle.': '.trans('tables.'.$segments[1]).' - '.$list->{$segments[1]};

        } elseif (count($segments) > 1) {

            switch ($segments[1]) :
                case 'name' :
                    $clients = DB::table('client')
                        ->select('*', 'client.id as id')
                        ->where('name', 'like', '%'.$segments[2].'%')
                        ->orWhere('middlename', 'like', '%'.$segments[2].'%')
                        ->orWhere('lastname', 'like', '%'.$segments[2].'%')
                        ->paginate(15);
                    break;
                case 'email' :
                    $clients = DB::table('client')
                        ->select('*', 'client.id as id')
                        ->join('email', 'client.id', '=', 'email.id_client')
                        ->where('email', 'like', '%'.$segments[2].'%')
                        ->paginate(15);
                    break;
                case 'phone' :
                    $clients = DB::table('client')
                        ->select('*', 'client.id as id')
                        ->join('phone', 'client.id', '=', 'phone.id_client')
                        ->where('country_code', 'like', '%'.$segments[2].'%')
                        ->orWhere('area_code', 'like', '%'.$segments[2].'%')
                        ->orWhere('phone', 'like', '%'.$segments[2].'%')
                        ->orWhere('extension', 'like', '%'.$segments[2].'%')
                        ->paginate(15);
                    break;
                case 'function' :
                    $clients = DB::table('client')
                        ->select('*', 'client.id as id', 'client.name as name', 'function.name as function')
                        ->join('client_has_function', 'client.id', '=', 'client_has_function.id_client')
                        ->join('function', 'client_has_function.id_function', '=', 'function.id')
                        ->where('function.name', 'like', '%'.$segments[2].'%')
                        ->paginate(15);
                    break;
                case 'company' :
                    $clients = DB::table('client')
                        ->select('*', 'client.id as id', 'client.name as name')
                        ->join('client_has_company', 'client.id', '=', 'client_has_company.id_client')
                        ->join('company', 'client_has_company.id_company', '=', 'company.id')
                        ->where('company.name', 'like', '%'.$segments[2].'%')
                        ->paginate(15);
                    break;
            endswitch;

        } else {
            $clients = DB::table('client')->paginate(15);
        }

        $queriedClients = $this->buildClientListInfo($clients);

        return $queriedClients;
    }

    private function buildClientListInfo($clients)
    {
        foreach($clients as $client) {
            $client->emails = DB::table('email')->where('id_client', $client->id)->lists('email');
            $client->phones = DB::table('phone')->where('id_client', $client->id)->get();
            $client->company = DB::table('company')
                ->join('client_has_company', 'company.id', '=', 'client_has_company.id_company')
                ->where('client_has_company.id_client', $client->id)
                ->lists('company.name');
            $client->function = DB::table('function')
                ->join('client_has_function', 'function.id', '=', 'client_has_function.id_client')
                ->where('client_has_function.id_client', $client->id)
                ->get();
            $client->address = DB::table('address')
                ->join('street', 'address.id_street', '=', 'street.id')
                ->join('neighborhood', 'street.id_neighborhood', '=', 'neighborhood.id')
                ->join('city', 'neighborhood.id_city', '=', 'city.id')
                ->join('state', 'city.id_state', '=', 'state.id')
                ->join('country', 'state.id_country', '=', 'country.id')
                ->where('address.id_client', $client->id)
                ->get();
        }

        return $clients;
    }

    public function getClientToEdit($id)
    {
        $client = DB::table('client')->where('id',$id)->first();

        $client->emails = DB::table('email')->where('id_client', $client->id)->get();
        $client->phones = DB::table('phone')->where('id_client', $client->id)->get();
        $client->company = DB::table('company')
            ->join('client_has_company', 'company.id', '=', 'client_has_company.id_company')
            ->where('client_has_company.id_client', $client->id)
            ->lists('company.name');
        $client->function = DB::table('function')
            ->select('function.id', 'company.name as company', 'function.name as function', 'id_department')
            ->join('client_has_function', 'function.id', '=', 'client_has_function.id_function')
            ->join('department', 'department.id', '=', 'function.id_department')
            ->join('company', 'company.id', '=', 'client_has_function.id_company')
            ->where('client_has_function.id_client', $client->id)
            ->get();
        $client->interests = DB::table('interest_group')
            ->join('client_has_interest_group', 'interest_group.id', '=', 'client_has_interest_group.id_interest_group')
            ->where('client_has_interest_group.id_client', $client->id)
            ->get();
        $client->interestIDs = array();
        foreach($client->interests as $interest) {
            array_push($client->interestIDs, $interest->id);
        }
        $client->address = DB::table('address')
            ->join('street', 'address.id_street', '=', 'street.id')
            ->join('neighborhood', 'street.id_neighborhood', '=', 'neighborhood.id')
            ->join('city', 'neighborhood.id_city', '=', 'city.id')
            ->join('state', 'city.id_state', '=', 'state.id')
            ->join('country', 'state.id_country', '=', 'country.id')
            ->where('address.id_client', $client->id)
            ->get();

        $client->birthday = explode('-', $client->birthday);
        // VIP status
        $client->vipClass = '';
        if(!$client->vip)
            $client->vipClass = 'gray-state';

        return $client;

    }


    public function storeClient(Request $request, $client, $cadastrointerno)
    {
        $client->name = $request->name;
        $client->middlename = $request->middlename;
        $client->lastname = $request->lastname;
        $client->birthday = '2000-'.$request->birthmonth.'-'.$request->birthday;
        $client->type = ($request->associate == 'on' ? 'individual' : 'nao-associado');
        $client->identification  = $request->identification;
        $client->identification_type  = $request->identificationtype;
  //      $client->id_city  = $request->business['city'];

        if($cadastrointerno) {
            // Cadastro feito na área administrativa
            $entry_method = DB::table('entry_method')->where('name','Manual')->first();
            $client->id_origin = $request->input('origin');
            $client->id_entry_method = $entry_method->id;
            $client->vip = (bool) $request->input('vip');
            $client->associate_code = $request->input('associate_code');
            $client->type = $request->input('associate_type');
            $client->language = $request->input('language');
            $client->foreigner = ($request->input('foreigner') == 'yes' ? true : false);
        } else {
            // Cadastro feito na área aberta
            $origin = DB::table('origin')->where('name','Cadastro Site')->first();
            $entry_method = DB::table('entry_method')->where('name','Manual')->first();
            $client->password  = bcrypt($request->password);
            $client->id_origin  = $origin->id;
            $client->id_entry_method  = $entry_method->id;
        }

        $client->save();

        $image = $request->input('uploadedpath');
        if(!empty($image))
            $client = $this->storeClientImage($image, $client);

        if(!is_array($request->input('business')))
            $request->business = [$request->input('business')];

        if(!is_array($request->input('company'))) {
            $request->company = [$request->input('company')];
        }

        foreach($request->business as $address) {
            if($address['country'] == '1') {
                $this->storeClientAddressBR($address, $client);
            } else {
                $this->storeClientAddressIntl($address, $client);
            }
        }

        $this->storeClientEmails($request, $client);
        $this->storeClientPhoneNumbers($request, $client);

        foreach($request->company as $company) {
            $this->storeClientCompany($company, $client);
            $this->storeClientFunctionAndDepartment($company, $client);
        }

        $this->storeClientInterestGroups($request->interests, $client);

        return $client;
    }


    private function storeClientImage($image, $client)
    {
        $uploadPath = app_path('Uploads');
        $imagesPath = public_path('img');

        $finalimg = $imagesPath.'/'.$client->id.'.jpg';
        $finalimgUrl = '/img/'.$client->id.'.jpg';
        $move = rename($uploadPath.'/'.$image, $finalimg);
        $client->image = $finalimgUrl;
        $client->save();

        return $client;
    }

    private function storeClientAddressBR($address, $client)
    {
        // Find City
        $city = DB::table('city')
                    ->where('id', $address['city'])
                    ->where('id_state', $address['state'])
                    ->first();

        // Find Neighborhood
        $neighborhood = ['neighborhood' => $address['address']['neighborhood'],
                         'id_city' => $city->id];

        $neighborhood_exists = DB::table('neighborhood')->where('neighborhood', $neighborhood['neighborhood'])
                                 ->where('id_city', $neighborhood['id_city'])
                                 ->get();

        // If not found, create it
        if(empty($neighborhood_exists)) {
            $neighborhood['id'] = DB::table('neighborhood')->insertGetId($neighborhood);
        } else {
            $neighborhood['id'] = $neighborhood_exists[0]->id;
        }

        // Find Street
        $street = ['street' => $address['address']['street'],
                   'id_neighborhood' => $neighborhood['id']];

        $street_exists = DB::table('street')->where('street', $street['street'])
                                 ->where('id_neighborhood', $street['id_neighborhood'])
                                 ->get();

        // If not found, create it
        if(empty($street_exists)) {
            $street['id'] = DB::table('street')->insertGetId($street);
        } else {
            $street['id'] = $street_exists[0]->id;
        }

        // Compose address
        $composedAddress = ['number' => $address['address']['number'],
                    'complement' => $address['address']['complement'],
                    'zip' => $address['zip'],
                    'type' => 'residencial',
                    'id_street' => $street['id'],
                    'id_neighborhood' => $neighborhood['id'],
                    'id_city' => $city->id,
                    'id_state' => $address['state'],
                    'id_country' => $address['country'],
                    'id_client' => $client->id,
                ];

        // Store
        return DB::table('address')->insert($composedAddress);
    }

    private function storeClientAddressIntl($address, $client)
    {
        // Find country
        $country = DB::table('country')
                    ->where('id', $address['country'])
                    ->first();

        // Find city
        $intl_city = DB::table('intl_city')
                    ->where('id', $address['city'])
                    ->where('id_country', $country->id)
                    ->first();

        // Compose Address
        $composedAddress = ['street' => $address['intl_address']['street'],
                    'complement' => $address['intl_address']['complement'],
                    'zip' => $address['zip'],
                    'type' => 'residencial',
                    'id_intl_city' => $intl_city->id,
                    'id_intl_state' => $address['state'],
                    'id_country' => $address['country'],
                    'id_client' => $client->id,
                    ];
        
        return DB::table('intl_address')->insert($composedAddress);
    }

    private function storeClientEmails(Request $request, $client)
    {
        // Trata emails para salvar
        foreach($request->emailaddr as $key => $addr) {
            if(isset($request->emailaddr[$key]) && isset($request->emailtype[$key])) {
                $emails[] = ['email' => $request->emailaddr[$key][0], 
                            'type' => $request->emailtype[$key][0],
                            'id_client' => $client->id];
            }
        }
        // Salva emails
        return ( empty($emails) ? false : DB::table('email')->insert($emails) );
    }

    private function storeClientPhoneNumbers(Request $request, $client)
    {
        // Trata telefones para salvar
        // Pessoais
        foreach ($request->phonenum as $key => $phone) {
            if (isset($request->phoneint[$key]) 
                && isset($request->phonenum[$key]) 
                && isset($request->phonetype[$key])
            ) {
                $area_code = preg_match('|\(\d+\)|', $request->phonenum[$key][0], $ddd);
                $area_code = str_replace(['(',')'], '', array_shift($ddd));
                $phonenum = preg_split('|\(\d+\)|', $request->phonenum[$key][0], -1, PREG_SPLIT_NO_EMPTY);
                $phonenum = array_shift($phonenum);

                $phones[] = ['country_code' => trim($request->phoneint[$key][0]), 
                            'area_code' => trim($area_code),
                            'phone' => trim($phonenum),
                            'type' => $request->phonetype[$key][0],
                            'relation' => 'pessoal',
                            'extension' => null,
                            'id_client' => $client->id];
            }
        }

        // Comerciais
        if (isset($request->company['phonenum'])) {
            $phoneint = $request->company['phoneint'];
            $phonenum = $request->company['phonenum'];
            $phonetype = $request->company['phonetype'];

            if (isset($request->company['phoneext'])) 
                $phoneext = $request->company['phoneext'];

            foreach ($phonenum as $key => $value) {
                $area_code = preg_match('|\(\d+\)|', $value[0], $ddd);
                $area_code = str_replace(['(',')'], '', array_shift($ddd));
                $phonenum = preg_split('|\(\d+\)|', $value[0], -1, PREG_SPLIT_NO_EMPTY);
                $phonenum = array_shift($phonenum);

                $phones[] = ['country_code' => trim($phoneint[$key][0]), 
                            'area_code' => trim($area_code),
                            'phone' => trim($phonenum),
                            'type' => $phonetype[$key][0],
                            'relation' => 'comercial',
                            'extension' => (isset($phoneext[$key][0]) ? $phoneext[$key][0] : null),
                            'id_client' => $client->id];
            }
        }

        // Salva telefones
        return ( empty($phones) ? false : DB::table('phone')->insert($phones) );
    }

    private function storeClientCompany($org, $client)
    {
        if(!isset($org['name']))
            return;

        $company = DB::table('company')->where('name', $org['name'])->first();
        if(empty($company)) {
            $company = (object) array();
            $company->id = DB::table('company')->insertGetId(['name' => $org['name']]);
        }
        if(isset($company->id))
            DB::table('client_has_company')->insert(['id_client' => $client->id, 'id_company' => $company->id]);

        return;
    }

    private function storeClientFunctionAndDepartment($org, $client)
    {
        if(!isset($org['name']))
            return;
        // Departamento
        $department = DB::table('department')->where('name', $org['area'])->first();
        if(empty($department)) {
            $department = (object) array();
            $department->id = DB::table('department')->insertGetId(['name' => $org['area']]);
        }

        // Cargo
        $company = DB::table('company')->where('name', $org['name'])->first();
        $function = DB::table('function')->where('name', $org['role'])->first();
        if(empty($function)) {
            $function = (object) array();
            $function->id = DB::table('function')->insertGetId(['name' => $org['role'], 'id_department' => $department->id]);
        }
        if(isset($function->id))
            DB::table('client_has_function')->insert(['id_client' => $client->id, 'id_company' => $company->id, 'id_function' => $function->id]);

        return;
    }

    private function storeClientInterestGroups($interests, $client)
    {
        foreach($interests as $key => $int) {
            $group = DB::table('interest_group')->where('id', $key)->first();
            $groups[] = ['id_client' => $client->id, 'id_interest_group' => $group->id];
        }
        return DB::table('client_has_interest_group')->insert($groups);
    }
}
