<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $timestamps = false;
    protected $table = 'state';

    /**
     * Eloquent Relations
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'id_country');
    }
}
