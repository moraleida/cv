<?php

namespace App;

use DB;
use Event;
use App\Lib\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Request;

class Lists extends Model
{
    protected $table = 'list';
    protected $fillable = ['list', 'rules', 'filters', 'mailchimp_id', 'id_user'];

    private static $columns = ['city', 'company', 'country',
                    'department', 'entry_method', 'function',
                    'interest_group', 'intl_city', 'intl_state',
                    'list', 'neighborhood', 'origin', 'state'];

    private $tables_columns = [
                'entry_method'   => ['table' => 'client', 'id_column' => 'id', 'val_column' => 'id_entry_method', 'field' => 'name'],
                'origin'         => ['table' => 'client', 'id_column' => 'id', 'val_column' => 'id_origin', 'field' => 'name'],
                'company'        => ['table' => 'client_has_company',        'id_column' => 'id_client', 'val_column' => 'id_company', 'field' => 'name'],
                'function'       => ['table' => 'client_has_function',       'id_column' => 'id_client', 'val_column' => 'id_function', 'field' => 'name'],
                'department'     => ['table' => 'client_has_department',     'id_column' => 'id_client', 'val_column' => 'id_department', 'field' => 'name'],
                'interest_group' => ['table' => 'client_has_interest_group', 'id_column' => 'id_client', 'val_column' => 'id_interest_group', 'name' => 'city'],
                'city'           => ['table' => 'address', 'id_column' => 'id_client',         'val_column' => 'id_city', 'field' => 'city'],
                'state'          => ['table' => 'address', 'id_column' => 'id_client',        'val_column' => 'id_state', 'field' => 'state'],
                'country'        => ['table' => 'address', 'id_column' => 'id_client',       'val_column' => 'id_country', 'field' => 'country'],
                'neighborhood'   => ['table' => 'address', 'id_column' => 'id_client', 'val_column' => 'id_neighborhood', 'field' => 'neighborhood'],
                'intl_city'      => ['table' => 'intl_address', 'id_column' => 'id_client',    'val_column' => 'id_intl_city', 'field' => 'city'],
                'intl_state'     => ['table' => 'intl_address', 'id_column' => 'id_client',   'val_column' => 'id_intl_state', 'field' => 'state'],
            ];

    private $where_operators = [
               'is'     => '=',
               'isnot'  => '!=',
               'like'    => 'LIKE',
               'notlike' => 'NOT LIKE' 
        ];
    
    private static $verbs = [ 
        'is' => 'é igual a', 
        'isnot' => 'não é igual a', 
        'like' => 'contém', 
        'notlike' => 'não contém'
    ];

    public static function getFilterColumns()
    {
        foreach(self::$columns as $col) {
            $cs[$col] = trans('tables.'.$col);
        }
        asort($cs);
        array_unshift($cs, 'Selecione');

        return ['column1' => $cs, 'verbs' => self::$verbs];
    }

    public function getListsWithSegments($paginate = 15, $current = 1)
    {
        $lists = DB::table('list')
            ->select('id', 'list', 'filters')
            ->orderBy('id', 'DESC')->get();
        foreach ($lists as $list) {
            $segments = DB::table('segment')
                ->where('id_list', $list->id)->get();
            $list->segments = $segments;
            $list->allowSegments = true;

            if (empty($list->filters))
                $list->allowSegments = false;
        }
        $lists = collect($lists);
        
        if (!$paginate)
            return $lists;
        
        return new LengthAwarePaginator(
            $lists->forPage($current, $paginate), $lists->count(), 
            $paginate, $current, ['path' => '/listas']
        );
    }

    public function getListFiltersColumns($listFilters, $segmentFilters = false)
    {
        $cols = [];
        $filters = unserialize($listFilters->filters);
        if($segmentFilters) {
            $filters = array_merge($filters, unserialize($segmentFilters->filters));
        }

        foreach($filters as $set) {
            
            $table    = $this->tables_columns[$set['col1']]['table'];
            $id_column = $this->tables_columns[$set['col1']]['id_column'];
            $val_column = $this->tables_columns[$set['col1']]['val_column'];
            
            $label = trans('tables.'.$set['col1']);
            $operator = self::$verbs[$set['col2']];

            $nametable = str_replace('id_', '', $val_column);
            $namefield = $this->tables_columns[$set['col1']]['field'];
            $value = DB::table($nametable)
                ->select($namefield)
                ->where($nametable.'.id', $set['col3'])->first();

            if ($value) {
                $set['col1'] = [$set['col1'], $label];
                $set['col2'] = [$set['col2'], $operator];
                $set['col3'] = [$set['col3'], $value->{$namefield}];
                $cols[] = $set;
           }

        }
        if($segmentFilters) {
            $segmentFilters->cols = $cols;
            return $segmentFilters;
        } else {
            $listFilters->cols = $cols;
            return $listFilters;
        }
    }

    public static function getFilterOptions($table)
    {
        if (!in_array($table, self::$columns)) {
            return false;
        }

        $field = 'name';
        $options = DB::table($table)->first();

        if (!isset($options->name)) {
            $field = str_replace('intl_', '', $table);
        }

        $options = DB::table($table)
            ->select('id', $field.' as value')
            ->orderBy($field)
            ->get();

        return $options;
    }

    public function getFilteredResults($filters, $sanitized = false, $perpage = '200')
    {
        $rule = $filters['rule'];
        $skip = (isset($filters['skip']) ? $filters['skip'] : 0);

        if($sanitized) {
            $ruleset = $filters['filters'];
        } else {
            $ruleset = $this->sanitizeFiltersToRetrieve($filters['filters']);
        }

        $i=0;
        foreach ($ruleset as $set) {
            if($sanitized) {
                $col1 = $set['col1'];
                $col2 = $set['col2'];
                $col3 = $set['col3'];
            } else {
                $col1 = $set[0]['col1'];
                $col2 = $set[1]['col2'];
                $col3 = $set[2]['col3'];
            }

            if(!isset($col1)) {
                continue;
            }
            $table      = $this->tables_columns[$col1]['table'];
            $id_column  = $this->tables_columns[$col1]['id_column'];
            $val_column = $this->tables_columns[$col1]['val_column'];
            $comparison = $this->where_operators[$col2];
            $value      = $col3;

            $allSelects[] = ['table' => $table, 'id_column' => $id_column,
                'val_column' => $val_column, 'comparison' => $comparison,
                'value' => $value];

            if( $skip == 'false' || $i < $skip ) {
                // Se estamos construindo uma lista, todos os contatos
                // seguem as mesmas regras. Caso este seja um segmento
                // as regras da lista valem para os contatos da lista
                // e os contatos do segmento devem estar entre os filtrados
                // pela lista.
                $listuserstoquery[] = DB::table($table)
                    ->where($val_column, $comparison, $value)
                    ->lists($id_column);
            } else {
                $segmentuserstoquery[] = DB::table($table)
                    ->where($val_column, $comparison, $value)
                    ->lists($id_column);
            }

            $i++;
        }

        if ($rule == 'qualquer') { // busca todos os items em cada filtro
            // Flatten array
            $listuserstoquery = Helpers::flattenArray($listuserstoquery);
        } else { // busca somente os itens que correspondem a todos os filtros
            // Busca elementos em todos os arrays
            if (count($listuserstoquery) > 1) {
                $listuserstoquery = call_user_func_array('array_intersect', $listuserstoquery);
            } else {
                $listuserstoquery = $listuserstoquery[0];
            }
        }
        //
        // Remove duplicados
        $listuserstoquery = array_keys(array_flip($listuserstoquery));

        if(!empty($segmentuserstoquery)) {

            if (count($segmentuserstoquery) > 1) {
                $segmentuserstoquery = call_user_func_array('array_intersect', $segmentuserstoquery);
            } else {
                $segmentuserstoquery = $segmentuserstoquery[0];
            }

            $listuserstoquery = array_intersect($listuserstoquery, $segmentuserstoquery);
        }

        
        sort($listuserstoquery);

        if(Request::ajax()) {
            $resultSet = DB::table('client')
                ->select(
                    'client.id', 'client.name', 'client.lastname', 
                    'email.email', 'company.name as company', 
                    'function.name as function'
                )
                ->leftJoin('email', 'client.id', '=', 'email.id_client')
                ->leftJoin('address', 'client.id', '=', 'address.id_client')
                ->leftJoin('client_has_company', 'client.id', '=', 'client_has_company.id_client')
                ->leftJoin('client_has_function', 'client.id', '=', 'client_has_function.id_client')
                ->leftJoin('company', 'client_has_company.id_company', '=', 'company.id')
                ->leftJoin('function', 'client_has_function.id_function', '=', 'function.id')
                ->whereIn('client.id', $listuserstoquery);
        } else {
            $resultSet = DB::table('client')
                ->select(
                    'client.id', 'client.name', 'client.lastname', 
                    'email.email', 'company.name as company', 
                    'function.name as function'
                )
                ->leftJoin('email', 'client.id', '=', 'email.id_client')
                ->leftJoin('client_has_company', 'client.id', '=', 'client_has_company.id_client')
                ->leftJoin('client_has_function', 'client.id', '=', 'client_has_function.id_client')
                ->leftJoin('company', 'client_has_company.id_company', '=', 'company.id')
                ->leftJoin('function', 'client_has_function.id_function', '=', 'function.id')
                ->whereIn('client.id', $listuserstoquery);
        }

        foreach($allSelects as $whereClause) {
            $resultSet->where($whereClause['table'].'.'.$whereClause['val_column'], $whereClause['comparison'], $whereClause['value']);
        }
        $resultSet->groupBy('client.id');
        $resultSet = $resultSet->get();

        return $resultSet;
    }

    public function getFilteredResultsFromDBObject($obj, $perpage = 15)
    {
        $filters = unserialize($obj->filters);
        $query['filters'] = $filters;
        $query['rule'] = $obj->rules;

        return $this->getFilteredResults($query, true, $perpage);
    }

    private function sanitizeFiltersToRetrieve($filters)
    {
        // Busca as regras específicas pra montar a query
        $i=0;
        $invalid = false;
        foreach ($filters as $key => $filter) {
            // TODO: melhorar
            preg_match("|[{}]|", $filter['name'], $illegal);
            if (!empty($illegal)) {
                continue;
                unset($filters['filters'][$key]);
            }

            preg_match("|\[\d+]|", $filter['name'], $field);
            $field = str_replace(array('[',']'), '', $field);

            preg_match("|\[col\d+]|", $filter['name'], $col);
            $col = str_replace(array('[',']'), '', $col);
            
            $key = array_shift($field);
            $colkey = array_shift($col);

            if(empty($filter['value']))
                $invalid = true;

            if(!$invalid)
                $ruleset[$i][] = array($colkey => $filter['value']);

            if ($colkey == 'col3') {
                $i++;
                $invalid = false;
            }
        }

        foreach ($ruleset as $key => $rule) {
            if (count($rule) < 3) {
                unset($ruleset[$key]);
            }
        }

        return $ruleset;
    }

    public function sanitizeFiltersToSave($filters)
    {
        unset($filters['rules']);
        foreach ($filters as $key => $value) {
            if (isset($value['col1']) 
                && $value['col1'] != '0'
                && count($value) > 2
            ) {
                $filtered[] = $value;
            }
        }

        return $filtered;
    }


    public function getListOfLists($withSegments = false, $verb = 'filtrar por ', $formatted = false)
    {
        
        if(!$withSegments) {
            $init = ['' => $verb.' lista'];
            $options =  DB::table('list')->orderBy('list', 'asc')->lists('list', 'id');
            $options = $init+$options;

            if($formatted) {
                foreach($options as $key => $opt){
                    $result[] = '<option value="'.$key.'">'.$opt.'</option>';
                }
                $options = join('',$result);
            }
        } else {
            $options = array();
            $init = ['' => $verb.' segmento'];
            $segments = DB::table('segment')
                ->join('list', 'list.id', '=', 'segment.id_list')
                ->select('segment.id as id', 'segment.segment as segment', 'list.list as list')
                ->get();
            foreach($segments as $segment) {
                $options[$segment->list][$segment->id] = $segment->segment;
            }
            ksort($options);

            if($formatted) {
                $result = '<optgroup label="'.array_keys($options)[0].'">';
                $lists = [array_keys($options)[0]];
                foreach($options as $key => $opt){

                    if(!in_array($key, $lists)) {
                        $result .= '</optgroup><optgroup label="'.$key.'">';
                        $lists[] = $key;
                    }

                    foreach($opt as $o) {
                        $result .= '<option value="'.$o['id'].'">'.$o['segment'].'</option>';
                    }

                }
                $result .= '</optgroup>';
                $options = $result;
            }
            $options = $init+$options;
        }


        return $options;

    }

    public function getListFilters($id)
    {
        $list = DB::table('list')->select('filters')->where('id', $id)->first();

        return unserialize($list->filters);
    }

    public function getCampaignPreview($campaign, $return = 'table')
    {
        if(isset($campaign['lists'])) {
            foreach($campaign['lists'] as $list) {
                $l = DB::table('list')->where('id', $list)->first();
                $LAP = $this->getFilteredResultsFromDBObject($l);
                $results[] = $LAP;
                $totals[] = ['name' => $l->list, 'count' => count($LAP), 'type' => 'Lista'];
            }
        }

        if(isset($campaign['segments'])) {
            foreach($campaign['segments'] as $list) {
                $l = DB::table('segment')->where('id', $list)->first();
                $LAP = $this->getFilteredResultsFromDBObject($l);
                $results[] = $LAP;
                $totals[] = ['name' => $l->segment, 'count'  => count($LAP), 'type' => 'Segmento'];
            }
        }

        $results = $this->sanitizeCampaignPreview($results);
        if($return == 'table') {
            $results = $this->buildPreviewTable($results);
        } elseif ($return == 'emails') {
            $results = $this->getCampaignEmails($results);
        }

        $results = ['totals' => $totals, 'results' => $results];

        return $results;

    }

    private function sanitizeCampaignPreview($results)
    {
        $results = Helpers::flattenArray($results);
        $items = array();
        foreach($results as $item) {
            if(!in_array($item, $items))
                $items[] = $item;
        }

        return $items;
    }

    public function buildPreviewTable($results, $count = false)
    {
        $table = array();
        $i = 0;
        foreach($results as $client) {
            if($i % 2 === 0) {
                $class = 'even';
            } else {
                $class = 'odd';
            }
            $table[] = '<tr class="row '.$class.'">
                            <td>'.$client->name.' '.$client->lastname.'</td>
                            <td>'.$client->email.'</td>
                            <td>'.$client->company.'</td>
                            <td>'.$client->function.'</td></tr>';
                            
        }

        if($count)
            return ['totals' => count($table), 'results' => $table];

        return join('', $table);
    }

    public function buildSelectOptions($results)
    {
        $select = array();
        foreach($results as $result) {
            $select[] = '<option value="'.$result->id.'">'.$result->value.'</option>';
        }

        return join('', $select);
    }

    public function getCampaignEmails($results)
    {
        $emails = array();

       if(isset($results['name']))
           unset($results['name']);

       if(isset($results['emails']))
            $emails = $results['emails'];

        $clientEmails = array();

       if(isset($results['emails']))
           unset($results['emails']);

        foreach($results as $key => $values) {
            if($key == 'lists') {
                $table = 'list';
            } else {
                $table = 'segment';
            }

            foreach($values as $id) {
                $obj = DB::table($table)->where('id', $id)->first();
                $filtered = $this->getFilteredResultsFromDBObject($obj);
                foreach($filtered as $contact) {
                    $clientEmails[] = DB::table('email')->where('id_client', $contact->id)->lists('email');
                }
            }
        }
        if(!empty($clientEmails))
            $emails = array_filter($emails+$clientEmails);

        return $emails;
    }
        
}
