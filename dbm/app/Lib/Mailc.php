<?php

namespace App\Lib;

use NZTim\Mailchimp\GuzzleFactory;
use NZTim\Mailchimp\Mailchimp;


class Mailc extends Mailchimp
{
    use Mailchimp2, Mailchimp3;

    public function __construct($var = null, $var1 = null, $var2 = null){
        parent::__construct(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
    }


}