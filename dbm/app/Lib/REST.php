<?php

namespace App\Lib;

use Config;
use Requests;

class REST
{

    private $_user;
    private $_key;
    private $_base;
    private $_list;
    private $_options;

    /**
     * Class Constructor
     * initiates authentication values from the config files
     * $authenticate bool
     */
    public function __construct($webservice, $authenticate = true)
    {
        $this->_user = Config::get($webservice.'.user');
        $this->_key = Config::get($webservice.'.key');
        $this->_base = Config::get($webservice.'.base');
        $this->_list = Config::get($webservice.'.masterlist');

        if ($authenticate) {
            $this->authenticate();
        }
    }

    private function authenticate()
    {
        $this->_options['auth'] = [$this->_user, $this->_key];
    }

    public function sendGet($endpoint, $params = false)
    {
        $response = Requests::get($this->_base.$endpoint, array(), $this->_options);
        return $response;
    }

    public function getSubscriber($id)
    {
        $endpoint = 'lists/'.$this->_list.'/members/'.$id;
        $response = Requests::get(
            $this->_base.$endpoint,
            array(),
            $this->_options
        );
        return $response;
    }

    /*
    $subscriber = [
        "email_address" => "ricardo@codezone.com.br",
        "status" => "pending",
        "merge_fields" => [
            "FNAME" => "Ricardo",
            "LNAME" => "Moraleida"
        ]
    ];
     */
    public function createSubscriber($subscriber)
    {

        $endpoint = 'lists/'.$this->_list.'/members';

        $response = Requests::post(
            $this->_base.$endpoint,
            array(),
            json_encode($subscriber),
            $this->_options
        );
        return $response;
    }

    public function createMultipleSubscribers($subscriber, $emails)
    {
        $endpoint = 'lists/'.$this->_list.'/members';
        foreach ($emails as $em) {
               $subscriber['email_address'] = $em['email'];
               $response = $this->createSubscriber($subscriber);
               $result = json_decode($response->body);

            if ($result->status = 200) {
                $return[] = ['email' => $em['email'], 'id_mailchimp' => $result->id];
            } else {
                $return[] = ['email' => $em['email'], 'error' => $result];
            }
        }
    }

    public function updateSubscriber($id = false, $subscriber = false)
    {
        $endpoint = 'lists/'.$this->_list.'/members/'.$id;
        $response = Requests::patch(
            $this->_base.$endpoint,
            array(),
            json_encode($subscriber),
            $this->_options
        );
        return $response;
    }

    public function deleteSubscriber($id)
    {
        $endpoint = 'lists/'.$this->_list.'/members/'.$id;
        $response = Requests::delete(
            $this->_base.$endpoint,
            array(),
            $this->_options
        );
        return $response;
    }
}
