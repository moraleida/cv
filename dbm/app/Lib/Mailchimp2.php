<?php
/**
 * Created by PhpStorm.
 * User: kdbolds toreador@gmail.com
 * Date: 16/07/2015
 * Time: 13:25
 */

namespace App\Lib;


use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use NZTim\Mailchimp\GuzzleFactory;
use PhpSpec\Exception\Example\ExampleException;

trait Mailchimp2
{

    /**
     *  Insert Subscriber
     *
     * @param $user array user info [email, fname, lname]
     * @return mixed mailchimp Id
     */

    public function subscriberInsert($user)
    {
        $data = [
            'apikey' => config('mailchimp.key'),
            'id' => config('mailchimp.masterlist'),
            'email' => [
               'email' => $user['email'],
                'euid' => md5($user['email']),

            ],
            'merge_vars' => [
                'FNAME' => $user['fname'],
                'LNAME' => $user['lname'],
            ],
            'email_type' => 'html' ,
            'double_optin' => false,
            'update_existing' => true,
            'replace_interests' => false,
            'send_welcome' => false,
        ];


        $data = $this->callApi2('post', 'lists/subscribe.json', $data);
        $info = json_decode($data->getBody());
        if($data->getStatusCode() == 200)
            return $info->leid;
        throw new Exception('Mailchimp Api Status:'. $data->getStatusCode());
    }

    /**
     *
     * Edit Existing user
     *
     * @param $mailchimpId
     * @param $user user array info
     * @return true
     */
    public function subscriberEdit($oldEmail, $user)
    {
        $data = [
            'apikey' => config('mailchimp.key'),
            'id' => config('mailchimp.masterlist'),
            'email' => [
                'email' => $oldEmail,
                ],
            'merge_vars' => [
                'FNAME' => $user['fname'],
                'LNAME' => $user['lname'],
                'EMAIL' => $user['email'],
            ],
            'email_type' => 'html' ,
            'replace_interests' => false,

        ];
        $data = $this->callApi2('post', 'lists/update-member.json', $data);
        $info = json_decode($data->getBody());
        if($data->getStatusCode() == 200)
            return true;
        throw new Exception('Mailchimp Api Status:'. $data->getStatusCode());
    }

    /**
     * @param string$campaingName
     * @param string $subject
     * @param array $emails
     * @return array id/webid
     * @throws MailchimpException
     */
    public function campaignInsert($campaingName = null, $subject = 'Subject', array $emails = [])
    {
        if ($campaingName == null){
            $campaingName = 'Campanha'.date('Y-m-d\TH:i:sO');
        }


        $data = $this->callApi2('post', 'lists/static-segment-add.json', [
            'apikey' => config('mailchimp.key'),
            'id' => config('mailchimp.masterlist'),
            'name' => md5($campaingName.time()),
        ]);
        $segmentId = json_decode($data->getBody())->id;
        if(env('APP_DEBUG') == 1)
            Log::info("Seguimento criado Id: ".$segmentId);

        $data = $this->callApi2('post', 'lists/static-segment-members-add.json', [
            'apikey' => config('mailchimp.key'),
            'id' => config('mailchimp.masterlist'),
            'seg_id' => $segmentId,
            'batch' => $emails,
        ]);
        try {
            if (env('APP_DEBUG') == 1)
                Log::info("Usuarios inscritos no seguimento: " . json_decode($data->getBody())->success_count);

        } catch(Exception $e){

        }
        $data = $this->callApi2('post', 'campaigns/create.json', [
            'apikey' => config('mailchimp.key'),
            'type' => 'regular',
            'options' => [
                'list_id' => config('mailchimp.masterlist'),
                'title' => $campaingName,
                'authenticate' => 1,
                'subject' => $subject,
                'from_email' => config('mailchimp.from_email'),
                'from_name' => config('mailchimp.from_name'),
                'to_name' => config('mailchimp.to_name'),

            ],
            'segment_opts' => ['saved_segment_id' => $segmentId],
            'content' => [
                'html' => '',
                'text' => '',

            ],
        ]);
        $return = json_decode($data->getBody());
        try {
            if (env('APP_DEBUG') == 1)
                Log::info("Campanha Criada: " . $return->title . 'Id: ' . $return->id);
            return ['id' => $return->id, 'web_id' => $return->web_id];
        }catch (Exception $e){
            throw new Exception('Camapnha n�o foi criada. Erro Mailchimp: '.$data->getStatusCode());
        }


    }


    /**
     * @param string Campaing Id (not webid)
     * @param string$campaingName
     * @param string $subject
     * @param array $emails
     * @return array id/webid
     * @throws MailchimpException
     */

    public function campaignEdit($campaignId, $campaingName, $subject, array $emails)
    {

        $data = $this->callApi2('post', 'lists/static-segment-add.json', [
            'apikey' => config('mailchimp.key'),
            'id' => config('mailchimp.masterlist'),
            'name' => md5($campaingName),
        ]);
        $segmentId = json_decode($data->getBody())->id;
        $data =
        $data = $this->callApi2('post', 'lists/static-segment-members-add.json', [
            'apikey' => config('mailchimp.key'),
            'id' => config('mailchimp.masterlist'),
            'seg_id' => $segmentId,
            'batch' => $emails,
        ]);
        $this->callApi2('post', 'campaigns/update.json', [
            'apikey' => config('mailchimp.key'),
            'cid' => $campaignId,
            'name' => 'options',
            'value' => [
                    'title' => $campaingName,
                    'subject' => $subject,
            ]
        ]);

        $data = $this->callApi2('post', 'campaigns/update.json', [
            'apikey' => config('mailchimp.key'),
            'cid' => $campaignId,
            'name' => 'segment_opts',
            'value' => ['saved_segment_id' => $segmentId],
        ]);

        if(env('APP_DEBUG') == 1)
            Log::info("Campanha Editada");

        $return = json_decode($data->getBody());
        return [ 'id' => $return->data->id, 'web_id' => $return->data->web_id ];


    }

    /**
     *
     *  Remove Camapaign
     * @param $campaignId
     */
    public function campaignRemove($campaignId)
    {
        $data = $this->callApi2('post', 'campaigns/delete.json', [
            'apikey' => config('mailchimp.key'),
            'cid' => $campaignId,
        ]);
    }


    /**
     *
     * List All Campaigns.
     *
     * @return array campaign list
     */
    public function campaignList()
    {
        $data = $this->callApi2('post', 'campaigns/list.json', [
            'apikey' => config('mailchimp.key'),
        ]);

        $data = json_decode($data->getBody(), true);

        $i = 0;
        $return = [];
        if($data != null) {
            foreach ($data['data'] as $d) {
                $return[$i]['title'] = $d['title'];
                $return[$i]['schedule'] = $d['timewarp_schedule'];
                $return[$i]['send_time'] = $d['send_time'];
                $return[$i]['status'] = $d['status'];
                $return[$i]['id'] = $d['id'];
                $return[$i]['web_id'] = $d['web_id'];
                $return[$i]['subject'] = $d['subject'];
                $return[$i]['emails_sent'] = $d['emails_sent'];
                $return[$i]['segment_id'] = (array_key_exists('id', $d['saved_segment']) ? $d['saved_segment']['id'] : null);
                $return[$i]['comments_total'] = $d['comments_total'];
                $return[$i]['hard_bounces'] = (array_key_exists('hard_bounces', $d['summary']) ? $d['summary'] ['hard_bounces'] : 0);
                $return[$i]['abuse_reports'] = (array_key_exists('abuse_reports', $d['summary']) ? $d['summary'] ['abuse_reports'] : 0);
                $return[$i]['forwards'] = (array_key_exists('forwards', $d['summary']) ? $d['summary'] ['forwards'] : 0);
                $return[$i]['forwards_opens'] = (array_key_exists('forwards_opens', $d['summary']) ? $d['summary'] ['forwards_opens'] : 0);
                $return[$i]['last_open'] = (array_key_exists('last_open', $d['summary']) ? $d['summary'] ['last_open'] : 0);
                $return[$i]['clicks'] = (array_key_exists('clicks', $d['summary']) ? $d['summary'] ['clicks'] : 0);
                $return[$i]['opens'] = (array_key_exists('opens', $d['summary']) ? $d['summary'] ['opens'] : 0);
                $return[$i]['unique_clicks'] = (array_key_exists('unique_clicks', $d['summary']) ? $d['summary'] ['unique_clicks'] : 0);
                $return[$i]['clicks'] = (array_key_exists('clicks', $d['summary']) ? $d['summary'] ['clicks'] : 0);
                if (array_key_exists('unique_clicks', $d['summary'])) {
                    $return[$i]['open_rate'] = $d['summary']['industry']['open_rate'];
                    $return[$i]['click_rate'] = $d['summary']['industry']['click_rate'];
                    $return[$i]['bounce_rate'] = $d['summary']['industry']['bounce_rate'];
                    $return[$i]['unopen_rate'] = $d['summary']['industry']['unopen_rate'];
                    $return[$i]['unsub_rate'] = $d['summary']['industry']['unsub_rate'];
                    $return[$i]['abuse_rate'] = $d['summary']['industry']['abuse_rate'];
                } else {
                    $return[$i]['open_rate'] = 0;
                    $return[$i]['click_rate'] = 0;
                    $return[$i]['bounce_rate'] = 0;
                    $return[$i]['unopen_rate'] = 0;
                    $return[$i]['unsub_rate'] = 0;
                    $return[$i]['abuse_rate'] = 0;
                }
                $i++;
            }
        }
        return $return;
    }
    public function reportCampaign($campaignId)
    {
        $data = $this->callApi2('post', 'campaigns/list.json', [
            'apikey' => config('mailchimp.key'),
            'filters' => ['campaign_id' => $campaignId],
        ]);

        $data = json_decode($data->getBody(), true);

        if(array_key_exists('data', $data) != 1)
            return false;

        if(array_key_exists(0, $data['data']) != 1)
            return false;
        $data = $data['data'][0];
        if($data['status'] != 'sent')
            return false;
        $d = $data;
        $return['title'] = $d['title'];
        $return['schedule'] = $d['timewarp_schedule'];
        $return['send_time'] = $d['send_time'];
        $return['status'] = $d['status'];
        $return['id'] = $d['id'];
        $return['web_id'] = $d['web_id'];
        $return['subject'] = $d['subject'];
        $return['emails_sent'] = $d['emails_sent'];
        $return['segment_id'] = (array_key_exists('id',$d['saved_segment']) ? $d['saved_segment']['id']: null);
        $return['comments_total'] = $d['comments_total'];
        $return['hard_bounces'] = (array_key_exists('hard_bounces', $d['summary']) ? $d['summary'] ['hard_bounces'] : 0);
        $return['abuse_reports'] = (array_key_exists('abuse_reports', $d['summary']) ? $d['summary'] ['abuse_reports'] : 0);
        $return['forwards'] = (array_key_exists('forwards', $d['summary']) ? $d['summary'] ['forwards'] : 0);
        $return['forwards_opens'] = (array_key_exists('forwards_opens', $d['summary']) ? $d['summary'] ['forwards_opens'] : 0);
        $return['last_open'] = (array_key_exists('last_open', $d['summary']) ? $d['summary'] ['last_open'] : 0);
        $return['clicks'] = (array_key_exists('clicks', $d['summary']) ? $d['summary'] ['clicks'] : 0);
        $return['opens'] = (array_key_exists('opens', $d['summary']) ? $d['summary'] ['opens'] : 0);
        $return['unique_clicks'] = (array_key_exists('unique_clicks', $d['summary']) ? $d['summary'] ['unique_clicks'] : 0);
        $return['clicks'] = (array_key_exists('clicks', $d['summary']) ? $d['summary'] ['clicks'] : 0);
        if(array_key_exists('unique_clicks', $d['summary'])){
            $return['open_rate'] = $d['summary']['industry']['open_rate'];
            $return['click_rate'] = $d['summary']['industry']['click_rate'];
            $return['bounce_rate'] = $d['summary']['industry']['bounce_rate'];
            $return['unopen_rate'] = $d['summary']['industry']['unopen_rate'];
            $return['unsub_rate'] = $d['summary']['industry']['unsub_rate'];
            $return['abuse_rate'] = $d['summary']['industry']['abuse_rate'];
        }else{
            $return['open_rate']  = 0;
            $return['click_rate']  = 0;
            $return['bounce_rate']  = 0;
            $return['unopen_rate']  = 0;
            $return['unsub_rate']  = 0;
            $return['abuse_rate']  = 0;
        }


        return $return;

    }

    public function reportCampaignShort($campaignId)
    {
        $data = $this->callApi2('post', 'campaigns/list.json', [
            'apikey' => config('mailchimp.key'),
            'filters' => ['campaign_id' => $campaignId],
        ]);
        $data = json_decode($data->getBody(), true);

        if(array_key_exists('data', $data) != 1)
            return false;

        if(array_key_exists(0, $data['data']) != 1)
            return false;
        $data = $data['data'][0];
        if($data['status'] != 'sent')
            return false;
        $d = $data;
        $curr= time();
        $result = [
            'total' => ['emails_sent' => 0, 'unique_opens' => 0, 'recipients_click' => 0],
            24 => ['emails_sent' => 0, 'unique_opens' => 0, 'recipients_click' => 0],
            48 => ['emails_sent' => 0, 'unique_opens' => 0, 'recipients_click' => 0],
            72 => ['emails_sent' => 0, 'unique_opens' => 0, 'recipients_click' => 0]
        ];
        foreach($d['summary']['timeseries'] as $ts){
            $time = strtotime($ts['timestamp']);
            if($time > $curr - 24*60*60){
                $result['24']['emails_sent'] += $ts['emails_sent'];
                $result['24']['unique_opens'] += $ts['unique_opens'];
                $result['24']['recipients_click'] += $ts['recipients_click'];
            }
            if($time > $curr - 48*60*60){
                $result[48]['emails_sent'] += $ts['emails_sent'];
                $result[48]['unique_opens'] += $ts['unique_opens'];
                $result[48]['recipients_click'] += $ts['recipients_click'];
            }
            if($time > $curr - 72*60*60){
                $result[72]['emails_sent'] += $ts['emails_sent'];
                $result[72]['unique_opens'] += $ts['unique_opens'];
                $result[72]['recipients_click'] += $ts['recipients_click'];
            }
            $result['total']['emails_sent'] += $ts['emails_sent'];
            $result['total']['unique_opens'] += $ts['unique_opens'];
            $result['total']['recipients_click'] += $ts['recipients_click'];
        }
        return $result;

    }


    protected function callApi2($method, $endpoint, $data = [])
    {
        $guzzleFactory = new GuzzleFactory();
        $this->endpoint = "https://".config('mailchimp.dc').".api.mailchimp.com/2.0/";
        $this->client = $guzzleFactory->createClient([
            'base_uri' => $this->endpoint,
            'timeout'  => 2.0,
            'auth' => ['nztim/mailchimp', $this->apikey],
            'exceptions' => false
        ]);
        try {
            if($method == 'get') {
                $response = $this->client->get($endpoint);
            } else {
                $response = $this->client->$method($endpoint, ['body' => json_encode($data)]);
            }

        } catch (RequestException $e) {
            // All networking/comms errors caught here
            $message = $e->getMessage();
            $headers = $e->getRequest()->getHeaders();
            if(isset($headers['Authorization'])) {
                unset($headers['Authorization']);
            }
            $this->endpoint = "https://".config('mailchimp.dc').".api.mailchimp.com/3.0/";
            $this->client = $guzzleFactory->createClient([
                'base_uri' => $this->endpoint,
                'timeout'  => 2.0,
                'auth' => ['nztim/mailchimp', $this->apikey],
                'exceptions' => false
            ]);
            throw new MailchimpException('Mailchimp networking error: ' . $message . json_encode($headers));
        }
        $this->endpoint = "https://".env('MC_DC').".api.mailchimp.com/3.0/";

        $r = json_decode($response->getBody(), true);
        if(is_array($r)) {
            if (array_key_exists('status', $r)) {
                if ($r['status'] == 'error')
                    throw new Exception($r['error']);
            }
        }else{

        }
        return $response;
    }

}