<?php

function e($value)
{
    if ($value instanceof Htmlable) {
        return $value->toHtml();
    }

    if(is_array($value) && count($value) == 1)
        $value = array_shift($value);

    return htmlentities($value, ENT_QUOTES, 'UTF-8', false);
}
