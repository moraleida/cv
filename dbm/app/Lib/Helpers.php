<?php

namespace App\Lib;

use DB;

class Helpers {
    
    public static function get_names_list($table)
    {
        $start = array('' => trans('register.select'));
        $list = DB::table($table)->lists('name', 'id');
        return $start+$list;
    }

    public static function get_country_list() 
    {

        $start = array('' => trans('register.select'));
        $list = DB::table('country')->lists('country', 'id');
        //array_unshift($list, 'select');
        return $start+$list;
    }

    public static function get_state_list($countryid, $list = false) 
    {
        $start = [(object) ['id' => '', 'safe' => '0', 'state' => trans('register.select')]];

        if($countryid == 1) {
            $table = 'state';
        } else {
            $table = 'intl_state';
        }
        if($list)
            return DB::table($table)->where('id_country', $countryid)->list('state', 'id');

        $states = DB::table($table)->where('id_country', $countryid)->get(); 
        
        return $start+$states;
    }

    public static function get_city_list($countryid, $stateid = false) 
    {
        $start = [(object) ['id' => '', 'safe' => '0', 'city' => trans('register.select')]];

        if($countryid == 1) {
            $whereclause = 'id_state';
            $whereval = $stateid;
            $table = 'city';
        } else {
            $whereclause = 'id_country';
            $whereval = $countryid;
            $table = 'intl_city';
        }

        $cities =  DB::table($table)->where($whereclause, $whereval)->get();

        return $start+$cities;
    }

    /**
     * Flatten Multidimensional Arrays
     * ref: http://stackoverflow.com/a/1320156/1001109
     * @return unidimensional array
     * doesn't preserve keys
     */
    public static function flattenArray($array)
    {
        $return = array();
        array_walk_recursive(
            $array, function ($a) use (&$return) {
                $return[] = $a;
            }
        );

        return $return;
    }
    
     /**
     * Alias for Request::is(route(...))
     *
     * @return mixed
     * source: http://www.ltconsulting.co.uk/laravel-4-using-request-is-named-routes/
     */
    public static function isRoute()
    {
        $args = func_get_args();
        foreach($args as &$arg)
        {
            if(is_array($arg))
            {
                $route = array_shift($arg);
                $arg = ltrim(route($route, $arg, false), '/');
                continue;
            }
            $arg = ltrim(route($arg, [], false), '/');
        }
        return call_user_func_array(array(app('request'), 'is'), $args);
    }

}
