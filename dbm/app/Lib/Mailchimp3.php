<?php
/**
 * Created by PhpStorm.
 * User: kdbolds toreador@gmail.com
 * Date: 16/07/2015
 * Time: 13:24
 */

namespace App\Lib;


use Mockery\Exception;
use NZTim\Mailchimp\Mailchimp;

trait Mailchimp3
{



    /**
     *
     * Remove Mailchimp user
     *
     * @param $email string email
     * @return bool
     *
     */
    public function subscriberRemove($email)
    {
        $data = $this->callApi('delete', 'lists/'.config('mailchimp.masterlist').'/members/'.md5($email).'/');
        if($data->getStatusCode() < 300)
        return true;
        throw new Exception('Mailchimp Api Status:'. $data->getStatusCode());

    }


    /**
     *
     *   Report user stats
     *
     * @param $mailchimpId
     * @return array userinfo.
     */

    public function reportSubscriber($email)
    {
        $data = $this->callApi('get', 'lists/'.config('mailchimp.masterlist').'/members/'.md5($email).'/');
        if($data->getStatusCode() > 300)
            throw new Exception('Mailchimp Api Status:'. $data->getStatusCode());
        $data = json_decode($data->getBody());
        $return['avg_open_rate'] = $data->stats->avg_open_rate;
        $return['avg_click_rate'] = $data->stats->avg_click_rate;
        $return['member_rating'] = $data->member_rating;
        $return['status'] = $data->status;

        return $return;

    }
}