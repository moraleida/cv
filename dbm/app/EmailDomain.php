<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailDomain extends Model
{
    public $timestamps = false;
    protected $table = 'email_domain';
    //
}
