<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;
    protected $table = 'city';

    /**
     * Eloquent Relations
     */
    public function state()
    {
        return $this->belongsTo('App\State', 'id_state');
    }
}
