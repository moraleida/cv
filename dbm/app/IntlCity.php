<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntlCity extends Model
{
    public $timestamps = false;
    protected $table = 'intl_city';
    //
}
