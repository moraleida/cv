<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    public $domains;
    public $timestamps = false;
    protected $table = 'email';
    protected $fillable = ['email', 'type', 'mailchimp_id', 'id_client'];
    // = file(app_path('/Lib/domains.txt'))

    public function __construct($attributes = array())  
    {
        parent::__construct($attributes);

        $this->domains = str_replace('"', '', file(app_path('/Lib/domains.txt'), FILE_IGNORE_NEW_LINES));

        return;
    }
}
