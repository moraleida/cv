<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientHasCompany extends Model
{
    public $timestamps = false;
    protected $table = 'client_has_company';
    //
}
