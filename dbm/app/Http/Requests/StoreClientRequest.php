<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validationRules = [
            'name' => 'required|max:50',
            'lastname' => 'required|max:50',
            'identification' => 'required',
            'identification_type' => 'required',
            'birthday' => 'required|digits:1,31',
            'birthmonth' => 'required|digits:1,12',
            'associate_type' => 'required|in:nao-associado,coletivo,cooperador,emerito,entidade,individual,patrimonial',
        ];

        foreach($_POST['emailaddr'] as $key => $value) {
            $validationRules['emailaddr.'.$key] = 'required|email|max:50';
            $validationRules['emailtype.'.$key] = 'required|array|in:pessoal,comercial';
        }

        foreach($_POST['phoneint'] as $key => $value) {
            $validationRules['phoneint.'.$key] = 'required|digits:1,3';
            $validationRules['phonenum.'.$key.'.0'] = 'Regex:/^\(\d+\)\s\d{4,5}\-\d{4,5}$/';
            $validationRules['phonetype.'.$key.'.0'] = 'required|array|in:fixo,celular';
        }

        foreach($_POST['business'] as $key => $value) {
            $validationRules['business.zip'] = 'required|numeric';
            $validationRules['business.country'] = 'exists:country,id';
        }

        foreach($_POST['company'] as $key => $value) {
            $validationRules['company.name'] = 'required';
            $validationRules['company.role'] = 'required';
            $validationRules['company.area'] = 'required';
            $validationRules['company.career'] = 'required';
        }

        $company_phones_int = $_POST['company']['phoneint'];
        $company_phones_num = $_POST['company']['phonenum'];
        $company_phones_typ = $_POST['company']['phonetype'];

        foreach($company_phones_int as $key => $value) {
            $validationRules['company.phoneint.'.$key] = 'required|digits:1,3';
            $validationRules['company.phonenum.'.$key.'.0'] = 'Regex:/^\(\d+\)\s\d{4,5}\-\d{4,5}$/';
            $validationRules['company.phonetype.'.$key] = 'required|array|in:fixo,celular';
        }
        
//        dd($validationRules);

        return $validationRules;
    }
}
