<?php

/**
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
Route::get('/reset', ['as' => 'auth.reset', 'uses' => 'Auth\PasswordController@getReset']);

// index interna e reports
Route::get('/home', ['as' => 'home', 'uses' => 'ReportController@home']);
Route::get('/relatorios', ['as' => 'report.list', 'uses' => 'ReportController@index']);

// list
Route::get('/listas', ['as' => 'list.list', 'uses' => 'ListsController@index']);
Route::get('/listas/cadastro', ['as' => 'list.create', 'uses' => 'ListsController@create']);
Route::post('/listas/cadastro', ['as' => 'list.store', 'uses' => 'ListsController@store']);
Route::get('/listas/editar/{id}', ['as' => 'list.edit', 'uses' => 'ListsController@editList']);
Route::post('/listas/editar/{id}', ['as' => 'list.update', 'uses' => 'ListsController@updateList']);
Route::post('/listas/exportar/{id}', ['as' => 'list.export', 'uses' => 'ListsController@export']);
Route::post('/listas/duplicar/{id}', ['as' => 'list.duplicate', 'uses' => 'ListsController@duplicateList']);
Route::post('/listas/excluir/{id}', ['as' => 'list.delete', 'uses' => 'ListsController@destroyList']);
// segment
Route::get('/segmentos/cadastro', ['as' => 'segment.create', 'uses' => 'ListsController@create']);
Route::post('/segmentos/cadastro', ['as' => 'segment.store', 'uses' => 'ListsController@store']);
Route::get('/segmentos/editar/{id}', ['as' => 'segment.edit', 'uses' => 'ListsController@editSegment']);
Route::post('/segmentos/editar/{id}', ['as' => 'segment.update', 'uses' => 'ListsController@updateSegment']);
Route::post('/segmentos/exportar/{id}', ['as' => 'segment.export', 'uses' => 'ListsController@export']);
Route::post('/segmentos/duplicar/{id}', ['as' => 'segment.duplicate', 'uses' => 'ListsController@duplicateSegment']);
Route::post('/segmentos/excluir/{id}', ['as' => 'segment.delete', 'uses' => 'ListsController@destroySegment']);

// campaign
Route::get('/campanhas', ['as' => 'campaign.list', 'uses' => 'CampaignController@index']);
Route::get('/campanhas/cadastro', ['as' => 'campaign.create', 'uses' => 'CampaignController@create']);
Route::get('/campanhas/editar/{id}', ['as' => 'campaign.edit', 'uses' => 'CampaignController@edit']);
Route::post('/campanhas/duplicar/{id}', ['as' => 'campaign.duplicate', 'uses' => 'CampaignController@duplicate']);
Route::post('/campanhas/excluir/{id}', ['as' => 'campaign.delete', 'uses' => 'CampaignController@destroy']);

// user
Route::get('/usuarios', ['as' => 'user.list', 'uses' => 'UserController@index']);
Route::get('/usuarios/editar/{id}', ['as' => 'user.edit', 'uses' => 'UserController@edit']);
Route::post('/usuarios/editar/{id}', ['as' => 'user.update', 'uses' => 'UserController@update']);
Route::post('/usuarios/duplicar/{id}', ['as' => 'user.duplicate', 'uses' => 'UserController@duplicate']);
Route::post('/usuarios/excluir/{id}', ['as' => 'user.delete', 'uses' => 'UserController@delete']);
Route::post('/usuarios/destruir/{id}', ['as' => 'user.destroy', 'uses' => 'UserController@destroy']);
Route::get('/usuarios/cadastro', ['as' => 'user.register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('/usuarios/cadastro',['as' => 'user.register', 'uses' => 'Auth\AuthController@postRegister']);

// client
Route::get('/clientes', ['as' => 'client.list', 'uses' => 'ClientController@index']);
Route::get('/clientes/editar/{id}', ['as' => 'client.edit', 'uses' => 'ClientController@edit']);
Route::get('/clientes/{source}/{id}', ['as' => 'client.list.from.source', 'uses' => 'ClientController@index']);
Route::get('/clientes/cadastro', ['as' => 'client.register', 'uses' => 'ClientController@create']);
Route::get('/clientes/novo', ['as' => 'client.add', 'uses' => 'ClientController@add']);
Route::post('/clientes/novo', ['as' => 'client.add', 'uses' => 'ClientController@store']);
Route::get('/clientes/register', ['as' => 'client.register_en', 'uses' => 'ClientController@create']);
Route::get('/clientes/importar', ['as' => 'import', 'uses' => 'ClientController@import']);
Route::post('/clientes/cadastro', ['as' => 'client.register', 'uses' => 'ClientController@store']);
Route::post('/clientes/excluir/{id}', ['as' => 'client.delete', 'uses' => 'ClientController@delete']);

// support
Route::get('/suporte', ['as' => 'support', 'uses' => 'SupportController@index']);
Route::post('/suporte', ['as' => 'support', 'uses' => 'SupportController@store']);

// ajax calls
Route::group(['prefix' => 'api'], function()
{
    // Cadastro Cliente
    Route::get('{id}/states', ['as' => 'ajax.getstates', 'uses' => 'ClientController@ajax']);
    Route::get('{id}/cities', ['as' => 'ajax.getcities', 'uses' => 'ClientController@ajax']);
    Route::get('state/{state}/city/{city}', ['as' => 'ajax.getcityname', 'uses' => 'ClientController@ajaxGetCity']);
    Route::get('{id}/citiesintl', ['as' => 'ajax.getcitiesintl', 'uses' => 'ClientController@ajax']);
    Route::post('image/upload', ['as' => 'ajax.postImage', 'uses' => 'ClientController@ajaxImages']);
    Route::post('image/social', ['as' => 'ajax.postSocialImage', 'uses' => 'ClientController@ajaxSocialImages']);
    Route::post('image/remove', ['as' => 'ajax.removeImage', 'uses' => 'ClientController@ajaxRemoveImage']);

    // Lista
    Route::get('lista/filtro/{name}', ['as' => 'ajax.geListFilters', 'uses' => 'ListsController@ajax']);
    Route::post('lista/filtrada/', ['as' => 'ajax.getListFiltered', 'uses' => 'ListsController@ajax']);

    // Campanha
    Route::post('campanhas/preview/', ['as' => 'ajax.previewCampaign', 'uses' => 'ListsController@ajaxGetCampaignPreview']);
    Route::post('campanhas/nova/', ['as' => 'ajax.previewCampaign', 'uses' => 'CampaignController@store']);
    
    // Email
    Route::get('email/domains/', ['as' => 'ajax.getEmailDomains', 'uses' => 'EmailController@ajaxGetDomains']);

});

// mailchimp tests
Route::get('mc/subscriber/insert/', ['as' => 'mc.subscriberInsert', 'uses' => 'MCController@subscriberInsert']);
Route::get('mc/subscriber/edit/', ['as' => 'mc.subscriberEdit', 'uses' => 'MCController@subscriberEdit']);
Route::get('mc/subscriber/remove/', ['as' => 'mc.subscriberRemove', 'uses' => 'MCController@subscriberRemove']);

Route::get('mc/campaign/insert', ['as' => 'mc.campaignInsert', 'uses' => 'MCController@campaignInsert']);
Route::get('mc/campaign/edit', ['as' => 'mc.campaignEdit', 'uses' => 'MCController@campaignEdit']);
Route::get('mc/campaign/remove', ['as' => 'mc.campaignRemove', 'uses' => 'MCController@campaignRemove']);
Route::get('mc/campaign/list', ['as' => 'mc.campaignList', 'uses' => 'MCController@campaignList']);

Route::get('mc/report/campaign', ['as' => 'mc.reportCampaign', 'uses' => 'MCController@reportCampaign']);
Route::get('mc/report/campaign/short/', ['as' => 'mc.reportCampaignShort', 'uses' => 'MCController@reportCampaignShort']);
Route::get('mc/report/subscriber/', ['as' => 'mc.reportSubscriber', 'uses' => 'MCController@reportSubscriber']);
