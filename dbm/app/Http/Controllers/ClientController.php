<?php

namespace App\Http\Controllers;

use DB;
use App;
use App\Client;
use App\Lists;
use App\Lib\Helpers;
use App\Lib\REST;
use Illuminate\Http\Request;
use App\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Requests\StoreClientRequest;

class ClientController extends Controller
{

    protected $attr;

    public function setupVars()
    {
        $this->attr  = Client::getIdentifiers();
        $this->uploadPath = app_path('Uploads');
        $this->imagesPath = public_path('img');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $clientInstance = new Client;
        $l = new Lists;
        $segments = $request->segments();
        $this->attr['clients'] = $clientInstance->getClientList($request);
        $this->attr['lists'] = $l->getListOfLists(false);
        $this->attr['segments'] = $l->getListOfLists(true);
        if (count($segments) > 1) {
              $this->attr['pagetitle'] = $clientInstance->altPageTitle;
        }

        return view('client.list', $this->attr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        $segments = $request->segments();
        if($segments[1] == 'register')
            App::setLocale('en');

        $this->attr['strings']['select'] = trans('register.select');
        $this->attr['strings']['personal'] = trans('register.personal');
        $this->attr['strings']['business'] = trans('register.business');

        return view('client.create', $this->attr);
    }

    public function add(Request $request)
    {

        $segments = $request->segments();
        if($segments[1] == 'register')
            App::setLocale('en');

        $this->attr['strings']['select'] = trans('register.select');
        $this->attr['strings']['personal'] = trans('register.personal');
        $this->attr['strings']['business'] = trans('register.business');
        $this->attr['headerbtn'] = false;
        $this->attr['origins'] = DB::table('origin')->lists('name', 'id');
        $this->attr['interest_groups'] = DB::table('interest_group')->lists('id', 'name');

        return view('client.add', $this->attr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        
        $cadastrointerno = $request->input('cadastrointerno');

        // Cria e salva o contato
        $client = new Client;
        $client = $client->storeClient($request, $client, $cadastrointerno);

        if($cadastrointerno) {
            return redirect()->route('client.list');
        } else {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $client = new Client;
        $this->attr['strings']['select'] = trans('register.select');
        $this->attr['strings']['personal'] = trans('register.personal');
        $this->attr['strings']['business'] = trans('register.business');
        $this->attr['client'] = $client->getClientToEdit($id);
        $this->attr['origins'] = DB::table('origin')->lists('name', 'id');
        $this->attr['interest_groups'] = DB::table('interest_group')->lists('id', 'name');
        $this->attr['headerbtn'] = false;

        return view('client.edit', $this->attr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $remove = DB::table('client')->where('id', $id)->delete();

        return redirect()->route('client.list');
    }
    /**
     * Carrega a tela de importação de contatos.
     *
     */
    public function import()
    {
        //
        $this->attr['headerbtn'] = false;
        $this->attr['pagetitle'] = 'Importar contatos';
        return view('client.import', $this->attr);
    }

    /**
     * Handle ajax calls for helper classes and methods
     *
     * @param  Request $request
     * @return Response
     */
    public function ajax(Request $request)
    {
        $segments = $request->segments();
        $endpoint = array_pop($segments);

        if($endpoint == 'states') {
            $states = Helpers::get_state_list($segments[1]);
            return response()->json($states);
        } elseif($endpoint == 'cities') {
            $cities = Helpers::get_city_list(1,$segments[1]);
            return response()->json($cities);
        } else {
            $cities = Helpers::get_city_list($segments[1]);
            return response()->json($cities);
        }
    }

    public function ajaxSocialImages()
    {
        $headers = apache_request_headers();
        $filename = $headers['X-CSRF-TOKEN'];
        $path = app_path('Uploads/'.$filename);

        if(empty($_POST) || count($_POST) > 1)
            return response()->json($_POST);
        
        if(isset($_POST['img']))
            $stream = file_get_contents($_POST['img']);
            
        if(file_put_contents($path, $stream))
            $file = getimagesize($path);

        if($file) {
            return response()->json(['filename' => $filename]);
        }

    }

    public function ajaxImages(Request $request)
    {
        $headers = apache_request_headers();
        $filename = $headers['X-Csrf-Token'];

        if(empty($_FILES) || $_FILES['file']['error'])
            return response()->json($_FILES);

        move_uploaded_file($_FILES['file']['tmp_name'], app_path('Uploads/'.$filename)); 
        return response()->json(['filename' => $filename]);
    }

    public function ajaxRemoveImage(Request $request)
    {
        $headers = apache_request_headers();
        $filename = $headers['X-Csrf-Token'];

        if(unlink(app_path('Uploads/'.$request->img)))
            return response()->json(['status' => 'success']);

        return response()->json(['status' => 'failed']);
    }

    public function ajaxGetCity(Request $request)
    {
        $segments = $request->segments();
        
        $state = DB::table('state')->where('safe', $segments[2])->first();
        $city = DB::table('city')
            ->where('city', urldecode($segments[4]))
            ->where('id_state', $state->id)
            ->first();
        return response()->json($city);
    }
}
