<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Report;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    protected $attr;

    public function setupVars()
    {
        $this->attr  = Report::getIdentifiers();
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function home()
    {
        $this->attr['clients'] = DB::table('client')->count();
        $this->attr['lists'] = DB::table('list')->count();
        $this->attr['campaigns'] = DB::table('campaign')->count();
        return view('report.home', $this->attr);
    }

    public function index()
    {
        return view('report.list', $this->attr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
