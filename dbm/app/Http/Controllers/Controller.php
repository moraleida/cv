<?php

namespace App\Http\Controllers;

use Auth;
use View;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class ShareController extends BaseController
{
    public function __construct()
    {
        $current_user = Auth::user();
        View::share('current_user', $current_user);

        $this->setupVars();
    }

    public function setupVars() {}
}

abstract class Controller extends ShareController
{
    use DispatchesJobs, ValidatesRequests;
}
