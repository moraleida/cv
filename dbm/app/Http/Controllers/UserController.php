<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    protected $pagetitle = 'usuários';
    protected $headerbtn = true;
    protected $headerbtntext = 'criar usuário';
    protected $headerbtnroute = 'user.register';
    protected $attr;

    public function setupVars()
    {
        $this->attr  = ['pagetitle' => $this->pagetitle, 
                        'headerbtn' => $this->headerbtn,
                        'headerbtntext' => $this->headerbtntext,
                        'headerbtnroute' => $this->headerbtnroute];
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $alluser = DB::table('user')->paginate(15);
        $this->attr['user'] = $alluser;
//        if( !empty($user) )
            return view('user.list', $this->attr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $thisuser = DB::table('user')->where('id', $id)->first();
        return view('user.edit', ['user' => $thisuser]);
    }

    public function duplicate($id)
    {
        $thisuser = DB::table('user')->where('id', $id)->first();
        $thisuser->id = null;
        $thisuser->username .= '_'.time();
        $thisuser->password = '';
        DB::table('user')->insert(get_object_vars($thisuser));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        if( empty($input['password']) )
            unset($input['password']);
        dd($input); 
    }

    /**
     * Soft-delete. Changes user role to inactive so it remains on the system
     * but is no longer available and doesn't have access to any data.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        $delete = DB::table('user')->where('id', $id)->update(['role'=>'inativo']); 
        $response = ['id' => $id, 'role' => 'inactive', 'delete' => $delete];

        return response()->json( $response );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $destroy = DB::table('user')->where('id', $id)->delete();
        $response = ['id' => $id, 'destroy' => $destroy];
        return response()->json( $response );
    }
}
