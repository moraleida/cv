<?php

namespace App\Http\Controllers;

use Mail;
use Auth;
use Illuminate\Http\Request;
use App\Support;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SupportController extends Controller
{

    protected $pagetitle = 'suporte';
    protected $headerbtn = false;
    protected $headerbtntext = 'criar lista';
    protected $headerbtnroute = 'list.create';
    protected $attr;

    public function setupVars()
    {
        $this->attr  = ['pagetitle' => $this->pagetitle, 
                        'headerbtn' => $this->headerbtn,
                        'headerbtntext' => $this->headerbtntext,
                        'headerbtnroute' => $this->headerbtnroute];
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('support.index', $this->attr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $support = new Support;
        $support->message = $request->message;
        $support->subject = $request->subject;
        $support->id_user = $user->id;
        $support->save();

        return redirect()->route('report.home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
