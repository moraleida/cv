<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Illuminate\Http\Request;
use App\Campaign;
use App\Lists;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CampaignController extends Controller
{
    protected $attr;

    public function setupVars()
    {
        $this->attr  = Campaign::getIdentifiers();
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $l = new Lists;
        $campaigns = DB::table('campaign')->orderBy('created_at', 'desc')->paginate(20);

        if (empty($campaigns) || $campaigns->total() == 0)
            return view('campaign.empty', $this->attr);

        $this->attr['lists'] = $l->getListOfLists(false);
        $this->attr['segments'] = $l->getListOfLists(true);
        $this->attr['campaigns'] = $campaigns;
        return view('campaign.list', $this->attr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $l = new Lists;
        $this->attr['lists'] = $l->getListOfLists(false, 'adicionar');
        $this->attr['segments'] = $l->getListOfLists(true, 'adicionar');
        $this->attr['headerbtn'] = false;
        return view('campaign.create', $this->attr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $campaign = new Campaign;
        $campaign = $campaign->storeNewCampaign($request->input());
        
        return response()->json($campaign);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $l = new Lists;
        $this->attr['campaign'] = DB::table('campaign')
            ->leftJoin('campaign_has_list', 'campaign.id', '=', 'campaign_has_list.id_campaign')
            ->leftJoin('campaign_has_segment', 'campaign.id', '=', 'campaign_has_segment.id_campaign')
            ->where('campaign.id', $id)
            ->first();
        $this->attr['lists'] = $l->getListOfLists(false, 'adicionar');
        $this->attr['segments'] = $l->getListOfLists(true, 'adicionar');
        $this->attr['loadedLists'] = DB::table('campaign_has_list')->where('id_campaign', $id)->lists('id_list');
        $this->attr['loadedSegments'] = DB::table('campaign_has_segment')->where('id_campaign', $id)->lists('id_segment');
        $this->attr['loadedEmails'] = unserialize($this->attr['campaign']->extra_emails);
        $this->attr['headerbtn'] = false;
//        dd($this->attr);

        return view('campaign.edit', $this->attr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
