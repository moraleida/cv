<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Config;
use App\Lists;
use App\Lib\REST;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ListsController extends Controller
{

    protected $pagetitle = 'listas';
    protected $headerbtn = true;
    protected $headerbtntext = 'criar lista';
    protected $headerbtnroute = 'list.create';
    protected $attr;
    protected $Lists;

    public function setupVars()
    {
        $this->attr  = ['pagetitle' => $this->pagetitle, 
                        'headerbtn' => $this->headerbtn,
                        'headerbtntext' => $this->headerbtntext,
                        'headerbtnroute' => $this->headerbtnroute];

        $this->Lists = new Lists;
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $current = 1;
        if($request->query('page'))
            $current = $request->query('page');

        $list = $this->Lists->getListsWithSegments(15, $current);

        if($list->total() < 1)
            return view('list.empty');

        $this->attr['lists'] = $list;

        return view('list.list', $this->attr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $this->attr['headerbtn'] = false;
        $this->attr['pagetitle'] .= ' - nova';
        $this->attr['filterColumns'] = Lists::getFilterColumns();

        
        if ($request->query('parent')) {
            $this->attr['pagetitle'] = 'Segmento - novo';
            $this->attr['parent'] = $request->query('parent');
            $this->getListToEdit($this->attr['parent']);
            return view('segment.create', $this->attr);
        }

        return view('list.create', $this->attr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $parentList = $request->input('parent');

        $lists = new Lists;
        $filters = $lists->sanitizeFiltersToSave($_POST['filter']);

        if(!$parentList) {
            $lists->list = $request->input('name');
            $lists->rules = $request->input('filter')['rules'];
            $lists->filters = serialize($filters);
            $lists->id_user = $user->id;
            $lists->save();
        } else {
            $segment = DB::table('segment')->insert(
                [
                    'segment' => $request->input('name'),
                    'filters' => serialize($filters),
                    'id_user' => $user->id,
                    'id_list' => $parentList,
                ]
            );

        }

        // $rest = new REST('mailchimp');

        return redirect()->route('list.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function editList(Request $request, $id)
    {
        $l = new Lists;
        $list = DB::table('list')->where('id',$id)->first();
        $filters['filters'] = unserialize($list->filters);
        $filters['rule'] = $list->rules;
        $results = $l->getFilteredResults($filters, true);
        $this->attr['list'] = $l->getListFiltersColumns($list);
        $this->attr['headerbtn'] = false;
        $this->attr['filterColumns'] = Lists::getFilterColumns();

        return view('list.edit', $this->attr);
    }

    public function editSegment(Request $request, $id)
    {
        $l = new Lists;
        $segment = DB::table('segment')->where('id',$id)->first();
        $list = DB::table('list')->where('id',$segment->id_list)->first();
        $filters['filters'] = array_merge(unserialize($list->filters), unserialize($segment->filters));
        $filters['rule'] = $list->rules;
        $results = $l->getFilteredResults($filters, true);
        $this->attr['list'] = $l->getListFiltersColumns($list, $segment);
        $this->attr['list']->skip = count(unserialize($list->filters));
        $this->attr['list']->parent = $list->list;
        $this->attr['list']->rule = $list->rules;
        $this->attr['headerbtn'] = false;
        $this->attr['pagetitle'] = 'Segmento - editar';
        $this->attr['filterColumns'] = Lists::getFilterColumns();

        if ($request->query('parent')) {
            $this->attr['pagetitle'] = 'Segmento - novo';
            $this->attr['parent'] = $request->query('parent');
            $this->getListToEdit($this->attr['parent']);
            return view('segment.create', $this->attr);
        }
        return view('segment.edit', $this->attr);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateList(Request $request, $id)
    {
        $user = Auth::user();
        $lists = new Lists;
        $filters = $lists->sanitizeFiltersToSave($request->input('filter'));

        $lists->exists = true;
        $lists->id = $id;
        $lists->list = $request->input('name');
        $lists->rules = $request->input('filter')['rules'];
        $lists->filters = serialize($filters);
        $lists->id_user = $user->id;
        $lists->save();

        return redirect()->route('list.list');
    }

    public function updateSegment(Request $request, $id)
    {
        $user = Auth::user();
        $lists = new Lists;
        $filters = $lists->sanitizeFiltersToSave($request->input('filter'));

        $segment = DB::table('segment')->where('id', $id)->update(
            [
                'segment' => $request->input('name'),
                'filters' => serialize($filters),
                'id_user' => $user->id,
            ]
        );

        return redirect()->route('list.list');
        
    }


    public function duplicateList(Request $request, $id)
    {
        $user = Auth::user();
        $origin = DB::table('list')->where('id', $id)->first();

        $lists = new Lists;
        if ($request->input('name') != '' && $request->input('name') != $origin->list) {
            $lists->list = $request->input('name');
        } else {
            $lists->list = $origin->list. '(cópia)';
        }

        $lists->rules = $origin->rules;
        $lists->filters = $origin->filters;
        $lists->id_user = $user->id;
        $lists->save();
        
        return redirect()->route('list.list');
    }

    public function duplicateSegment(Request $request, $id)
    {
        $user = Auth::user();
        $origin = DB::table('segment')->where('id', $id)->first();

        if ($request->input('name') != '' && $request->input('name') != $origin->list) {
            $name = $request->input('name');
        } else {
            $name = $origin->list. '(cópia)';
        }

        $segment = DB::table('segment')->insert(
            [
                'segment' => $name,
                'filters' => $origin->filters,
                'id_user' => $user->id,
                'id_list' => $origin->id_list,
            ]
        );
        
        return redirect()->route('list.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroyList($id)
    {
        $remove = DB::table('list')->where('id', $id)->delete();

        return redirect()->route('list.list');
    }

    public function destroySegment($id)
    {
        $remove = DB::table('segment')->where('id', $id)->delete();

        return redirect()->route('list.list');
    }

    private function getListToEdit($id)
    {
        $l = new Lists;
        $list = DB::table('list')->where('id',$id)->first();
        $filters['filters'] = unserialize($list->filters);
        $filters['rule'] = $list->rules;
        $results = $l->getFilteredResults($filters, true);
        $this->attr['list'] = $l->getListFiltersColumns($list);
        $this->attr['name'] = $list->list;

        return ['list' => $list, 'results' => $results];
    }

    /**
     * Handle ajax calls for helper classes and methods
     *
     * @param  Request $request
     * @return Response
     */
    public function ajax(Request $request)
    {
        $segments = $request->segments();
        $endpoint = $segments[count($segments)-1];
        $lists = new Lists;

        if($endpoint == 'filtrada') {
            $results = $lists->getFilteredResults($_POST);
            $results = $lists->buildPreviewTable($results, true);
        } else {
            $results = Lists::getFilterOptions($endpoint);
            $results = $lists->buildSelectOptions($results);
        }
        
        return response()->json($results);
    }

    public function ajaxGetCampaignPreview(Request $request)
    {
        $lists = new Lists;
        $preview = $lists->getCampaignPreview($request->input());

        return $preview;
//        return response()->json($preview);
    }
}
