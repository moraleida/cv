<?php

namespace App\Http\Controllers;


use DB;
use Illuminate\Http\Request;
use App\Report;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lib\Mailc;
use NZTim\Mailchimp\GuzzleFactory;

class MCController extends Controller {

    public function subscriberInsert(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $leid = $mailC->subscriberInsert([
            'fname' => 'Danilo',
            'lname' => 'Sabbagh',
            'email' => 'tore.ador@gmail.com'
        ]);

        echo 'User Created '. $leid;
    }
    public function subscriberEdit(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $mailC->subscriberEdit('tore.ador@gmail.com',[
            'fname' => 'Dani',
            'lname' => 'Matoso',
            'email' => 'toreador@gmail.com'
        ]);
        echo 'Email changed from tore.ador@gmail.com to toreador@gmail.com';
    }
    public function subscriberRemove(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $mailC->subscriberRemove('toreador@gmail.com');
        echo 'Email toreador@gmail.com removed';
    }

    public function campaignInsert(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $mailC->campaignInsert('Campanha Feliz'.rand(0,95554), "Subject Legal", [
            ['email' => 'toreador@gmail.com'],
            ['email' => 'email1@moraleida.me'],
            ['email' => 'email2@moraleida.me'],
        ]);

    }

    public function campaignEdit(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $mailC->campaignEdit('1da5bfb917', 'Campanha Feliz'.rand(0,95554), "Subject Diferente", [
            ['email' => 'toreador@gmail.com'],
            ['email' => 'email1@moraleida.me'],
            ['email' => 'email2@moraleida.me'],
        ]);

    }

    public function campaignRemove(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $mailC->campaignRemove('1da5bfb917');
        echo 'Campanha removida';

    }
    public function campaignList(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        foreach($mailC->campaignList() as $c){
            echo "<pre>";
            echo "Nome: ".$c['title'];
            echo "\nID: ".$c['id'];
            echo "\nAssunto: ".$c['subject'];
            echo "\nStatus: ".$c['status'];
            echo "\nData de Envio: ". $c['send_time'];
            echo "\nEmails Enviados: " . $c['emails_sent'];
            echo "\nEmails Abertos: " . $c['opens'];
            echo "\nEmails Retornados: " . $c['hard_bounces'];
            echo "\n\n--------------//---------------\n\n";


            /*
             *  $return[$i]['title'] = $d['title'];
            $return[$i]['schedule'] = $d['timewarp_schedule'];
            $return[$i]['send_time'] = $d['send_time'];
            $return[$i]['status'] = $d['status'];
            $return[$i]['id'] = $d['id'];
            $return[$i]['web_id'] = $d['web_id'];
            $return[$i]['subject'] = $d['subject'];
            $return[$i]['emails_sent'] = $d['emails_sent'];
            $return[$i]['segment_id'] = (array_key_exists('id',$d['saved_segment']) ? $d['saved_segment']['id']: null);
            $return[$i]['comments_total'] = $d['comments_total'];
            $return[$i]['hard_bounces'] = (array_key_exists('hard_bounces', $d['summary']) ? $d['summary'] ['hard_bounces'] : 0);
            $return[$i]['abuse_reports'] = (array_key_exists('abuse_reports', $d['summary']) ? $d['summary'] ['abuse_reports'] : 0);
            $return[$i]['forwards'] = (array_key_exists('forwards', $d['summary']) ? $d['summary'] ['forwards'] : 0);
            $return[$i]['forwards_opens'] = (array_key_exists('forwards_opens', $d['summary']) ? $d['summary'] ['forwards_opens'] : 0);
            $return[$i]['last_open'] = (array_key_exists('last_open', $d['summary']) ? $d['summary'] ['last_open'] : 0);
            $return[$i]['clicks'] = (array_key_exists('clicks', $d['summary']) ? $d['summary'] ['clicks'] : 0);
            $return[$i]['unique_clicks'] = (array_key_exists('unique_clicks', $d['summary']) ? $d['summary'] ['unique_clicks'] : 0);
            $return[$i]['clicks'] = (array_key_exists('clicks', $d['summary']) ? $d['summary'] ['clicks'] : 0);
            if(array_key_exists('unique_clicks', $d['summary'])){
            $return[$i]['open_rate'] = $d['summary']['industry']['open_rate'];
            $return[$i]['click_rate'] = $d['summary']['industry']['click_rate'];
            $return[$i]['bounce_rate'] = $d['summary']['industry']['bounce_rate'];
            $return[$i]['unopen_rate'] = $d['summary']['industry']['unopen_rate'];
            $return[$i]['unsub_rate'] = $d['summary']['industry']['unsub_rate'];
            $return[$i]['abuse_rate'] = $d['summary']['industry']['abuse_rate'];
             */

        }


    }

    public function reportCampaign(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $data = $mailC->reportCampaign('9a14250543');
        echo "<pre>";
        print_r($data);

    }

    public function reportCampaignShort(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $data = $mailC->reportCampaignShort('9a14250543');
        echo "<pre>";
        print_r($data);
    }
    public function reportSubscriber(){
        $mailC = new Mailc(config('mailchimp.key'), config('mailchimp.dc'), new GuzzleFactory());
        $data = $mailC->reportSubscriber('toreador@gmail.com');
        print_r($data);
    }

}