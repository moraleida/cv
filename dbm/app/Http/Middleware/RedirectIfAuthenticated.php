<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Decide what to do if we're already on the login screen
        if ($request->url() == route('login')) {
             if (!$this->auth->check())
                return $next($request);

             return redirect(route('home'));
        }

        // Redirect all unauthenticated requests to the login screen
        // except if we're requesting the Registration form.
        // or this is an ajax call
        if (!$this->auth->check() && !$request->ajax() &&
            $request->url() != route('auth.reset') &&
            $request->url() != route('ajax.postImage') &&
            $request->url() != route('client.register') &&
            $request->url() != route('client.register_en'))
            return redirect(route('login'));

        return $next($request);
    }
}
