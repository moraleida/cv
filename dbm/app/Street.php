<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    public $timestamps = false;
    protected $table = 'street';

    /**
     * Eloquent Relations
     */
    public function neighborhood()
    {
        return $this->belongsTo('App\Neighborhood', 'id_neighborhood');
    }
}
