<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterestArea extends Model
{
    public $timestamps = false;
    protected $table = 'interest_area';
    //
}
