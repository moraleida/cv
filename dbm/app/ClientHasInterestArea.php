<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientHasInterestArea extends Model
{
    public $timestamps = false;
    protected $table = 'client_has_interest_area';
    //
}
