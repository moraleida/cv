<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientHasInterestGroup extends Model
{
    public $timestamps = false;
    protected $table = 'client_has_interest_group';
    //
}
