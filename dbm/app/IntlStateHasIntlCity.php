<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntlStateHasIntlCity extends Model
{
    public $timestamps = false;
    protected $table = 'intl_state_has_intl_city';
    //
}
