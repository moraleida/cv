<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry_method extends Model
{
    public $timestamps = false;
    protected $table = 'entry_method';
}
