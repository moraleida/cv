<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterestGroup extends Model
{
    public $timestamps = false;
    protected $table = 'interest_group';
    //
}
