<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignHasList extends Model
{
    public $timestamps = false;
    protected $table = 'campaign_has_list';
    //
}
