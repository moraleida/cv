<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Neighborhood extends Model
{
    public $timestamps = false;
    protected $table = 'neighborhood';

    /**
     * Eloquent Relations
     */
    public function city()
    {
        return $this->belongsTo('App\City', 'id_city');
    }
}
