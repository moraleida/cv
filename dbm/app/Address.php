<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $timestamps = false;
    protected $table = 'address';

    /**
     * Eloquent Relations
     */
    public function contact()
    {
        return $this->belongsTo('App\Client', 'id_client');
    }
    public function street()
    {
        return $this->belongsTo('App\Street', 'id_street');
    }
}
