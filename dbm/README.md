DBM
==========

PHP MVC Project built over the Laravel 5 framework - work in progress.

This is a full MVC project of a CRM built on top of the MailChimp API. It abstracts the formal attributes of MailChimp to create full profiles of the clients as well as context-oriented lists and segments for better targeting of campaigns. All of the php code for Models, Views and Controllers are mine, as well as a good chunk of javascript and templates. Original HTML and CSS belongs to third-parties.