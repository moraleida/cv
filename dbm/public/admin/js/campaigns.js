$(document).ready(function() {
    // Campanhas
    $('#addList, #addSegment').change(function() {
       var val = $(this).val();
       var tabId = '#tab-1-'+val;
       var tabClass = 'tab'+val;
       var tabRel = $(this).attr('id');

       if($('.'+tabClass).exists()) {
        return false;
       }

       // Adiciona o título
       var tabName = $(this).find('option[value="'+val+'"]').eq(0).text();
       var newTab = '<li class="'+tabClass+'"><a href="'+tabId+'">'+tabName+'<span class="close"></span></a></li>';

       // Adiciona os filtros
       $('.tab-holder').append('<div class="tab-content" id="tab-1-'+val+'">'+
                                '<div class="edit-block"></div></div>');

       $('.tab-holder > div').removeClass('opened');                                    
       $(tabId).addClass('opened');

       if(tabRel == 'addList') {
           ajaxUrl = '/segmentos/cadastro/?parent='+val+ ' #rules';
            loadedLists.push(val);
       } else {
           ajaxUrl = '/segmentos/editar/'+val+ ' #rules';
            loadedSegments.push(val);
       }

        $.ajax({
            method: 'POST',
            data: {lists: loadedLists, segments: loadedSegments},
            url: '/api/campanhas/preview',
            beforeSend: function(request) {
                request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            }
        }).done(function(response) {
            var totalsList = [];
            for(var i = 0; i < response.totals.length; i++) {
                var this_ = response.totals[i];
                totalsList.push('<li>'+this_.type+': '+this_.name+' ('+this_.count+' contatos)</li>');
            }
            $('.preview tbody').html(response.results);
            $('.data-list').html(totalsList.join(''));
            //
            // Tablesorter reset
            var sorting = [[0,0]];
            $('.data > table').trigger('update');
            $('.data > table').trigger('sorton', [sorting]);
        });

        $(tabId).load(ajaxUrl, function() {
            $(tabId).find('select').attr('disabled', 'disabled');
            customForm.customForms.destroyAll();
            customForm.customForms.replaceAll();
        });;


       window.tabSet.addTab(newTab, 0);
       return false;
    });

    $('#addEmail').click(function() {
        if(loadedEmails.length > 0) {
        } else {
            var newTab = '<li class="emails"><a href="#emails">Emails<span class="close"></span></a></li>';
            $('.tab-holder')
            .append('<div class="tab-content" id="emails">'+
                    '<div class="edit-block">'+
                    '<textarea class="form-control emailtextarea" placeholder="digite um email por linha"></textarea>'+
                    '</div></div>');
           $('#emails').addClass('opened');
           window.tabSet.addTab(newTab);
        }
    });

    $('.campaign-opts').on('keyup', 'textarea.emailtextarea', function(e) {
            loadedEmails = $(this).val().split(/\n/);
    });

    $('.btn-continue').click(function(e) {
        e.preventDefault();
        campaignName = $('#campaign').val();

        $.ajax({
            method: 'POST',
            data: {
                lists: loadedLists, 
                segments: loadedSegments, 
                emails: loadedEmails,
                name: campaignName
            },
            url: '/api/campanhas/nova',
            beforeSend: function(request) {
                request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            }
        }).done(function(response) {
            window.open('https://us10.admin.mailchimp.com/campaigns/wizard/confirm?id='+response.web_id);
            window.location.href = window.location.origin+'/campanhas';
        });

    });
});
