window.Dbm = {}
window.tabSet = false;

$(document).ready(function(){

    $.fn.extend({
        exists: function() { return this.length > 0; }
    });

    var delayThis = (function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    // equalheight('.contacts-sidebar, .contacts-edit');
	customForm.lib.domReady(function(){
		customForm.customForms.replaceAll();
	});

    $('.btn-history').click(function() {
        $('.table-block').toggle();
    });

    function loadClients(field, val) {
        $('#datatable').animate({opacity: '0.5'});
        $('#datatable').load(window.location.origin+'/clientes/'+field+'/'+val+' #datatable');
        $('#pagination').load(window.location.origin+'/clientes/'+field+'/'+val+' #pagination');
        $('#datatable').animate({opacity: '1'});
        return false;
    }
    $('.contact-filter > select').not('#searchfield').change(function() {
        var field = $(this).attr('name');
        var val = $(this).val();
        loadClients(field,val);
        return false;
    });

    $('.search-form.client #searchtext').keyup(function(e) {
        if(e.keyCode === 13){
            $(this).next('.btn-submit').click();
        }
    });

    $('.search-form.client .btn-submit').click(function(e) {
        var field = $('#searchfield').val();
        var val = $('#searchtext').val();
        loadClients(field,val);
        return false;
    });

    // Consulta Postman pra buscar endereço por CEP // somente Brasil
    $('.contacts-edit').on('keyup', '.zip', function() {
        var cleanval =  $(this).val().replace(/\D/, '');
        $(this).val(cleanval);
        var val = $(this).val();
        var context = $(this).parents('.duplicate-group');

        if(context.find('.businesscountry').val() != '1')
            return false;

        if(val.length != 8)
            return false;

        $.ajax('http://api.postmon.com.br/v1/cep/'+val).done(function(response) {

            context.find('.rua').val(response.logradouro);
            context.find('.bairro').val(response.bairro);

            $.each(context.find('.businessstate option'), function(i,v) {
                if($(this).text() == response.estado_info.nome)
                    $(this).prop('selected', true);
            });

            $.ajax('/api/state/'+response.estado+'/city/'+response.cidade).done(function(response) {
                var option = '<option value="'+response.id+'" selected="selected">'+response.city+'</option>';
                context.find('.businesscity').html(option);

                customForm.customForms.destroyAll();
                customForm.customForms.replaceAll();
            });

            context.find('.rua, .bairro, .businesscity, .businessstate')
                .attr('readonly', 'readonly')
                .addClass('disabled');

        });

    });
    $('.contacts-edit .ico-star').click(function() {
        var inpt = $('input[name="vip"]');
        if(inpt.val() == 'false' || inpt.val() == '0') {
            inpt.val('true');
        } else {
            inpt.val('false');
        }
    });
/*
$('.pagination a').on('click', function (event) {
    event.preventDefault();
    if ( $(this).attr('href') != '#' ) {
        console.log($(this).attr('href'));
        $("html, body").animate({ scrollTop: 0 }, "fast");
        $('#paginate').load($(this).attr('href')+' #paginate > tr');
    }
});
*/
	//rule for country code
	$.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+[0-9]$/);
	},'Enter Valid Country Code');
	//rule for phone number
	$.validator.addMethod('phoneWithMask', function(value, element) {
		return value.match(/^\([0-9][0-9]\) [0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$/);
	},'Enter Valid Phone');
	//rule for selects
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
		return arg != value;
	}, "This field is required.");
	// Validation plugin
	$('.contacts-edit form').validate({
		highlight: function(element, errorClass, validClass) {
				if($(element).is('input') == false){
        			$(element).parent().find('.select-area').addClass('error');
        		}else{
					$(element).addClass(errorClass).removeClass(validClass);
					// $(element).closest('.input-holder').addClass(errorClass).removeClass(validClass);
        		}
		},
		unhighlight: function(element, errorClass, validClass){
			if($(element).is('input') == false){
    			$(element).parent().find('.select-area').removeClass('error');
    		}else{
				$(element).removeClass(errorClass).addClass(validClass);
				 // $(element).closest('.input-holder').addClass(errorClass).removeClass(validClass);
				if (($(element).closest('.input-holder').find('input.valid').length + $(element).closest('.input-holder').find('select.valid').length) == ($(element).closest('.input-holder').find('input').length + $(element).closest('.input-holder').find('select').length)){
					$(element).closest('.input-holder').removeClass(errorClass).addClass(validClass);
				}
    		}
		},
		// unhighlight: function(element, errorClass, validClass) {
		// 	$(element).removeClass(errorClass).addClass(validClass);
		// 	$(element).closest('.form-holder').removeClass(errorClass).addClass(validClass);
		// },
		// unhighlight: function(element, errorClass, validClass) {
		// 	$(element).removeClass(errorClass).addClass(validClass);
		// 	if (($(element).closest('.input-holder').find('input.valid').length + $(element).closest('.input-holder').find('select.valid').length) == ($(element).closest('.input-holder').find('input').length + $(element).closest('.input-holder').find('select').length)){
		// 		$(element).closest('.input-holder').removeClass(errorClass).addClass(validClass);
		// 	}
		// },
		rules: {
			'f-name':{
				required: true,
				minlength: 2
			},
			'm-name':{
				required: true,
				minlength: 2
			},
			'l-name':{
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			'p-type': {
				valueNotEquals: 'default'
			},
			'p-type1': {
				valueNotEquals: 'default'
			},
			'p-type2': {
				valueNotEquals: 'default'
			},
			'm-type': {
				valueNotEquals: 'default'
			},
			'm-type1': {
				valueNotEquals: 'default'
			},
			'date-month': {
				valueNotEquals: 'default'
			},
			'identification-type': {
				valueNotEquals: 'default'
			},
			'country': {
				valueNotEquals: 'default'
			},
			'region': {
				valueNotEquals: 'default'
			},
			'state': {
				valueNotEquals: 'default'
			},
			'area': {
				valueNotEquals: 'default'
			},
			'job-title': {
				valueNotEquals: 'default'
			},
			'carrer': {
				valueNotEquals: 'default'
			},
			'identification': {
				minlength: 2,
				required: true
			},
			'date-day': {
				required: true,
				digits: true,
				minlength: 2,
				maxlength:2
			},
			'country-code': {
				fnType: true,
				minlength: 2,
				maxlength:2
			},
			'phone-number': {
				phoneWithMask: true
			},
			'zip': {
				required: true,
				digits: true,
				minlength: 5,
				maxlength:5
			},
			'address': {
				required: true,
				minlength: 3
			},
			'address-2': {
				required: true,
				minlength: 3
			},
			'company': {
				required: true,
				minlength: 3
			},
			'country-code2': {
				fnType: true,
				minlength: 2,
				maxlength:2
			},
			'phone-number2': {
				phoneWithMask: true
			},
			'extension-number': {
				required: true,
				digits: true
			},
			password : {
				minlength : 5,
				required: true
			},
			password_confirm : {
				required: true,
				equalTo : "#password"
			},
			'.phone-code':{
				required:true,
			},
			'.phone01':{
				required:true,
			},
		},
		groups: {
	        '.phone01': "phone01 phone-code"
		},
        errorPlacement: function(error, element){
        	// console.log(element);
        	// $(element).each(function(index, elem){
        	// 	console.log($(elem).is('input'));
        	// 	if($(elem).is('input') == false){
        	// 		$(elem).parent().find('.select-area').addClass('error');
        	// 	}
        	// })
            return false;  // suppresses error message text
        }
		// errorPlacement: function(error, element) {
		// 	console.log(error);
	 //       if (element.hasClass('phone01 ') && element.val() == '' || element.hasClass('phone-code') && element.val() == '') 
	 //        // error.insertAfter(".phone01");
	 //        error.insertAfter(".phone-number");
	 //       else 
	 //        error.insertAfter(element);
	 //   }
		// messages: {
		// 	'f-name': 'Please specify your name',
		// 	'm-name': 'Please specify your name',
		// 	'l-name': 'Please specify your name',
		// 	email: {
		// 		required: 'We need your email address to contact you',
		// 		email: 'Enter valid email'
		// 	},
		// 	'phone-number': 'Invalid phone number',
		// 	'address': 'Your address is too short',
		// 	'address-2': 'Your address is too short',
		// 	password:{
		// 		required: 'Enter your password',
		// 		minlength: 'Password is too short'
		// 	},
		// 	password_confirm:{
		// 		required: 'Repeat your password',
		// 		equalTo: 'Password doesn`t match'
		// 	}
		// }
	});
	//all default messages
	// jQuery.extend(jQuery.validator.messages, {
	// 	required: "This field is required.",
	// 	remote: "Please fix this field.",
	// 	email: "Please enter a valid email address.",
	// 	url: "Please enter a valid URL.",
	// 	date: "Please enter a valid date.",
	// 	dateISO: "Please enter a valid date (ISO).",
	// 	number: "Please enter a valid number.",
	// 	digits: "Please enter only digits.",
	// 	creditcard: "Please enter a valid credit card number.",
	// 	equalTo: "Please enter the same value again.",
	// 	accept: "Please enter a value with a valid extension.",
	// 	maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
	// 	minlength: jQuery.validator.format("Please enter at least {0} characters."),
	// 	rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
	// 	range: jQuery.validator.format("Please enter a value between {0} and {1}."),
	// 	max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
	// 	min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	// });

	$('.phone01').mask("(99) 99999-9999");
	$('.phone01').each(function(){
		$(this).on("blur", function() {
			var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
			if( last.length == 3 ) {
				var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
				var lastfour = move + last;
				var first = $(this).val().substr( 0, 9 );
				$(this).val( first + '-' + lastfour );
			}
		});
	});

	jQuery.validator.addMethod('selectcheck', function (value) {
		return (value != '0');
	});
	$('.login-form form').validate();
/*	
	$('.feedback-form form').validate({
		submitHandler: function(form) {
			$(form).fadeOut(300).closest('.feedback-form').find('.success-box').fadeIn(300);
		}
	});
*/	
	$('.wrapper').has('.side-menu').addClass('offset-left');
	
	$('#header').find('.welcome-box > a').on('click', function() {
		$(this).next('ul').slideToggle(300);
		return false;
	});
	
	customForm.lib.domReady(function(){
		customForm.customForms.destroyAll();
		customForm.customForms.replaceAll();
	});

	var template_filters_  = jQuery.validator.format($.trim($("#template_filter").html()));
    var template_filters_email_  = jQuery.validator.format($.trim($("#template_filter_email").html()));
    var template_filters_phone_  = jQuery.validator.format($.trim($("#template_filter_phone").html()));
    var template_filters_corporate_phone_  = jQuery.validator.format($.trim($("#template_filter_corporate_phone").html()));
    var template_filters_address_  = jQuery.validator.format($.trim($("#template_filter_address").html()));
    var template_filters_company_  = jQuery.validator.format($.trim($("#template_filter_company").html()));
    var template_filters_function_  = jQuery.validator.format($.trim($("#template_filter_function").html()));
	
    $('.btn-add-group').click(function() {
        var this_ = $(this),
            rules_block = this_.closest('.edit-block').find('.rules');
            i = rules_block.find('.group').length;
            z = i++;
            
            $(template_filters_(z)).appendTo(rules_block.eq(0));
            customForm.customForms.destroyAll();
            customForm.customForms.replaceAll();
            return false;
    });

	$('.contacts-edit .duplicate-group .btn-addrow').click(function() {

        var this_ = $(this),
            rules_block = this_.closest('.outer-group');
            rules_block_name = rules_block.attr('rel');
            groupsize = $('#groupsize');
        
            // Garantir Indexes unicos pros arrays
            z = groupsize.val();
            if(z === 'true') {
                z = $('.duplicate-group').length;
                groupsize.val(z);
            } else {
                z = parseInt(z)+10;
                groupsize.val(z);
            }

            if(rules_block_name == 'email') {
                $(template_filters_email_(z)).appendTo(rules_block.eq(0));
            } else if(rules_block_name == 'phone') {
                $(template_filters_phone_(z)).appendTo(rules_block.eq(0));
            } else if(rules_block_name == 'corporatephone') {
                $(template_filters_corporate_phone_(z)).appendTo(rules_block.eq(0));
            } else if(rules_block_name == 'address') {
                $(template_filters_address_(z)).appendTo(rules_block.eq(0));
            } else if(rules_block_name == 'company') {
                $(template_filters_company_(z)).appendTo(rules_block.eq(0));
            }

		customForm.customForms.destroyAll();
		customForm.customForms.replaceAll();
		setTimeout(function(){
			equalheight('.contacts-sidebar, .contacts-edit');
		}, 200);
		return false;
	});

    $('.contacts-edit').on('click', '.btn-removerow', function(e) {
        e.preventDefault();
        $(this).parent().detach();
    });

    $('.edit-block').on('click', '.btn-remove-group', function(e) {
        e.preventDefault();
        $(this).parent().detach();
    });

    $('.edit-block').on('change', '.group > select', function() {
        var el = $(this);
        var val = el.val();
        var name = el.attr('name').split(/\[\d]/)[1];

        if( name == '[col2]' )
            return false;

        if( name == '[col1]' ) {
            $.ajax('/api/lista/filtro/'+val).done(function(response) {
                el.parents('.group').find('select').eq(2).html(response);
                customForm.customForms.destroyAll();
                customForm.customForms.replaceAll();
            });
        }

        if( name == '[col3]' ) {
            Dbm.getListorSegmentFilteredResults();
        }

        customForm.customForms.destroyAll();
        customForm.customForms.replaceAll();
        return false;
    });


    $('.search-form').not('.client').on('keyup', 'input', function() {
        var busca = $(this).val();
        var rows = $('.data tbody > tr');
        $('.data').addClass('loading');
        delayThis(function() {
            rows.hide();
            for(var i=0; i<rows.length; i++) {
                re = new RegExp(busca, 'i');
                if(rows.eq(i).text().split(re).length > 1) {
                    rows.eq(i).show();
                }
            }
            $('.data').removeClass('loading');
        }, 1000);
    });

    /*
	(function() {
		var $form = $('.edit-block'),
			$btn = $form.find('.btn-add-group');
		
		$btn.on('click', function() {
			var $parent = $(this).closest('.edit-block').find('.rules'),
				$el = $parent.find('.group:last-child').clone();
			$el.find('> div').remove();
			$parent.append($el);
			customForm.customForms.replaceAll();
			return false;
		});
	})();
	*/

	(function() {
		var $parent = $('.data'),
			$tbl = $parent.find('> table'),
			optWidgets = [],
			$pager = $('.pagination'),
			num = $pager.find('.pagesize').val();
			
		if ($parent.hasClass('zebra')) {
			optWidgets.push('zebra');
		}
		$tbl.tablesorter({
			widthFixed: true,
			cssChildRow: "tablesorter-childRow",
			widgets: optWidgets
		});
		$tbl.find('th').has('.content-menu').addClass('menu-parent');
		$tbl.find('th').has('input').addClass('menu-parent');
		$tbl.find('.tablesorter-childRow td').hide();
		$tbl.delegate('.toggle-children', 'click', function(){
			$(this).closest('tr').nextUntil('tr:not(.tablesorter-childRow)').find('td').toggle();
			return false;
		});
		$tbl.tablesorterPager({
			container: $("#pager"),
			size: num
		});
		
		$pager.find('> .prev').on('click', function() {
			$(this).closest('.pagination').find('.pager').find('.first').click();
			$(this).next('ul').find('li:first-child').addClass('active').siblings().removeClass('active');
			return false;
		});
		$pager.find('> .next').on('click', function() {
			$(this).closest('.pagination').find('.pager').find('.last').click();
			$(this).prev('ul').find('li:last-child').addClass('active').siblings().removeClass('active');
			return false;
		});
		$pager.find('ul').find('a').on('click', function() {
			var $par = $(this).closest('li'),
				ix = $(this).closest('ul').children().index($par) + 1;
			$pager.find('.gotoPage').val(ix).change();
			$par.addClass('active').siblings().removeClass('active');
			return false;
		});
		
		$tbl.find('.check-all').on('change', function() {
			if($(this).is(':checked')){
				$(this).closest('table').find('tbody').find('input[type="checkbox"]').click().prop('checked',true);
			} else {
				$(this).closest('table').find('tbody').find('input[type="checkbox"]').click().prop('checked',false);
			} 
		});
	})();
	
	$('.content-menu').find('.btn-toggle').on('click', function() {
		var $parent = $(this).closest('.content-menu');
		$parent.find('ul').fadeToggle(300);
		$('.content-menu').not($parent).find('ul:visible').fadeOut(300);
		return false;
	});
	
	$('.open-modal').fancybox({
		wrapCSS : 'fancybox-custom',
		padding: 20,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(158,157,158,0.7)'
				}
			}
		}
	});

    $('.open-modal').click(function() {
        var el = $(this);
        var title = el.parents('tr').find('td').eq(0).text();
        var id = el.attr('href');
        var ref = el.attr('rel');
        $(id).find('h3 > span').text(title);
        $(id).find('input[name="name"]').val(title+' (cópia)');
        $(id).find('form').attr('action', ref);
    });
	
	$('.circle-graph').each(function(){
		var this_ = $(this),
			start_angle = -Math.PI / 2,
			number_ = this_.find('.circle-value').text();

		this_.circleProgress({
			value: '.' + this_.attr('data-value'),
			fill: { color: '#dfe0e2'},
			size: this_.width(),
			startAngle: start_angle,
			emptyFill: '#5c9891',
			animation: {
				duration: 800
			}
		}).on('circle-animation-progress', function(event, progress, stepValue) {
			this_.find('.circle-value').text(parseInt(number_ * progress));
		});
	});
	
    
    var openClass = 'opened';
    var activeClass = 'ui-state-active';
	window.tabSet = $('.tabset').scrollTabs({
        'click_callback': function(e) {
            e.preventDefault();
            $(this).addClass(activeClass).siblings('li').removeClass(activeClass);
            $($(this).find('a').attr('href')).addClass(openClass).siblings('.tab-content').removeClass(openClass);
            $('.campaign-opts').removeClass('hidden-block');

            adjustTabs();

            return false;
        }
    });

    function adjustTabs() {
        $('.tabset').each(function() {
            var $this = $(this),
                $active = $this.find('.' + activeClass),
                href;
            
            if ($active.length > 0) {
                href = $active.find('a').attr('href');
            } else {
                href = $this.find('li').eq(0).addClass(activeClass).find('a').attr('href');
            }
            $(href).addClass(openClass);
            
            $this.find('.close').on('click', function() {
                var $par = $(this).closest('li'),
                    $next;
                var listID = $par.find('a').attr('href').split('#tab-1-')[1];
                var index = $.inArray(listID, loadedLists);
                if( index !== -1) {
                    loadedLists.splice(index, 1);
                }
                $($(this).closest('a').attr('href')).remove();
                if ($par.hasClass(activeClass)) {
                    $next = $par.next('li');
                    $next = ($next.length <= 0) ? $par.prev('li') : $next;
                    if ($next.length > 0) {
                        $next.find('a').click();
                    }
                }
                $par.remove();
                return false;
            });
        });
    }
//

	$('.choose-new').find('.go-campaign').on('click', function() {
		$(this).closest('.choose-new').addClass('hidden-block').next('.campaign-opts').removeClass('hidden-block');
		return false;
	});
	
	$('input:radio[name=contacts-type][value=val-001]').click();
	$('.contacts-accordion header').click(function() {
		$(this).closest('.contacts-accordion').find('.collapse-block').not($(this).closest('li').find('.collapse-block')).slideUp();
		$(this).closest('li').find('.collapse-block').slideToggle();
		return false;
	});
	$('.ico-star').click(function() {
		$(this).toggleClass('gray-state');
		return false;
	});
/*
	$('.contacts-edit .duplicate-group .btn-addrow').click(function() {
		var $new_row = $(this).closest('.duplicate-group').find('.cols:last-child').clone();
		$new_row.appendTo($(this).closest('.duplicate-group'));
		$new_row.find('.select-area').remove();
		customForm.customForms.destroyAll();
		customForm.customForms.replaceAll();
		setTimeout(function(){
			equalheight('.contacts-sidebar, .contacts-edit');
		}, 200);
		return false;
	});
    */
	$('.ibp-list header input:checkbox').change(function(){
		if($(this).is(':checked')){
			$(this).closest('li').addClass('checked-block');
		} else {
			$(this).closest('li').removeClass('checked-block');
		}
	});

    $('.contacts-edit').on('change', '.businesscountry', function() {
        var val = $(this).val();
        var states = $(this).parents('.duplicate-group').find('.businessstate');
        if(val == '1') {
            $('.home_address').show();
            $('.intl_address').hide();
        } else {
            $('.home_address').hide();
            $('.intl_address').show();
        }

        $(this).parents('.duplicate-group')
            .find('.businesscity, .businessstate, .rua, .bairro, .zip, .numero, .complemento')
            .removeAttr('readonly')
            .removeAttr('disabled')
            .removeClass('disabled');


        $.ajax('/api/'+val+'/states').done(function(response) {
            var options = [];
            states.html('<option value="0">Selecionar</option>');
            for(var i=0; i<response.length; i++) {
                options.push('<option value="'+response[i].id+'">'+response[i].state+'</option>');
            }
            states.append(options.toString());


            customForm.customForms.destroyAll();
            customForm.customForms.replaceAll();
        });

    });

    $('.contacts-edit').on('change', '.businessstate', function() {
        var val = $(this).val();
        var country = $(this).parents('.duplicate-group').find('.businesscountry').val();
        var city = $(this).parents('.duplicate-group').find('.businesscity');

        if(country == '1') {
            endpoint = 'cities';
        } else {
            endpoint = 'citiesintl';
            val = country;
        }

        $.ajax('/api/'+val+'/'+endpoint).done(function(response) {
            var options = [];
            for(var i=0; i<response.length; i++) {
                options.push('<option value="'+response[i].id+'">'+response[i].city+'</option>');
            }
            city.html(options.toString());

            customForm.customForms.destroyAll();
            customForm.customForms.replaceAll();
        });

    });

    Dbm.duplicateItem = function () {
        var el = $(this);

        $.ajax({
            method: 'POST',
            url: el.attr('href'),
            beforeSend: function(request) {
                request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            }
        }).done(function(response) {
            //
        });
    }

    // Helper functions
    Dbm.getListorSegmentFilteredResults = function () {
        var filters = [];
        var rule = $('select[name="filter[rules]"]').val();
        var skip = false;

        $('.data').addClass('loading');

        if( $('form').hasClass('segment') )
            skip = $('.group.disabled').length;

        $.each($('.group > select'), function(i,v) {
           var selectName = $(this).attr('name'); 
           filters.push({'name': selectName, 'value': $(this).val()});
        });

        $.ajax({
            method: 'POST',
            url: '/api/lista/filtrada',
            data: {rule: rule, filters: filters, skip: skip},
            beforeSend: function(request) {
                request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            }
        }).done(function(response) {
            var html = [];
            $('.data tbody').html(response.results.join(''));
            $('.data-list').html('<li>Lista: '+$('input[name="name"]').val()+' ('+response.totals+') contatos</li>');

            // Tablesorter reset
            var sorting = [[0,0]];
            $('.data > table').trigger('update');
            $('.data > table').trigger('sorton', [sorting]);
            
            $('.data').removeClass('loading');
        });
    }
});

$(window).load(function(){
	// equalheight('.contacts-sidebar, .contacts-edit');

    // Autoload resultados nas edições de listas/segmentos
    if(typeof(autoloadList) != 'undefined')
        Dbm.getListorSegmentFilteredResults();
});
$(window).resize(function(){
	equalheight('.contacts-sidebar, .contacts-edit');
});
