$(document).ready(function(){

    $('.feedback-form form').on('submit', function(e) {
        console.log('click');
        e.preventDefault();

        var form = $(this);
        var method = form.find('input[name="_method"]').val() || 'POST';
        var url = form.prop('action');

        $.ajax({
            type: method,
            url: url,
            data: form.serialize(),
            success: function(response) {
                console.log(response);
            }
        });
    });
});
