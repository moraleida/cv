$(document).ready(function() {

    $.fn.extend({
        exists: function() { return this.length > 0; }
    });

    var defaultImage = '<img src="/images/contato-profile.png" alt="image">';

    // TODO: Debugar click em photo
    if($('.profile-photo').exists()) {
        $('#photo').click(function(e){ e.preventDefault(); });

        $('.profile-photo').on('click', '.btn-remove', function() {
            $('#photo').find('canvas').detach();
            var path = $('input[name="uploadedpath"]').val();
            $('input[name="uploadedpath"]').val('');
            $('#photo').append(defaultImage);
            $(this).removeClass('btn-remove').addClass('btn-add');


            $.ajax({
                method: 'POST',
                url: '/api/image/remove',
                data: {img: path},
                beforeSend: function(request) {
                    request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                }
            });

        });

        // Pluploader - Imagem no cadastro
        var uploader = new plupload.Uploader({
            browse_button: 'photo',
            url: '/api/image/upload',
            filters: {
                mime_types: [{title: 'Imagens', extensions: 'jpg' }]
            },
            resize: {
                width: '100px',
                height: '100px',
                crop: true,
                preserve_headers: false
            },
            headers: { 
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var img_holder = $('.photo-upload .img');
        var img_placeholder = img_holder.find('img').eq(0);
        var img_input = $('input[name="uploadedpath"]');

        uploader.init();

        uploader.bind('FilesAdded', function(up, files, response) {

            img_holder.html('');
            $('.photo-upload .btn-add').removeClass('btn-add').addClass('btn-remove');

            if(up.files.length > 1) {
                uploader.removeFile(up.files[0]);

                if(up.files.length < 1)
                   img_holder.append(img_placeholder); 
            }

            $.each(files, function(){
                
                var img = new mOxie.Image();

                img.onload = function() {
                    this.embed(img_holder.get(0), {
                        width: 70,
                        height: 70,
                        crop: true
                    });
                };

                img.onembedded = function() {
                    this.destroy();
                };

                img.onerror = function() {
                    this.destroy();
                };

                img.load(this.getSource());        
                
            });

            uploader.start();
        });

        uploader.bind('FileUploaded', function(up, file, response) {
            var resp = $.parseJSON(response.response);
            $('input[name="uploadedpath"]').val(resp.filename);
        });
    }
});
