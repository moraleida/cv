$(document).ready(function(){
	customForm.lib.domReady(function(){
		customForm.customForms.replaceAll();
	});
    // Consulta Postman pra buscar endereço por CEP // somente Brasil
    $('#registration-form').on('keyup', '.zip', function() {
        var cleanval =  $(this).val().replace(/\D/, '');
        $(this).val(cleanval);
        var val = $(this).val();
        var context = $(this).parents('.container');

        if(context.find('#businesscountry').val() != '1')
            return false;

        if(val.length != 8)
            return false;

        $.ajax('http://api.postmon.com.br/v1/cep/'+val).done(function(response) {

            context.find('.rua').val(response.logradouro);
            context.find('.bairro').val(response.bairro);
            context.find('.businessstate').val(response.estado_info.nome);

            context.find('.rua, .bairro, .businesscity, .businessstate')
                .attr('readonly', 'readonly')
                .addClass('disabled');

            $.each($('.businessstate option'), function(i,v) {
                if($(this).text() == response.estado_info.nome)
                    $(this).prop('selected', true);
            });

            $.ajax('/api/state/'+response.estado+'/city/'+response.cidade).done(function(response) {
                var option = '<option value="'+response.id+'" selected="selected">'+response.city+'</option>';
                context.find('.businesscity').html(option);

                customForm.customForms.destroyAll();
                customForm.customForms.replaceAll();
            });


        });

    });
	$('label a').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});
	//rule for country code
	$.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+[0-9]$/);
	},'Enter Valid Country Code');
	//rule for phone number
	$.validator.addMethod('phoneWithMask', function(value, element) {
		return value.match(/^\([0-9][0-9]\) [0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$/);
	},'Enter Valid Phone');
	//rule for selects
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
		return arg != value;
	}, "This field is required.");
    /*
    $.validator.addMethod("uniqueEmail", function(value, element) {
        var emails = $('input[type="email"]');
        var result = true;
        $.each(emails, function(i,v) {
            return $(this).val() != value;
        });
        return result;
    }, "This value was already inserted");
    */
	// Validation plugin
	$('#registration-form form').validate({
		highlight: function(element, errorClass, validClass) {
			$(element).addClass(errorClass).removeClass(validClass);
			$(element).closest('.form-holder').addClass(errorClass).removeClass(validClass);
		},
		// unhighlight: function(element, errorClass, validClass) {
		// 	$(element).removeClass(errorClass).addClass(validClass);
		// 	$(element).closest('.form-holder').removeClass(errorClass).addClass(validClass);
		// },
		unhighlight: function(element, errorClass, validClass) {
			$(element).removeClass(errorClass).addClass(validClass);
			if (($(element).closest('.form-holder').find('input.valid').length + $(element).closest('.form-holder').find('select.valid').length) == ($(element).closest('.form-holder').find('input').length + $(element).closest('.form-holder').find('select').length)){
				$(element).closest('.form-holder').removeClass(errorClass).addClass(validClass);
			}
		},
		rules: {
			'f-name':{
				required: true,
				minlength: 2
			},
			'm-name':{
				required: true,
				minlength: 2
			},
			'l-name':{
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			'p-type': {
				valueNotEquals: 'default'
			},
			'p-type1': {
				valueNotEquals: 'default'
			},
			'p-type2': {
				valueNotEquals: 'default'
			},
			'm-type': {
				valueNotEquals: 'default'
			},
			'm-type1': {
				valueNotEquals: 'default'
			},
			'date-month': {
				valueNotEquals: 'default'
			},
			'identification-type': {
				valueNotEquals: 'default'
			},
			'country': {
				valueNotEquals: 'default'
			},
			'region': {
				valueNotEquals: 'default'
			},
			'state': {
				valueNotEquals: 'default'
			},
			'area': {
				valueNotEquals: 'default'
			},
			'job-title': {
				valueNotEquals: 'default'
			},
			'carrer': {
				valueNotEquals: 'default'
			},
			'identification': {
				minlength: 2,
				required: true
			},
			'birthday': {
				required: true,
				digits: true,
				minlength: 2,
				maxlength:2,
                range: [01,31]
			},
			'country-code': {
				fnType: true,
				minlength: 2,
				maxlength:2
			},
			'phone-number': {
				phoneWithMask: true
			},
			'zip': {
				required: true,
				digits: true,
				minlength: 5,
				maxlength:5
			},
			'address': {
				required: true,
				minlength: 3
			},
			'address-2': {
				required: true,
				minlength: 3
			},
			'company': {
				required: true,
				minlength: 3
			},
			'country-code2': {
				fnType: true,
				minlength: 2,
				maxlength:2
			},
			'phone-number2': {
				phoneWithMask: true
			},
            /*
			'extension-number': {
				required: false,
				digits: true
			},
            */
			password : {
				minlength : 5,
				required: true
			},
			password_confirmation : {
				required: true,
				equalTo : "#password"
			},
			password_02 : {
				minlength : 5,
				required: true
			},
			password_confirmation_02 : {
				required: true,
				equalTo : "#password-02"
			}
		},
		// messages: {
		// 	'f-name': 'Please specify your name',
		// 	'm-name': 'Please specify your name',
		// 	'l-name': 'Please specify your name',
		// 	email: {
		// 		required: 'We need your email address to contact you',
		// 		email: 'Enter valid email'
		// 	},
		// 	'phone-number': 'Invalid phone number',
		// 	'address': 'Your address is too short',
		// 	'address-2': 'Your address is too short',
		// 	password:{
		// 		required: 'Enter your password',
		// 		minlength: 'Password is too short'
		// 	},
		// 	password_confirm:{
		// 		required: 'Repeat your password',
		// 		equalTo: 'Password doesn`t match'
		// 	}
		// }
/*
		messages: {
			'f-name': 'Please specify your name',
			'm-name': 'Please specify your name',
			'l-name': 'Please specify your name',
			email: {
				required: 'We need your email address to contact you',
				email: 'Enter valid email'
			},
			'phone-number': 'Invalid phone number',
			'address': 'Your address is too short',
			'address-2': 'Your address is too short',
			password:{
				required: 'Enter your password',
				minlength: 'Password is too short'
			},
			password_confirm:{
				required: 'Repeat your password',
				equalTo: 'Password doesn`t match'
			},
			password_02:{
				required: 'Enter your password',
				minlength: 'Password is too short'
			},
			password_confirm_02:{
				required: 'Repeat your password',
				equalTo: 'Password doesn`t match'
			}
		}
*/
	});
	console.log($('.check-select').val());
	//Take the templates from HTML
	var template_email_  = jQuery.validator.format($.trim($("#template01").html()));
	var template_phone_  = jQuery.validator.format($.trim($("#template02").html()));
	var corporate_phone_ = jQuery.validator.format($.trim($("#template03").html()));
	var corporate_ext_   = jQuery.validator.format($.trim($("#template04").html()));
	var template_email_2  = jQuery.validator.format($.trim($("#template_filter_email").html()));
	var template_phone_2  = jQuery.validator.format($.trim($("#template002").html()));
	var corporate_phone_2 = jQuery.validator.format($.trim($("#template003").html()));
	var corporate_ext_2   = jQuery.validator.format($.trim($("#template004").html()));
	// add more rows on click
// 	$(".btn-add").click(function(){
// 		var this_ = $(this),
// 			i = this_.closest('.add-rows').find('.form-group').length;
// //			i = this_.closest('.add-rows').find('.form-group').length + 1;
//             z=i++;
		
// 		if (this_.closest('.add-rows').hasClass('email')){
// 			$(template_email_(z)).appendTo(this_.closest('.add-rows'));
// 		}
// 		else if (this_.closest('.add-rows').hasClass('phone')){
// 			$(template_phone_(z)).appendTo(this_.closest('.add-rows'));
// 		}
// 		else if (this_.closest('.add-rows').hasClass('company-phone')){
// 			$(corporate_phone_(z)).appendTo(this_.closest('.add-rows'));
// 			$(corporate_ext_(z)).appendTo(this_.parents('.row').find('.col-right'));
// 		}
// 		customForm.customForms.refreshAll();
// 		return false;
// 	});

	$(".btn-add").click(function(){
		var this_ = $(this),
			i = this_.closest('.add-rows').find('.form-group').length;
		
		if (this_.closest('.add-rows').hasClass('email')){
			$(template_email_(i++)).appendTo(this_.closest('.add-rows'));
			$('.btn-remove').click(removeAppend);
		}
		else if (this_.closest('.add-rows').hasClass('phone')){
			$(template_phone_(i++)).appendTo(this_.closest('.add-rows'));
			$('.btn-remove').click(removeAppend);
		}
		else if (this_.closest('.add-rows').hasClass('company-phone')){
            z=i++;
			$(corporate_phone_(z)).appendTo(this_.closest('.add-rows'));
			$(corporate_ext_(z)).appendTo(this_.parents('.row').find('.col-right')).addClass('ext-'+z);
			$('.btn-remove').click(removeAppend).addClass('has-extension-'+z);
		}
		else if (this_.closest('.add-rows').hasClass('email-02')){
			$(template_email_2(i++)).appendTo(this_.closest('.add-rows'));
		}
		else if (this_.closest('.add-rows').hasClass('phone-02')){
			$(template_phone_2(i++)).appendTo(this_.closest('.add-rows'));
		}
		else if (this_.closest('.add-rows').hasClass('company-phone-02')){
            z=i++;
			$(corporate_phone_2(z)).appendTo(this_.closest('.add-rows'));
			$(corporate_ext_2(z)).appendTo(this_.parents('.row').find('.col-right'));
		}
		customForm.customForms.destroyAll();
		customForm.customForms.replaceAll();
		return false;
	});

	$('.btn-remove').click(removeAppend);

	/*phone01 masc*/
	$('.phone01').mask("(99) 99999-9999");
	$('.phone01').each(function(){
		$(this).on("blur", function() {
			var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
			if( last.length == 3 ) {
				var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
				var lastfour = move + last;
				var first = $(this).val().substr( 0, 9 );
				$(this).val( first + '-' + lastfour );
			}
		});
	});

    $('.btn-form').click(function(e) {
        e.preventDefault();
        jQuery('#registration-form .container+.container').addClass('visible');
    });

    $('#businesscountry').on('change', function() {
        var val = $(this).val();
        if( val == '1') {
            $('.home_address').fadeIn('slow');
            $('.intl_address').hide();
        } else {
            $('.home_address').hide();
            $('.intl_address').fadeIn('slow');
        }

        $(this).parents('.container')
            .find('.businesscity, .businessstate, .rua, .bairro, .zip, .numero, .complemento')
            .removeAttr('readonly')
            .removeAttr('disabled')
            .removeClass('disabled');

        $.ajax('/api/'+val+'/states').done(function(response) {
            var options = [];
            for(var i=0; i<response.length; i++) {
                options.push('<option rel="'+response[i].safe+'" value="'+response[i].id+'">'+response[i].state+'</option>');
            }
            $('#businessstate').html(options.toString());

            customForm.customForms.destroyAll();
            customForm.customForms.replaceAll();
        });

    });

    $('#businessstate').on('change', function() {
        var val = $(this).val();
        var country = $('#businesscountry').val();

        if(country == '1') {
            endpoint = 'cities';
        } else {
            endpoint = 'citiesintl';
            val = country;
        }

        $.ajax('/api/'+val+'/'+endpoint).done(function(response) {
            var options = [];
            for(var i=0; i<response.length; i++) {
                options.push('<option value="'+response[i].id+'">'+response[i].city+'</option>');
            }
            $('#businesscity').html(options.toString());
        });

        customForm.customForms.refreshAll();
    });

});
/*end document.ready*/
function removeAppend(){
	var extension_element 	= '',
		$this 				= $(this),
		extension_elements 	= $this.attr('class').replace("btn-remove has-extension-", "");

	$this.parent().parent().fadeOut('slow', function(){
		$(this).remove();
	});

	if(extension_elements != ''){
		$('.ext-'+extension_elements).remove();
	}
}

function getDomainType(email) {
    var emailType = 'pessoal';
    var userDomain = email.split('@')[1];

    $.ajax({
        method: 'GET',
        url: '/api/email/domains',
        async: false,
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        }
    }).done(function(response) {
        console.log(response);
        var personalDomains = response;
        var domainIsPersonal = $.inArray(userDomain, personalDomains);
        if(domainIsPersonal === -1)
            emailType = 'comercial';

    });

    return emailType;

}
