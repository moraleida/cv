$(document).ready(function(){
	jQuery.validator.addMethod('selectcheck', function (value) {
		return (value != '0');
	});
	$('.login-form form').validate();
	
	$('.feedback-form form').validate({
		submitHandler: function(form) {
			$(form).fadeOut(300).closest('.feedback-form').find('.success-box').fadeIn(300);
		}
	});
	
	$('.wrapper').has('.side-menu').addClass('offset-left');
	
	$('#header').find('.welcome-box > a').on('click', function() {
		$(this).next('ul').slideToggle(300);
		return false;
	});
	
	customForm.lib.domReady(function(){
		customForm.customForms.replaceAll();
	});
	
	(function() {
		var $form = $('.edit-block'),
			$btn = $form.find('.btn-add-group');
		
		$btn.on('click', function() {
			var $parent = $(this).closest('.edit-block').find('.rules'),
				$el = $parent.find('.group:last-child').clone();
			$el.find('> div').remove();
			$parent.append($el);
			customForm.customForms.replaceAll();
			return false;
		});
	})();
	
	(function() {
		var $parent = $('.data'),
			$tbl = $parent.find('> table'),
			optWidgets = [],
			$pager = $('.pagination'),
			num = $pager.find('.pagesize').val();
			
		if ($parent.hasClass('zebra')) {
			optWidgets.push('zebra');
		}
		$tbl.tablesorter({
			widthFixed: true,
			cssChildRow: "tablesorter-childRow",
			widgets: optWidgets
		});
		$tbl.find('th').has('.content-menu').addClass('menu-parent');
		$tbl.find('th').has('input').addClass('menu-parent');
		$tbl.find('.tablesorter-childRow td').hide();
		$tbl.delegate('.toggle-children', 'click', function(){
			$(this).closest('tr').nextUntil('tr:not(.tablesorter-childRow)').find('td').toggle();
			return false;
		});
		$tbl.tablesorterPager({
			container: $("#pager"),
			size: num
		});
		
		$pager.find('> .prev').on('click', function() {
			$(this).closest('.pagination').find('.pager').find('.first').click();
			$(this).next('ul').find('li:first-child').addClass('active').siblings().removeClass('active');
			return false;
		});
		$pager.find('> .next').on('click', function() {
			$(this).closest('.pagination').find('.pager').find('.last').click();
			$(this).prev('ul').find('li:last-child').addClass('active').siblings().removeClass('active');
			return false;
		});
		$pager.find('ul').find('a').on('click', function() {
			var $par = $(this).closest('li'),
				ix = $(this).closest('ul').children().index($par) + 1;
			$pager.find('.gotoPage').val(ix).change();
			$par.addClass('active').siblings().removeClass('active');
			return false;
		});
		
		$tbl.find('.check-all').on('change', function() {
			if($(this).is(':checked')){
				$(this).closest('table').find('tbody').find('input[type="checkbox"]').click().prop('checked',true);
			} else {
				$(this).closest('table').find('tbody').find('input[type="checkbox"]').click().prop('checked',false);
			} 
		});
	})();
	
	$('.content-menu').find('.btn-toggle').on('click', function() {
		var $parent = $(this).closest('.content-menu');
		$parent.find('ul').fadeToggle(300);
		$('.content-menu').not($parent).find('ul:visible').fadeOut(300);
		return false;
	});
	
	$('.open-modal').fancybox({
		wrapCSS : 'fancybox-custom',
		padding: 20,
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(158,157,158,0.7)'
				}
			}
		}
	});
	
	$('.circle-graph').each(function(){
		var this_ = $(this),
			start_angle = -Math.PI / 2,
			number_ = this_.find('.circle-value').text();

		this_.circleProgress({
			value: '.' + this_.attr('data-value'),
			fill: { color: '#dfe0e2'},
			size: this_.width(),
			startAngle: start_angle,
			emptyFill: '#5c9891',
			animation: {
				duration: 800
			}
		}).on('circle-animation-progress', function(event, progress, stepValue) {
			this_.find('.circle-value').text(parseInt(number_ * progress));
		});
	});
	
	$('.tabset').scrollTabs();
	$('.tabset').each(function() {
		var $this = $(this),
			openClass = 'opened',
			activeClass = 'ui-state-active',
			$active = $this.find('.' + activeClass),
			href;
		
		if ($active.length > 0) {
			href = $active.find('a').attr('href');
		} else {
			href = $this.find('li').eq(0).addClass(activeClass).find('a').attr('href');
		}
		$(href).addClass(openClass);
		
		$this.find('li').find('a').on('click', function() {
			$(this).closest('li').addClass(activeClass).siblings('li').removeClass(activeClass);
			$($(this).attr('href')).addClass(openClass).siblings('.tab-content').removeClass(openClass);
			return false;
		});
		$this.find('.close').on('click', function() {
			var $par = $(this).closest('li'),
				$next;
			$($(this).closest('a').attr('href')).remove();
			if ($par.hasClass(activeClass)) {
				$next = $par.next('li');
				$next = ($next.length <= 0) ? $par.prev('li') : $next;
				if ($next.length <= 0) {
					$this.closest('.campaign-opts').addClass('hidden-block').prev('.choose-new').removeClass('hidden-block');
				}
				else {
					$next.find('a').click();
				}
			}
			$par.remove();
			return false;
		});
	});
	
	$('.choose-new').find('.go-campaign').on('click', function() {
		$(this).closest('.choose-new').addClass('hidden-block').next('.campaign-opts').removeClass('hidden-block');
		return false;
	});
	
	$('input:radio[name=contacts-type][value=val-001]').click();
	$('.contacts-accordion header').click(function() {
		$(this).closest('.contacts-accordion').find('.collapse-block').not($(this).closest('li').find('.collapse-block')).slideUp();
		$(this).closest('li').find('.collapse-block').slideToggle();
		return false;
	});
	$('.ico-star').click(function() {
		$(this).toggleClass('gray-state');
		return false;
	});
	$('.contacts-edit .duplicate-group .btn-addrow').click(function() {
		var $new_row = $(this).closest('.duplicate-group').find('.cols:last-child').clone();
		$new_row.appendTo($(this).closest('.duplicate-group'));
		$new_row.find('.select-area').remove();
		customForm.customForms.replaceAll();
		setTimeout(function(){
			equalheight('.contacts-sidebar, .contacts-edit');
		}, 200);
		return false;
	});
	$('.ibp-list header input:checkbox').change(function(){
		if($(this).is(':checked')){
			$(this).closest('li').addClass('checked-block');
		} else {
			$(this).closest('li').removeClass('checked-block');
		}
	});
});

$(window).load(function(){
	equalheight('.contacts-sidebar, .contacts-edit');
});
$(window).resize(function(){
	equalheight('.contacts-sidebar, .contacts-edit');
});