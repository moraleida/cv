$(document).ready(function(){
	customForm.lib.domReady(function(){
		customForm.customForms.replaceAll();
	});
	$('label a').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});
	//rule for country code
	$.validator.addMethod('fnType', function(value, element) {
		return value.match(/^\+[0-9]$/);
	},'Enter Valid Country Code');
	//rule for phone number
	$.validator.addMethod('phoneWithMask', function(value, element) {
		return value.match(/^\([0-9][0-9]\) [0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]$/);
	},'Enter Valid Phone');
	//rule for selects
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
		return arg != value;
	}, "This field is required.");
	// Validation plugin
	$('#registration-form form').validate({
		highlight: function(element, errorClass, validClass) {
			$(element).addClass(errorClass).removeClass(validClass);
			$(element).closest('.form-holder').addClass(errorClass).removeClass(validClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).removeClass(errorClass).addClass(validClass);
			$(element).closest('.form-holder').removeClass(errorClass).addClass(validClass);
		},
		rules: {
			'f-name':{
				required: true,
				minlength: 2
			},
			'm-name':{
				required: true,
				minlength: 2
			},
			'l-name':{
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			'p-type': {
				valueNotEquals: 'default'
			},
			'p-type1': {
				valueNotEquals: 'default'
			},
			'p-type2': {
				valueNotEquals: 'default'
			},
			'm-type': {
				valueNotEquals: 'default'
			},
			'm-type1': {
				valueNotEquals: 'default'
			},
			'date-month': {
				valueNotEquals: 'default'
			},
			'identification-type': {
				valueNotEquals: 'default'
			},
			'country': {
				valueNotEquals: 'default'
			},
			'region': {
				valueNotEquals: 'default'
			},
			'state': {
				valueNotEquals: 'default'
			},
			'area': {
				valueNotEquals: 'default'
			},
			'job-title': {
				valueNotEquals: 'default'
			},
			'carrer': {
				valueNotEquals: 'default'
			},
			'identification': {
				minlength: 2,
				required: true
			},
			'date-day': {
				required: true,
				digits: true,
				minlength: 2,
				maxlength:2
			},
			'country-code': {
				fnType: true,
				minlength: 2,
				maxlength:2
			},
			'phone-number': {
				phoneWithMask: true
			},
			'zip': {
				required: true,
				digits: true,
				minlength: 5,
				maxlength:5
			},
			'address': {
				required: true,
				minlength: 3
			},
			'address-2': {
				required: true,
				minlength: 3
			},
			'company': {
				required: true,
				minlength: 3
			},
			'country-code2': {
				fnType: true,
				minlength: 2,
				maxlength:2
			},
			'phone-number2': {
				phoneWithMask: true
			},
			'extension-number': {
				required: true,
				digits: true
			},
			password : {
				minlength : 5,
				required: true
			},
			password_confirm : {
				required: true,
				equalTo : "#password"
			}
		},
		messages: {
			'f-name': 'Please specify your name',
			'm-name': 'Please specify your name',
			'l-name': 'Please specify your name',
			email: {
				required: 'We need your email address to contact you',
				email: 'Enter valid email'
			},
			'phone-number': 'Invalid phone number',
			'address': 'Your address is too short',
			'address-2': 'Your address is too short',
			password:{
				required: 'Enter your password',
				minlength: 'Password is too short'
			},
			password_confirm:{
				required: 'Repeat your password',
				equalTo: 'Password doesn`t match'
			}
		}
	});
	console.log($('.check-select').val());
	//all default messages
	jQuery.extend(jQuery.validator.messages, {
		required: "This field is required.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
	//Take the templates from HTML
	var template_email_ = jQuery.validator.format($.trim($("#template01").html()));
	var template_phone_ = jQuery.validator.format($.trim($("#template02").html()));
	// add more rows on click
	$(".btn-add").click(function(){
		var this_ = $(this),
			i = this_.closest('.add-rows').find('.form-group').length + 1;
		
		if (this_.closest('.add-rows').hasClass('email')){
			$(template_email_(i++)).appendTo(this_.closest('.add-rows'));
		}
		else if (this_.closest('.add-rows').hasClass('phone')){
			$(template_phone_(i++)).appendTo(this_.closest('.add-rows'));
		}
		customForm.customForms.replaceAll();
		return false;
	});
	/*phone01 masc*/
	$('.phone01').mask("(99) 99999-9999");
	$('.phone01').each(function(){
		$(this).on("blur", function() {
			var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
			if( last.length == 3 ) {
				var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
				var lastfour = move + last;
				var first = $(this).val().substr( 0, 9 );
				$(this).val( first + '-' + lastfour );
			}
		});
	});
});