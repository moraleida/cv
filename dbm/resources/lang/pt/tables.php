<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Table Names Language Lines
    |--------------------------------------------------------------------------
    */

    // Abertura
    'campaign' => 'Campanha',
    'city' => 'Cidade (Brasil)',
    'company' => 'Empresa',
    'country' => 'País',
    'department' => 'Departamento',
    'entry_method' => 'Método de Entrada',
    'function' => 'Cargo',
    'interest_group' => 'Grupo de Interesse',
    'intl_city' => 'Cidade (Exterior)',
    'intl_state' => 'Estado/Região (Exterior)',
    'neighborhood' => 'Bairro',
    'origin' => 'Origem',
    'state' => 'Estado/DF (Brasil)',
    'segment' => 'Segmento',
    'list' => 'Lista',

    'energy and fuel' => 'Energia e Combustíveis',
    'products and derivatives' => 'Produtos e Derivados',
    'products and equipment' => 'Produção e Equipamentos',
    'industry' => 'Indústria',
    'services and utilities' => 'Serviços e Utilidades',
    'laws and regulations' => 'Leis e Normas',
];
