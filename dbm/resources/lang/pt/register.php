<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration Language Lines
    |--------------------------------------------------------------------------
    */

    // Abertura
    'main_title'             => 'Inscreva-se',
    'main_description'       => 'Preencha os dados abaixo e receba informações e materiais do setor de petróleo, gás e biocombustíveis de acordo com seu interesse.',
    'secd_description'       => 'Você pode iniciar seu cadastro usando sua conta LinkedIn ou Facebook:',
    'btn_linkedin'           => 'Cadastre-se via LinkedIn',
    'btn_facebook'           => 'Cadastre-se via Facebook',
    'btn_independent'        => 'Cadastre-se pelo formulário',

    // Dados cadastrais
    'personal_title'        => 'Dados cadastrais',
    'first_name'            => 'Nome',
    'middle_name'           => '',
    'last_name'             => 'Sobrenome',
    'email'                 => 'Email',
    'birthday'              => 'Data de aniversário',
    'phone'                 => 'Telefone pessoal',
    'identification'        => 'Identificação',
    'associate'             => 'Associado IBP',
    'whatisthis'            => 'O que significa?',
    'country'               => 'País',
    'address'               => 'Endereço',
    'zipcode'               => 'CEP',
    'street'                => 'Rua',
    'number'                => 'Número',
    'complement'            => 'Complemento',
    'neighborhood'          => 'Bairro',
    'state'                 => 'Estado',
    'city'                  => 'Cidade',
    'company'               => 'Organização',
    'company_name'          => 'Empresa/Organização',
    'departament'           => 'Departamento',
    'job_title'             => 'Cargo',
    'carreer'               => 'Formação',
    'corporate_phone'       => 'Telefone empresarial',
    'extension_number'      => 'Ramal',
    'create_pass'           => 'Crie a sua senha',
    'password'              => 'Senha',
    'confirm_pass'          => 'Confirmar senha',
    'area_of_interest'      => 'Área de interesse',
    'fuel_n_energy'         => 'Energia e Combustíveis',
    'biofuel'               => 'Biocombustíveis',
    'fuel'                  => 'Combustíveis',
    'gas'                   => 'Gás',
    'economic_n_energypolicy'   => 'Economia e Política Energética',
    'industry'                  => 'Indústria',
    'explode_n_product'         => 'Exploração e Produção (E&P)',
    'research_dev_inovation'    => 'Pesquisa, Desenvolvimento e Inovação (PD&I)',
    'petroquimo'                => 'Petroquímica',
    'product_n_deriv'           => 'Produtos e Derivados',
    'alphalt'                   => 'Asfalto',
    'refining'                  => 'Refino',
    'lubrification_n_lubrificate'   => 'Lubrificantes e lubrificação',
    'production_n_equipment'        => 'Produção e Equipamentos',
    'equipments'                    => 'Equipamentos',
    'equipment_inspection'          => 'Inspeção de equipamento',
    'instrument_n_automatic'        => 'Equipamento e Automatização',
    'laboratory'                    => 'Laboratório',
    'exploration_n_production'      => 'Exploração e Produção',
    'sms'                           => 'SMS',
    'maintenance'                   => 'Manutenção',
    'services_n_utilities'          => 'Serviços e Utilidades',
    'logistics_n_distribution'      => 'Logística e Distribuição',
    'goods_n_services'              => 'Bens e Serviços',
    'sustainability'                => 'Sustentabilidade',
    'law_n_regulations'             => 'Leis e Normas',
    'laws_n_regulations'            => 'Legislação e regulamentação',
    'taxation'                      => 'Tributação',
    'normalization'                 => 'Normalização',
    'sign_up'                       => 'Enviar',
    'btn_take_your_photo'           => 'Coloque sua foto',
    'select'                        => 'Selecionar',
    'personal'                      => 'Pessoal',
    'business'                      => 'Comercial',

    //Meses
    'January'       => 'Janeiro', 
    'February'      => 'Fevereiro',  
    'March'         => 'Março', 
    'April'         => 'Abril', 
    'May'           => 'Maio', 
    'June'          => 'Junho', 
    'July'          => 'Julho',  
    'August'        => 'Agosto',  
    'September'     => 'Setembro', 
    'October'       => 'Outubro', 
    'November'      => 'Novembro',  
    'December'      => 'Dezembro',
    
];
