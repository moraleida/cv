@extends('master')

@section('content')
    @include('sub-header')

    <div class="container">
        <div class="feedback-form">
            
            {!! Form::open() !!}

                <p>Caso tenha alguma dúvida relacionada ao sistema, entre em contato conosco.</p>
                <select data-rule-selectcheck="true" name="subject">
                    <option value="0" selected="selected">Assunto</option>
                    <option value="1">Item 1</option>
                    <option value="2">Item 2</option>
                </select>
                <textarea class="form-control" cols="30" rows="10" name="message" placeholder="Mensagem" data-rule-required="true"></textarea>
                <input class="btn btn-submit" type="submit" value="ENVIAR" >

            {!! Form::close() !!}

            <div class="success-box">
                <div class="hold">
                    <span class="str">Sua mensagem foi enviada com sucesso!</span>
                </div>
                <a href="#" class="btn">&lt; VOLTAR</a>
            </div>
        </div>
    </div><!-- /container -->
@stop
