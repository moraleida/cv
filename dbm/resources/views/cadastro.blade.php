<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>IBP Cadastro</title>
	<link rel="stylesheet" href="/css/form.css">
	<link rel="stylesheet" href="/css/styles.css">
	<link rel="stylesheet" href="/css/ajustes.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<script src="/js/custom-form.js"></script>
	<script src="/js/custom-form.select.js"></script>
	<script src="/js/custom-form.checkbox.js"></script>
    <?php //	<script src="/js/custom-form.file.js"></script> ?>
	<script src="/js/custom-form.radio.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
    @if(Config::get('app.locale') == 'en')
	<script src="/js/messages_enUS.js"></script>
    @else
	<script src="/js/messages_ptBR.js"></script>
    @endif
	<script src="/js/placeholders.min.js"></script>
	<script src="/js/jquery.mask.min.js"></script>
	<script src="/js/plupload/plupload.full.min.js"></script>
	<script src="/js/scripts.js"></script>
	<script src="/js/plupload/plupload-settings.js"></script>
    @include('linkedin-sdk')
</head>
<body>
    @include('facebook-sdk')
<div id="registration-form">
    @yield('form')
</div>
</body>
</html>
