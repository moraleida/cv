<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DBM IBP</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400&subset=latin,latin-ext">
    <link rel="stylesheet" href="/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/admin/css/styles.css">
    <script src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/admin/js/messages_pt_BR.js"></script>
    <script src="/js/custom-form.js"></script>
    <script src="/js/custom-form.select.js"></script>
    <script src="/js/custom-form.radio.js"></script>
    <script src="/js/custom-form.file.js"></script>
    <script src="/js/custom-form.checkbox.js"></script>
    <script src="/js/jquery.tablesorter.js"></script>
    <script src="/js/jquery.tablesorter.pager.js"></script>
    <script src="/js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="/js/jquery.fancybox.pack.js"></script>
    <script src="/js/placeholders.min.js"></script>
    <script src="/js/circle-progress.js"></script>
    <script src="/js/jquery.scrolltabs.js"></script>
    <script src="/js/equal-height.js"></script>
    <script src="/admin/js/scripts.js"></script>
</head>
<body>
    <div class="login-area">
        <div class="tbl">
            <div class="tbl-cell">
                <div class="login-form">
                    <strong class="logo"><a href="#">ibp - instituto brasileiro de petroleo, gas e biocombustiveis</a></strong>
                    <header class="head">
                        <h1>dbm</h1>
                    </header>
                   @yield('content')
                </div>
            </div>
        </div>
    </div><!-- /login-area -->
</body>
</html>
