@extends('master')

@section('content')

    @include('sub-header')

    <div class="centered-container">
        <div class="edit-block">

            {!! Form::open() !!}

                <div class="row clearfix">
                    <label for="name">Nome da lista</label>
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="row clearfix">
                    <label for="filter[rules]">Filtrar contatos que satisfaçam</label>
                    {!! Form::select('filter[rules]', [ 'todas' => 'todas as regras', 'qualquer' => 'qualquer regra', null]) !!}
                </div>
                <div class="rules">
                    <div class="group clearfix">
                        {!! Form::select('filter[0][col1]', $filterColumns['column1'], ['class' => 'select filter']) !!}
                        {!! Form::select('filter[0][col2]', $filterColumns['verbs'], ['class' => 'select']) !!}
                        {!! Form::select('filter[0][col3]', [], ['class' => 'select']) !!}
                    </div>
                </div>
                <a href="#" class="btn-add-group">Adicionar nova regra</a>
                <div class="btn-holder clearfix">
                    <div class="pull-right">
                        <button class="btn btn-small" type="submit">Salvar</button>
                        <button class="btn btn-grey btn-small" type="reset">Cancelar</button>
                    </div>
                </div>


                @include('list.templatefilter')
            {!! Form::close() !!}
        </div><!-- /edit-block -->
        @include('list.templatetable')
    </div>

@stop
