@extends('master')

@section('content')

@include('sub-header')

        <div class="container">
            <div class="data">
                <table>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>contatos</th>
                            <th>Ativos</th>
                            <th>inativos</th>
                            <th class="sorter-false">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($lists as $list)
                        <tr>
                            <td><a href="#" class="toggle-children">{{ $list->list }}</a></td>
                            <td>{{ $list->contacts or '0' }}</td>
                            <td>{{ $list->active or '0' }}</td>
                            <td>{{ $list->inactive or '0' }}</td>
                            <td>
                                <div class="content-menu">
                                    <a href="#" class="btn-toggle">toggle menu</a>
                                    <ul>
                                        <li><a class="edit" href="{{ route('list.edit', $list->id) }}">Editar</a></li>
                                        <li><a class="export" href="{{ route('list.export', $list->id) }}">Exportar</a></li>
                                        <li><a class="see" href="{{ route('client.list.from.source', ['source' => 'list', 'id' => $list->id]) }}">Ver Contatos</a></li>
                                        <li><a class="duplicate open-modal" href="#duplicate" rel="{{ route('list.duplicate', $list->id) }}">Duplicar</a></li>
                                        <li class="sep"><a class="delete open-modal" href="#delete" rel="{{ route('list.delete', $list->id) }}">Excluir</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @if(!empty($list->segments))
                            @foreach($list->segments as $segment)
                                <tr class="tablesorter-childRow">
                                    <td>{{ $segment->segment }}</td>
                                    <td>{{ $segment->contacts or '0' }}</td>
                                    <td>{{ $segment->active or '0' }}</td>
                                    <td>{{ $segment->inactive or '0' }}</td>
                                    <td>
                                        <div class="content-menu">
                                            <a href="#" class="btn-toggle">toggle menu</a>
                                            <ul>
                                                <li><a href="{{ route('segment.edit', $segment->id) }}">Editar</a></li>
                                                <li><a href="{{ route('segment.export', $segment->id) }}">Exportar</a></li>
                                                <li><a href="{{ route('client.list.from.source', ['source' => 'segment', 'id' => $segment->id]) }}">Ver Contatos</a></li>
                                                <li><a class="duplicate open-modal" href="#duplicate" rel="{{ route('segment.duplicate', $segment->id) }}">Duplicar</a></li>
                                                <li class="sep"><a class="delete open-modal" href="#delete" rel="{{ route('segment.delete', $segment->id) }}">Excluir</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        @if($list->allowSegments)
                            <tr class="tablesorter-childRow colored">
                                <td colspan="5"><a href="{{ route('segment.create', ['parent' => $list->id]) }}" class="add-segment">Novo Segmento</a></td>
                            </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagination-wrapper">{!! $lists->render() !!}</div>
            @include('modal')
        </div><!-- /container -->
@stop
