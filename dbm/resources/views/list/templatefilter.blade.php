<div id="template_filter" style="display: none;">
    <div class="group clearfix">
        <a href="#" class="btn-remove-group">remove</a>
        {!! Form::select('filter[{0}][col1]', $filterColumns['column1'], ['class' => 'select filter']) !!}
        {!! Form::select('filter[{0}][col2]', $filterColumns['verbs'], ['class' => 'select']) !!}
        {!! Form::select('filter[{0}][col3]', [], ['class' => 'select']) !!}
    </div>
</div>
