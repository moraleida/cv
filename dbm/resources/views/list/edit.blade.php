@extends('master')

@section('content')

    @include('sub-header')

    <script>var autoloadList = true;</script>
    <div class="centered-container">
        <div class="edit-block">

            {!! Form::open() !!}

                <div class="row clearfix">
                    <label for="name">Nome da lista</label>
                    {!! Form::text('name', $list->list, ['class' => 'form-control']) !!}
                </div>
                <div class="row clearfix">
                    <label for="filter[rules]">Filtrar contatos que satisfaçam</label>
                    {!! Form::select('filter[rules]', [ 'todas' => 'todas as regras', 'qualquer' => 'qualquer regra'], $list->rules) !!}
                </div>
                <div id="rules" class="rules">
                    @foreach($list->cols as $key => $filter)
                        <div class="group clearfix">
                            <a href="#" class="btn-remove-group">remove</a>
                            {!! Form::select('filter['.$key.'][col1]', [$filter['col1'][0] => $filter['col1'][1]], $filter['col1'][0], ['class' => 'select filter']) !!}
                            {!! Form::select('filter['.$key.'][col2]', [$filter['col2'][0] => $filter['col2'][1]], $filter['col2'][0], ['class' => 'select']) !!}
                            {!! Form::select('filter['.$key.'][col3]', [$filter['col3'][0] => $filter['col3'][1]], $filter['col3'][0], ['class' => 'select']) !!}
                        </div>
                    @endforeach
                    <?php $key++; ?>
                    <div class="group clearfix">
                        {!! Form::select('filter['.$key.'][col1]', $filterColumns['column1'], ['class' => 'select filter']) !!}
                        {!! Form::select('filter['.$key.'][col2]', $filterColumns['verbs'], ['class' => 'select']) !!}
                        {!! Form::select('filter['.$key.'][col3]', [], ['class' => 'select']) !!}
                    </div>
                </div>
                <a href="#" class="btn-add-group">Adicionar nova regra</a>
                <div class="btn-holder clearfix">
                    <div class="pull-right">
                        <button class="btn btn-small" type="submit">Salvar</button>
                        <button class="btn btn-grey btn-small" type="reset">Cancelar</button>
                    </div>
                </div>

                @include('list.templatefilter')
            {!! Form::close() !!}
        </div><!-- /edit-block -->
        @include('list.templatetable')
    </div>

@stop
