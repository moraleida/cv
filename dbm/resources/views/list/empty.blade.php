@extends('master')

@section('content')

<div class="create-list">
    <div class="tbl">
        <div class="tbl-cell">
            <header>
                <h1>Ops!</h1>
            </header>
            <p>Você ainda não criou uma lista. <br>As listas são um conjunto de e-mails de assinantes. Não criou uma lista ainda? Sem problemas. <br>Clique abaixo em "Criar lista".</p>
            <a href="{{ route('list.create') }}" class="btn">criar lista</a>
        </div>
    </div>
</div><!-- /create-list -->

@stop
