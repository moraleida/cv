@if (count($errors) > 0)
    <div id="errorMsg">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
@endif
