<div class="hidden-modal">
    <div class="modal" id="duplicate">
        <h3>duplicar <span></span></h3>
        {!! Form::open(['id' => 'act']) !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome da lista ou segmento']) !!}
            {!! Form::hidden('href', null, ['id' => 'href', 'class' => 'form-control']) !!}
            <div class="btn-holder clearfix">
                {!! Form::submit('duplicar', ['class' => 'btn btn-submit']) !!}
                {!! Form::reset('cancelar', ['class' => 'btn btn-grey btn-submit']) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="hidden-modal">
    <div class="modal" id="delete">
        <h3>excluir <span></span>?</h3>
        <p>Esta ação não pode ser desfeita</p>
        {!! Form::open(['id' => 'act']) !!}
            <div class="btn-holder clearfix">
                {!! Form::submit('excluir', ['class' => 'btn btn-submit']) !!}
                {!! Form::reset('cancelar', ['class' => 'btn btn-grey btn-submit']) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
