<script>
// Substitui caracteres acentuados por seus similares para fins de busca
// ref.: http://stackoverflow.com/a/14480470/1001109
var accent_fold = (function () {
    var accent_map = {
        'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', // a
        'ç': 'c',                                                   // c
        'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e',                     // e
        'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',                     // i
        'ñ': 'n',                                                   // n
        'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ø': 'o', // o
        'ß': 's',                                                   // s
        'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u',                     // u
        'ÿ': 'y'                                                    // y
    };

    return function accent_fold(s) {
        if (!s) { return ''; }
        var ret = '';
        for (var i = 0; i < s.length; i++) {
            ret += accent_map[s.charAt(i)] || s.charAt(i);
        }
        return ret;
    };
} ());

  function doLoginFB() {
      FB.login(function(response) {
          if(response.status == 'connected')
              populateFields(response);
      }, {scope: 'public_profile,email,user_birthday,user_location,user_work_history'});
  }

  function populateFields(user) {
    FB.api('/me', function(response) {
        resp = response;
        var bday = response.birthday.split('/');
        var locId = response.location.id;
        var emailType = getDomainType(response.email);

        // Preenche dados básicos de identificação
        $('#f-name').val(response.first_name);
        $('#l-name').val(response.last_name);
        $('#email').val(response.email);
        $('.email #type').val(emailType).prop('selected', true).trigger('change');

        $('#birthday').val(bday[1]);
        $('#birthmonth').val(bday[0]);
        $('#company').val(response.work[0].employer.name);
        $('a.btn-form').click();

        // Busca e faz upload da imagem de perfil
        FB.api('/'+response.id+'/picture?width=100&height=100', function(image) {
            var img = image;
            $.ajax({
                method: 'POST',
                url: '/api/image/social',
                data: {img: image.data.url},
                beforeSend: function(request) {
                    request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                }
            }).done(function(response) {
                var img_holder = $('.photo-upload .img');
                var img_placeholder = img_holder.find('img').eq(0);
                if( img_placeholder.length < 1 ) {
                    img_holder.html('').append('<img src="#" />');
                    img_placeholder = img_holder.find('img').eq(0);
                }
                var img_input = $('input[name="uploadedpath"]');
                img_input.val(response.filename);
                img_placeholder.attr('src', img.data.url);
            });
        });

        // Busca e preenche os dados completos de localização (país, estado,cidade)
        FB.api('/'+locId, function(loc) {
            $.each($('#businesscountry > option'), function() {
                if($(this).text() == loc.location.country) {
                    $('#businesscountry').val($(this).val()).prop('selected', true).trigger('change');
                    return false;
                }
            });

            window.setTimeout(function() {
                $.each($('#businessstate > option'), function() {
                    if($(this).attr('rel') == loc.location.state) {
                        $('#businessstate').val($(this).val()).prop('selected', true).trigger('change');
                        return false;
                    }
                });

                window.setTimeout(function() {
                    $.each($('#businesscity > option'), function() {
                        if(accent_fold($(this).text()).toLowerCase() == accent_fold(loc.location.city).toLowerCase()) {
                            $('#businesscity').val($(this).val()).prop('selected', true).trigger('change');
                            return false;
                        }
                    });
                    customForm.customForms.destroyAll();
                    customForm.customForms.replaceAll();
                }, 2000); // Tempo para garantir que o select estará preenchido com as cidades
            }, 2000); // Tempo para garantir que o select estará preenchido com os estados
        });

    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '497646617049498',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });
  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

</script>
