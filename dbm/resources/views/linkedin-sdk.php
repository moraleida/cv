<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key:   776hilmd4dydjm
    authorize: false 
    onLoad: onLinkedInLoad
</script>
<script type="text/javascript">

    function liAuth(){
       IN.User.authorize(function(response){
            //
       });
    }
    
    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);
    }

    // Handle the successful return from the API call
    function onSuccess(response) {
        response = response.values[0];
        emailType = getDomainType(response.emailAddress);
        // Preenche dados básicos de identificação
        $('#f-name').val(response.firstName);
        $('#l-name').val(response.lastName);
        $('#email').val(response.emailAddress);
        $('.email #type').val(emailType).prop('selected', true).trigger('change');

        for(var i = 0; i < response.positions.values.length; i++ ) {
            var this_ = response.positions.values[i];
            if(this_.isCurrent === true) {
                if(typeof(this_.company.name != 'undefined')) {
                    $('#company').val(this_.company.name);
                }
                if(typeof(this_.title != 'undefined')) {
                    $.each($('#companyrole > option'), function() {
                        if(accent_fold($(this).text()).toLowerCase() == accent_fold(this_.title).toLowerCase()) {
                            $('#companyrole').val($(this).val()).prop('selected', true).trigger('change');
                            return false;
                        }
                    });
                }

                break;
            }

        }
        customForm.customForms.destroyAll();
        customForm.customForms.replaceAll();
        $('a.btn-form').click();

        if(response.pictureUrl.length > 1) {
            var img = response.pictureUrl;
            $.ajax({
                method: 'POST',
                url: '/api/image/social',
                data: {img: response.pictureUrl},
                beforeSend: function(request) {
                    request.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                }
            }).done(function(response) {
                var img_holder = $('.photo-upload .img');
                var img_placeholder = img_holder.find('img').eq(0);
                if( img_placeholder.length < 1 ) {
                    img_holder.html('').append('<img src="#" />');
                    img_placeholder = img_holder.find('img').eq(0);
                }
                var img_input = $('input[name="uploadedpath"]');
                img_input.val(response.filename);
                img_placeholder.attr('src', img);
            });
        }
    }

    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
//        IN.API.Raw("/people/~").result(onSuccess).error(onError);
        IN.API.Profile("me")
            .fields('firstName', 'lastName', 'picture-url', 'location', 'positions', 'email-address')
            .result(onSuccess);
    }

</script>
