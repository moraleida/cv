@include('servermsg')
<div class="heading">
    <div class="container">
        @if($headerbtn)
            <div class="pull-right">
                <a href="{{ route($headerbtnroute) }}" class="btn">{{ $headerbtntext }}</a>
            </div>
        @endif
        <h1 class="marked">{{ $pagetitle }}</h1>
    </div>
</div><!-- /heading -->
