@extends('master')

@section('content')

    @include('sub-header')

    <script>var autoloadList = true;</script>
    <div class="centered-container">
        <div class="edit-block">

            {!! Form::open(['class' => 'segment']) !!}

                <div class="row clearfix">
                    <label for="name">Nome do segmento</label>
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! Form::hidden('parent', $list->id, ['class' => 'form-control']) !!}
                </div>
                <div class="row clearfix">
                    <label for="filter[parent]">Lista de origem</label>
                    {!! Form::select('filter[parent]', [$list->id => $list->list], ['class' => 'select filter'], ['disabled']) !!}
                </div>
                <span id="rules">
                    <div class="row clearfix">
                        <label for="filter[rules]">Filtrar contatos que satisfaçam</label>
                        {!! Form::select('filter[rules]', [ 'todas' => 'todas as regras', 'qualquer' => 'qualquer regra'], $list->rules, ['disabled']) !!}
                    </div>
                    <div class="rules">
                        @foreach($list->cols as $key => $filter)
                            <div class="group clearfix disabled">
                                {!! Form::select('filter['.$key.'][col1]', [$filter['col1'][0] => $filter['col1'][1]], ['class' => 'select filter'], ['disabled']) !!}
                                {!! Form::select('filter['.$key.'][col2]', [$filter['col2'][0] => $filter['col2'][1]], ['class' => 'select'], ['disabled']) !!}
                                {!! Form::select('filter['.$key.'][col3]', [$filter['col3'][0] => $filter['col3'][1]], ['class' => 'select'], ['disabled']) !!}
                            </div>
                        @endforeach
                    </div>
                </span>
                    <a href="#" class="btn-add-group">Adicionar nova regra</a>
                <div class="btn-holder clearfix">
                    <div class="pull-right">
                        <button class="btn btn-small" type="submit">Salvar</button>
                        <button class="btn btn-grey btn-small" type="reset">Cancelar</button>
                    </div>
                </div>

                @include('list.templatefilter')
            {!! Form::close() !!}
        </div><!-- /edit-block -->
        @include('list.templatetable')
    </div>

@stop
