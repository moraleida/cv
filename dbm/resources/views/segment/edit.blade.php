@extends('master')

@section('content')

    @include('sub-header')

    <script>var autoloadList = true;</script>
    <div class="centered-container">
        <div class="edit-block">

            {!! Form::open(['class' => 'segment']) !!}

                <div class="row clearfix">
                    <label for="name">Nome do segmento</label>
                    {!! Form::text('name', $list->segment, ['class' => 'form-control']) !!}
                    {!! Form::hidden('parent', $list->id_list, ['class' => 'form-control']) !!}
                    {!! Form::hidden('skip', $list->skip, ['class' => 'form-control']) !!}
                </div>
                <span id="rules">
                <div class="row clearfix">
                    <label for="filter[parent]">Lista de origem</label>
                    {!! Form::select('filter[parent]', [$list->id_list => $list->parent], ['class' => 'select filter'], ['disabled']) !!}
                </div>
                <div class="row clearfix">
                    <label for="filter[rules]">Filtrar contatos que satisfaçam</label>
                    {!! Form::select('filter[rules]', [ 'todas' => 'todas as regras', 'qualquer' => 'qualquer regra' ], $list->rule, ['disabled']) !!}
                </div>
                <div class="rules">
                    <?php $i=0; ?>
                    @foreach($list->cols as $key => $filter)
                    <div class="group clearfix {{ ($i < $list->skip ? 'disabled' : '') }}">
                            @if( $i < $list->skip )
                                {!! Form::select('filter['.$key.'][col1]', [$filter['col1'][0] => $filter['col1'][1]], ['class' => 'select filter'], ($i<$list->skip ? ['disabled'] : [] )) !!}
                                {!! Form::select('filter['.$key.'][col2]', [$filter['col2'][0] => $filter['col2'][1]], ['class' => 'select'], ($i<$list->skip ? ['disabled'] : [] )) !!}
                                {!! Form::select('filter['.$key.'][col3]', [$filter['col3'][0] => $filter['col3'][1]], ['class' => 'select'], ($i<$list->skip ? ['disabled'] : [] )) !!}
                            @else
                                <a href="#" class="btn-remove-group">remove</a>
                                {!! Form::select('filter['.$key.'][col1]', $filterColumns['column1'], $filter['col1'][0], ['class' => 'select filter']) !!}
                                {!! Form::select('filter['.$key.'][col2]', $filterColumns['verbs'], $filter['col2'][0], ['class' => 'select']) !!}
                                {!! Form::select('filter['.$key.'][col3]', [$filter['col3'][0] => $filter['col3'][1]], $filter['col3'][0], ['class' => 'select']) !!}
                            @endif
                        </div>
                    <?php $i++; ?>
                    @endforeach
                </div></span>
                <a href="#" class="btn-add-group">Adicionar nova regra</a>
                <div class="btn-holder clearfix">
                    <div class="pull-right">
                        <button class="btn btn-small" type="submit">Salvar</button>
                        <button class="btn btn-grey btn-small" type="reset">Cancelar</button>
                    </div>
                </div>

                @include('list.templatefilter')
            {!! Form::close() !!}
        </div><!-- /edit-block -->
        @include('list.templatetable')
    </div>

@stop
