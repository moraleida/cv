@extends('master')

@section('content')

            <div class="welcome-block">
                <header>
                    <h1>Olá, <strong class="marked">{{ $current_user->name.' '.$current_user->lastname }}</strong> seja bem-vindo.</h1>
                </header>
                <ul class="statistics">
                    <li><a href="{{ route('campaign.list') }}">
                    <strong class="value">{{ $campaigns }}</strong>
                        <span class="name">campanhas</span>
                    </a></li>
                    <li><a href="{{ route('campaign.list') }}">
                        <strong class="value">16</strong>
                        <span class="name">enviadas</span>
                    </a></li>
                    <li><a href="{{ route('list.list') }}">
                    <strong class="value">{{ $lists }}</strong>
                        <span class="name">listas</span>
                    </a></li>
                    <li><a href="{{ route('client.list') }}">
                    <strong class="value">{{ $clients }}</strong>
                        <span class="name">cONTATOS</span>
                    </a></li>
                </ul>
                <div class="graph-holder">
                    <img src="/admin/images/img-02.jpg" alt="image description">
                </div>
            </div>
@stop
