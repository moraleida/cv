@extends('master')

@section('content')
    @include('sub-header')

            <div class="container">
                <form action="#">
                    <div class="contact-filter">
                        <div class="clearfix">
                            <select class="cs2">
                                <option>PASTAS</option>
                                <option>PASTAS</option>
                                <option>PASTAS</option>
                            </select>
                            <select class="cs2 sel-02">
                                <option>filtrar por</option>
                                <option>filtrar por</option>
                            </select>
                            <select class="cs2 sel-02">
                                <option>mover para</option>
                                <option>mover para</option>
                            </select>
                        </div>
                    </div>
                    <ul class="data-listing">
                        <li>
                            <input class="chk chk2" type="checkbox" >
                            <div class="base-inf">
                                <strong class="name">Campanha 1</strong>
                                <span class="status">Status: Rascunho</span>
                            </div>
                            <span class="time">Agendado para <em>(a definir)</em></span>
                            <div class="tools">
                                <a href="#" class="btn-graph">graph</a>
                                <div class="content-menu">
                                    <a href="#" class="btn-toggle">toggle menu</a>
                                    <ul>
                                        <li><a href="#">Editar</a></li>
                                        <li><a href="#">Duplicar</a></li>
                                        <li class="sep"><a href="#">Excluir</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <input class="chk chk2" type="checkbox" >
                            <div class="base-inf">
                                <strong class="name">Campanha 2</strong>
                                <span class="status success">Status: Enviado</span>
                            </div>
                            <span class="time">Enviado em 15/01/2015 14:30</span>
                            <div class="stat-data">
                                <strong class="value">42</strong>
                                <span class="unit">inscritos</span>
                            </div>
                            <div class="stat-data">
                                <strong class="value">27%</strong>
                                <span class="unit">clicados</span>
                            </div>
                            <div class="stat-data">
                                <strong class="value">36%</strong>
                                <span class="unit">abertos</span>
                            </div>
                            <div class="tools">
                                <a href="#" class="btn-graph">graph</a>
                                <div class="content-menu">
                                    <a href="#" class="btn-toggle">toggle menu</a>
                                    <ul>
                                        <li><a href="#">Editar</a></li>
                                        <li><a href="#">Duplicar</a></li>
                                        <li class="sep"><a href="#">Excluir</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <input class="chk chk2" type="checkbox" >
                            <div class="base-inf">
                                <strong class="name">Campanha 3</strong>
                                <span class="status">Status: Rascunho</span>
                            </div>
                            <span class="time">Agendado para 15/01/2015 14:30</span>
                            <div class="tools">
                                <a href="#" class="btn-graph">graph</a>
                                <div class="content-menu">
                                    <a href="#" class="btn-toggle">toggle menu</a>
                                    <ul>
                                        <li><a href="#">Editar</a></li>
                                        <li><a href="#">Duplicar</a></li>
                                        <li class="sep"><a href="#">Excluir</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <input class="chk chk2" type="checkbox" >
                            <div class="base-inf">
                                <strong class="name">Campanha 4</strong>
                                <span class="status success">Status: Enviado</span>
                            </div>
                            <span class="time">Enviado em 15/01/2015 14:30</span>
                            <div class="stat-data">
                                <strong class="value">42</strong>
                                <span class="unit">inscritos</span>
                            </div>
                            <div class="stat-data">
                                <strong class="value">27%</strong>
                                <span class="unit">clicados</span>
                            </div>
                            <div class="stat-data">
                                <strong class="value">36%</strong>
                                <span class="unit">abertos</span>
                            </div>
                            <div class="tools">
                                <a href="#" class="btn-graph">graph</a>
                                <div class="content-menu">
                                    <a href="#" class="btn-toggle">toggle menu</a>
                                    <ul>
                                        <li><a href="#">Editar</a></li>
                                        <li><a href="#">Duplicar</a></li>
                                        <li class="sep"><a href="#">Excluir</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </form>
            </div><!-- /container -->
@stop
