        <!-- template for add email row -->
        <div id="template01" style="display: none;">
            <div class="form-holder form-group remove-row">
                <div class="inputs-row">
                    <div class="item width01">
                        {!! Form::email('emailaddr[{0}][]', null, ['id' => 'email', 'class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-email' => 'true']) !!}
                    </div>
                    <div class="item width02">
                        {!! Form::select('emailtype[{0}][]', ['' => $strings['select'], 'pessoal' => 'Personal', 'comercial' => 'Business'], null, ['id' => 'type', 'required' => 'required']) !!}
                    </div>
                    <div class="btn-remove">
                        remove
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!-- template for add phone row -->
        <div id="template02" style="display: none;">
            <div class="form-holder form-group">
                <div class="inputs-row">
                    <div class="item width03">
                        {!! Form::text('phoneint[{0}][]', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '55', 'maxlength' => '3']) !!}
                    </div>
                    <div class="item width04">
                        {!! Form::text('phonenum[{0}][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '(21) 7575 7575']) !!}
                    </div>
                    <div class="item width05">
                        {!! Form::select('phonetype[{0}][]', ['' => $strings['select'], 'fixo' => 'Landline', 'celular' => 'Mobile'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
                    </div>
                    <div class="btn-remove">
                        remove
                    </div>
                </div>
            </div>
        </div>
        <!-- template for add company phone row -->
        <div id="template03" style="display: none;">
            <div class="form-holder form-group">
                <div class="inputs-row">
                    <div class="item width03">
                        {!! Form::text('company[phoneint][{0}][]', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '55', 'maxlength' => '3']) !!}
                    </div>
                    <div class="item width04">
                        {!! Form::text('company[phonenum][{0}][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '(21) 7575 7575']) !!}
                    </div>
                    <div class="item width05">
                        {!! Form::select('company[phonetype][{0}][]', ['' => $strings['select'], 'residencial' => 'Home', 'comercial' => 'Business', 'celular' => 'Mobile'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
                    </div>
                    <div class="btn-remove has-ext">
                        remove
                    </div>
                </div>
            </div>
        </div>
        <!-- Extension company phone -->
        <div id="template04" style="display: none;">
            <div class="form-holder form-holder-width02" style="margin-top: 11px;">
                {!! Form::text('company[phoneext][{0}][]', null, ['class' => 'form-control append-extension', 'data-extension' => '']) !!}
            </div>
        </div>
