@extends('master')

@section('content')
    @include('sub-header')

    <div class="container">
        {!! Form::open() !!}
            <div class="contact-filter clearfix">
                {!! Form::select('list', $lists, null, ['class' => 'cs2']) !!}
                {!! Form::select('segment', $segments, null, ['class' => 'cs2']) !!}
            </div>
        {!! Form::close() !!}
            <div class="search-form client clearfix">
                {!! Form::select('searchfield', ['' => 'campo', 'name' => 'Nome', 'email' => 'E-mail', 'phone' => 'Telefone', 'function' => 'Cargo', 'company' => 'Organização'], null, ['class' => 'cs2', 'id' => 'searchfield']) !!}
                {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Buscar', 'id' => 'searchtext']) !!}
                {!! Form::submit('busca', ['class' => 'btn btn-small btn-grey btn-submit']) !!}
            </div>
            <div id="datatable" class="data zebra">
                <table>
                    <thead>
                        <tr>
                            <th>
                                <div class="chk-hold">
                                    <input class="chk check-all" type="checkbox" >
                                </div>
                            </th>
                            <th>Nome completo</th>
                            <th>e-mail</th>
                            <th>Telefone</th>
                            <th>cargo</th>
                            <th>Organização</th>
                            <th class="sorter-false">
                                <div class="content-menu">
                                    <a href="#" class="btn-toggle light">toggle menu</a>
                                    <ul>
                                        <li><a class="delete open-modal" href="#delete" rel="{{ route('client.delete', '0') }}">Excluir</a></li>
                                    </ul>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="paginate">
                        @foreach($clients as $client)
                        <tr>
                            <td><input class="chk" type="checkbox" ></td>
                            <td>
                                <div class="base-inf">
                                    @if(!empty($client->image))
                                        <img src="{{ $client->image }}" alt="image description">
                                    @endif
                                    <strong class="name"><a href="{{ route('client.edit', $client->id) }}">{{ $client->name.' '.$client->lastname }}</a></strong>
                                    <div class="tip-info">
                                        @if(!empty($client->image))
                                            <img src="{{ $client->image }}" alt="image description" class="avatar">
                                        @endif
                                        <header>
                                            <strong class="f-name vip">{{ $client->name.' '.$client->lastname }}</strong>
                                            <strong class="role">{{  $client->function[0]->name or '' }}<span class="marked"> em</span></strong>
                                            <strong class="company">{{  $client->company[0] or '' }}</strong>
                                        </header>
                                        <strong class="type">{{ $client->type or '' }}</strong>
                                        <div class="holder clearfix">
                                            <div class="col">
                                                <dl class="dlist">
                                                    <dt>Aniversário</dt>
                                                    @if(isset($client->birthday))
                                                    <dd>{{ strftime('%d de %B', strtotime($client->birthday)) }}</dd>
                                                    @endif;
                                                </dl>
                                            </div>
                                            <div class="col">
                                                <dl class="dlist">
                                                    <dt>Telefone</dt>
                                                    @foreach($client->phones as $phone)
                                                    <dd>{{ '+'.$phone->country_code.' ('.$phone->area_code.') '.$phone->phone.' ['.$phone->extension.']' }}</dd>
                                                    @endforeach
                                                </dl>
                                            </div>
                                        </div>
                                        <dl class="dlist">
                                            <dt>email</dt>
                                            <dd>
                                            @if(!empty($client->emails))
                                                <a href="mailto:{{ $client->emails[0] }}">{{ $client->emails[0] }}</a>
                                            @endif
                                            </dd>
                                            <dt>endereço</dt>
                                            <dd>
                                            @if(!empty($client->address))
                                                <span class="str">{{ $client->address[0]->street.', '.$client->address[0]->number.' '.$client->address[0]->complement }}</span>
                                                <span class="str">{{ $client->address[0]->city.' - '.$client->address[0]->state }}</span>
                                                <span class="str">{{ $client->address[0]->country }}</span>
                                            @endif
                                            </dd>
                                        </dl>
                                        <ul class="socials">
                                            <li><a href="#" class="ico-01">facebook</a></li>
                                            <li><a href="#" class="ico-02">linked in</a></li>
                                        </ul>
<?php /*                                        
                                        <ul class="stat-list">
                                            <li>
                                                <figure class="circle-graph" data-value="75">
                                                    <canvas></canvas>
                                                    <figcaption><span class="circle-value">837</span></figcaption>
                                                </figure>
                                                <strong class="gr-name">emails enviados</strong>
                                            </li>
                                            <li>
                                                <figure class="circle-graph" data-value="70">
                                                    <canvas></canvas>
                                                    <figcaption><span class="circle-value">70</span>%</figcaption>
                                                </figure>
                                                <strong class="gr-name">abertos</strong>
                                            </li>
                                            <li>
                                                <figure class="circle-graph" data-value="52">
                                                    <canvas></canvas>
                                                    <figcaption><span class="circle-value">52</span>%</figcaption>
                                                </figure>
                                                <strong class="gr-name">clicados</strong>
                                            </li>
                                            <li>
                                                <figure class="circle-graph" data-value="12">
                                                    <canvas></canvas>
                                                    <figcaption><span class="circle-value">12</span>%</figcaption>
                                                </figure>
                                                <strong class="gr-name">spam</strong>
                                            </li>
                                            </ul>
 */ ?>                                            
                                    </div>
                                </div>
                            </td>
                            <td>
                                @if(!empty($client->emails))
                                    <a href="mailto:{{ $client->emails[0] }}">{{ $client->emails[0] }}</a>
                                @endif
                            </td>
                            <td>+{{ $phone->country_code or '' }}
                                ({{ $phone->area_code or '' }})
                                {{ $phone->phone or '' }}
                                [{{$phone->extension or '' }}]</td>
                            <td>{{ $client->function[0]->name or '' }}</td>
                            <td>{{ $client->company[0] or '' }}</td>
                            <td>
                                <div class="content-menu">
                                    <a href="#" class="btn-toggle">toggle menu</a>
                                    <ul>
                                        <li><a href="{{ route('client.edit', $client->id) }}">Editar</a></li>
                                        <li class="sep"><a class="delete open-modal" href="#delete" rel="{{ route('client.delete', $client->id) }}">Excluir</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        <div id="pagination" class="pagination-wrapper">{!! $clients->render() !!}</div>
        @include('modal')
    </div><!-- /container -->
@stop
