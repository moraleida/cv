@extends('master')
@section('content')
@include('sub-header')

<div class="container">
    <header class="headline">
        <div class="pull-right">
            <a href="#first_table" class="btn btn-history">visualizar histórico</a>
        </div>
        <h2 class="marked">Editar {{ $client->name.' '.$client->lastname }}</h2>
    </header><!-- end headline -->
    <section class="contacts-area">
<?php /*
        <nav class="navbar">
            <div class="content-menu">
                <a href="#" class="btn-toggle">toggle menu</a>
                <ul>
                    <li><a href="#">Detalhes do contato</a></li>
                    <li><a href="#">Visualizações e Respostas</a></li>
                    <li><a href="#">Descadastrar</a></li>
                    <li><a href="#">Adicionar contato a outra lista</a></li>
                    <li><a href="#">Deletar contato</a></li>
                </ul>
            </div>
        </nav><!-- end navbar -->
*/?>
        <div class="contacts-cols">
            <aside class="contacts-sidebar">
                <figure class="profile-name">
                    <div class="profile-photo">
                        @if(!empty($client->image))
                        <img src="{{ $client->image }}" alt="image">
                        @else
                            <img src="/admin/images/img-001.png" alt="image">
                        @endif
                        <a href="#" class="btn-remove">remove</a>
                    </div><!-- end profile-photo -->
                    <figcaption>
                        <h1>{{ $client->name.' '.$client->lastname }}</h1>
                        @if(isset($client->function[0]))
                        <p>{{ $client->function[0]->function.' ' }}<span>em</span><br><strong>{{ $client->function[0]->company }}</strong></p>
                        @endif
                    </figcaption>
                </figure><!-- end profile-name -->
                <ul class="socials">
                    <li><a href="#" class="ico-01">facebook</a></li>
                    <li><a href="#" class="ico-02">linked in</a></li>
                </ul>
                @if(isset($client->emails[0]))
                <strong class="profile-email"><a href="mailto:{{ $client->emails[0]->email }}">{{ $client->emails[0]->email }}</a></strong>
                @endif;
                <div class="contacts-box">
                    <h2>{{ $client->type }}</h2>
                    @if(isset($client->address[0]))
                    <p>{{ $client->address[0]->street.', '.$client->address[0]->number.' '.$client->address[0]->complement }}</p>
                    <p>{{ $client->address[0]->city.' - '.$client->address[0]->state }}</p>
                    <p>{{ $client->address[0]->country }}</p>
                    @endif
                    @if(count($client->phones) > 0)
                    <ul class="phones">
                        @foreach($client->phones as $phone)
                        <li>+{{ $phone->country_code }} {{ $phone->area_code or '' }} {{ $phone->phone }} {{ $phone->extension or '' }}</li>
                        @endforeach
                    </ul><!-- end phones -->
                    @endif
                </div><!-- end contacts-box -->
                @if(count($client->interests) > 1)
                <ul class="tags">
                    @foreach($client->interests as $interest)
                    <li><a href="#">{{ trans('tables.'.strtolower($interest->name)) }}</a></li>
                    @endforeach
                </ul><!-- end tags -->
                @endif
<?php /*
                <ul class="stat-list">
                    <li>
                        <figure class="circle-graph" data-value="75">
                            <canvas></canvas>
                            <figcaption><span class="circle-value">837</span></figcaption>
                        </figure>
                        <strong class="gr-name">emails enviados</strong>
                    </li>
                    <li>
                        <figure class="circle-graph" data-value="70">
                            <canvas></canvas>
                            <figcaption><span class="circle-value">70</span>%</figcaption>
                        </figure>
                        <strong class="gr-name">abertos</strong>
                    </li>
                    <li>
                        <figure class="circle-graph" data-value="52">
                            <canvas></canvas>
                            <figcaption><span class="circle-value">52</span>%</figcaption>
                        </figure>
                        <strong class="gr-name">clicados</strong>
                    </li>
                    <li>
                        <figure class="circle-graph" data-value="12">
                            <canvas></canvas>
                            <figcaption><span class="circle-value">12</span>%</figcaption>
                        </figure>
                        <strong class="gr-name">spam</strong>
                    </li>
                </ul>
*/ ?>
            </aside><!-- end contacts-sidebar -->
            <div class="contacts-edit">
                {!! Form::open() !!}
                    {!! Form::hidden('uploadedpath', null) !!}
                    {!! Form::hidden('cadastrointerno', 'true') !!}
                    {!! Form::hidden('groupsize', 'true', ['id' => 'groupsize']) !!}
                    <section class="section general-section">
                        <div class="section-container">
                            <div class="cols">
                                <div class="col">
                                    <h2>geral</h2>
                                </div><!-- end col -->
                                <div class="col">
                                    <div class="vip-box">
                                        <i class="ico-star {{ $client->vipClass }}">&nbsp;</i>
                                        <span>Contato VIP</span>
                                        {!! Form::hidden('vip', $client->vip) !!}
                                    </div><!-- end vip-box -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols">
                                <div class="col">
                                    <label for="lbl-001">Nome<strong>*</strong></label>
                                    <div class="input-holder">
                                        {!! Form::text('name', $client->name, ['id'=>'f-name', 'rel' => 'f-name', 'class' => 'form-control', 'data-rule-required' => 'true']) !!}
                                    </div><!-- end input-holder -->
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-002">Sobrenome<strong>*</strong></label>
                                    <div class="input-holder">
                                        {!! Form::text('lastname', $client->lastname, ['id'=>'l-name', 'rel' => 'f-name', 'class' => 'form-control', 'data-rule-required' => 'true']) !!}
                                    </div><!-- end input-holder -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols titles-cols">
                                <div class="col">
                                    <label for="lbl-003">Email<strong>*</strong></label>
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-004">Tipo de email<strong>*</strong></label>
                                </div><!-- end col -->
                            </div><!-- end cols -->
                             <div class="outer-group" rel="email">
                                @for($i = 0; $i < count($client->emails); $i++)
                                    <div class="duplicate-group">
                                        @if($i == 0)
                                        <a href="#" class="btn-addrow btn-addrow-fix">+</a>
                                        @else
                                        <a href="#" class="btn-removerow">-</a>
                                        @endif
                                        <div class="cols">
                                            <div class="col">
                                                <div class="input-holder type2">
                                                    {!! Form::email('emailaddr['.$i.'][]', $client->emails[$i]->email, ['id'=>'email', 'rel' => 'email', 'class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-email' => 'true']) !!}
                                                </div><!-- end input-holder -->
                                            </div><!-- end col -->
                                            <div class="col">
                                                <div class="input-holder">
                                                    {!! Form::select('emailtype['.$i.'][]', [''=> 'Selecionar', 'pessoal' => $strings['personal'], 'comercial' => $strings['business']], $client->emails[$i]->type, ['id' => 'type', 'required' => 'required'], ['class'=> 'form-control', 'data-rule-required'=> 'true']) !!}
                                                </div><!-- end input-holder -->
                                            </div><!-- end col -->
                                        </div><!-- end cols -->
                                    </div><!-- end duplicate-group -->
                                @endfor
                             </div>
                            <div class="cols titles-cols">
                                <div class="col">
                                    <label for="lbl-005">Telefone<strong>*</strong></label>
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-006">Tipo de telefone<strong>*</strong></label>
                                </div><!-- end col -->
                            </div><!-- end cols -->
                             <div class="outer-group" rel="phone">
                                @for($i = 0; $i < count($client->phones); $i++)
                                <div class="duplicate-group">
                                    @if($i == 0)
                                    <a href="#" class="btn-addrow btn-addrow-fix">+</a>
                                    @else
                                    <a href="#" class="btn-removerow">-</a>
                                    @endif
                                    <div class="cols">
                                        <div class="col">
                                            <div class="phone-number">
                                                {!! Form::text('phoneint['.$i.'][]', $client->phones[$i]->country_code, ['class' => 'form-control phone-code', 'data-rule-number' => 'true', 'data-rule-required' => 'true', 'placeholder' => "55", 'maxlength' => '2']) !!}
                                                {!! Form::text('phonenum['.$i.'][]', $client->phones[$i]->area_code.$client->phones[$i]->phone, ['class' => 'form-control phone01', 'data-rule-required' => 'true', 'placeholder' => "(21) 7575-7575"]) !!}
                                            </div><!-- end phone-number -->
                                        </div><!-- end col -->
                                        <div class="col">
                                            {!! Form::select('phonetype['.$i.'][]', [''=> 'Selecionar', 'fixo' => 'Fixo', 'celular' => 'Celular'], $client->phones[$i]->type, ['id' => 'phonetype', 'required' => 'required']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                </div><!-- end duplicate-group -->
                                @endfor
                            </div>
                            <div class="cols">
                                <div class="col">
                                    <div class="input-holder">
                                            <label for="lbl-007">Data de nascimento<strong>*</strong></label>
                                            <div class="item width01">
                                                {!! Form::text('birthday', $client->birthday[2], ['id' => 'birthday', 'class' => 'form-control', 'maxlength' => '2', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                                            </div>
                                            <div class="item width02">
                                                {!! Form::select('birthmonth', [''=> 'Selecionar', '01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro'], $client->birthday[1], ['id' => 'birthmonth', 'class' => 'select-required', 'data-rule-required' => 'true']) !!}
                                            </div>
                                            <div class="cols"></div>
                                            <label for="lbl-007">Documento<strong>*</strong></label>
                                            <div class="item width03">
                                                {!! Form::text('identification', $client->identification, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                                            </div>
                                            <div class="item width04">
                                                {!! Form::select('identificationtype', [''=> 'Selecionar', 'rg' => 'RG', 'cpf' => 'CPF', 'cnh' => 'CNH', 'passaporte' => 'Passaporte', 'social-security' => 'Social Security', 'outro' => 'Outro'], $client->identification_type, ['id' => 'identificationtype', 'required' => 'required']) !!}
                                            </div>
                                    </div><!-- end input-holder -->
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-008">Redes Sociais</label>
                                    <div class="input-holder type4">
                                        <div class="item width02" style="width: 164px;">
                                            {!! Form::text('social[0][url]', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="item width04">
                                            {!! Form::select('social[0][type]', ['' => 'Selecionar', 'Facebook' => 'Facebook', 'LinkedIn' => 'LinkedIn'], null, []) !!}
                                        </div>
                                    </div><!-- end input-holder -->
                                    <div class="input-holder type4">
                                        <div class="item width02" style="width: 164px;">
                                            {!! Form::text('social[1][url]', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="item width04">
                                            {!! Form::select('social[1][type]', ['' => 'Selecionar', 'Facebook' => 'Facebook', 'LinkedIn' => 'LinkedIn'], null, []) !!}
                                        </div>
                                    </div><!-- end input-holder -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols">
                                <div class="col">
                                    <label>Língua de preferência</label>
                                    <ul class="radio-group">
                                        <li>
                                        <input id="lbl-009" name="language" value="pt" type="radio" class="radio" {{ ($client->language == 'pt' ? 'checked="checked"' : '') }}>
                                            <label for="lbl-009">Português</label>
                                        </li>
                                        <li>
                                            <input id="lbl-010" name="language" value="en" type="radio" class="radio" {{ ($client->language == 'en' ? 'checked="checked"' : '') }}>
                                            <label for="lbl-010">English</label>
                                        </li>
                                    </ul><!-- end radio-group -->
                                </div><!-- end col -->
                                <div class="col">
                                    <label>Estrangeiro?</label>
                                    <ul class="radio-group">
                                        <li>
                                            <input id="lbl-011" name="foreigner" value="yes" type="radio" class="radio" {{ ($client->foreigner == 1 ? 'checked="checked"' : '') }}>
                                            <label for="lbl-011">Sim</label>
                                        </li>
                                        <li>
                                            <input id="lbl-012" name="foreigner" value="no" type="radio" class="radio" {{ ($client->foreigner == 0 ? 'checked="checked"' : '') }}>
                                            <label for="lbl-012">Não</label>
                                        </li>
                                    </ul><!-- end radio-group -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div id="first_table" class="table-block">
                                <h3>HISTÓRICO</h3>
                                <table>
                                    <tr>
                                        <th class="cell-001">ID</th>
                                        <th class="cell-002">NOME</th>
                                        <th class="cell-003">NOME DO MEIO</th>
                                        <th class="cell-004">SOBRENOME</th>
                                        <th class="cell-005">EMAIL</th>
                                        <th class="cell-006">TIPO</th>
                                        <th class="cell-007">DATA MODIFICAÇÃO</th>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td></td>
                                        <td>Alex</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>24/04/2015</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td></td>
                                        <td>Alek</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>24/04/2015</td>
                                    </tr>
                                </table>
                                <footer>
                                    <a href="#" class="btn">VER TODOS</a>
                                </footer>
                            </div><!-- end table-block -->
                        </div><!-- end section-container -->
                    </section><!-- end section -->
                    <section class="section address-section">
                        <div class="section-container">
                            <h2>endereços</h2>
                            <div class="outer-group" rel="address">
                                @for($i = 0; $i<count($client->address); $i++)
                                <div class="duplicate-group">
                                    @if($i == 0)
                                    <a href="#" class="btn-addrow btn-addrow-fix">+</a>
                                    @else
                                    <a href="#" class="btn-removerow">-</a>
                                    @endif
                                    <div class="cols">
                                            <div class="col">
                                                <label for="lbl-013">País<strong>*</strong></label>
                                                <div class="input-holder type3">
                                                    {!! Form::select('business[0][country]', App\Lib\Helpers::get_country_list(), $client->address[$i]->id_country, ['class' => 'businesscountry', 'required' => 'required']) !!}
                                                </div><!-- end input-holder -->
                                            </div><!-- end col -->
                                            <div class="col">
                                                <label for="lbl-014">CEP<strong>*</strong></label>
                                                <div class="input-holder type5 correcao-cep">
                                                    {!! Form::text('business[0][zip]', $client->address[$i]->zip, ['class' => 'form-control zip', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                                                </div><!-- end input-holder -->
                                            </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols titles-cols address-cols home_address">
                                        <div class="cols"></div>
                                        <div class="col col-001">
                                            <label for="lbl-015">Endereço<strong>*</strong></label>
                                        </div><!-- end col -->
                                        <div class="col col-002">
                                            <label for="lbl-016">Número<strong>*</strong></label>
                                        </div><!-- end col -->
                                        <div class="col col-003">
                                            <label for="lbl-017">Complemento<strong>*</strong></label>
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols titles-cols address-cols intl_address">
                                        <div class="cols"></div>
                                        <div class="col col-001">
                                            <label for="lbl-015">Endereço<strong>*</strong></label>
                                        </div><!-- end col -->
                                        <div class="col col-003">
                                            <label for="lbl-017">Complemento<strong>*</strong></label>
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols address-cols home_address">
                                        <div class="col col-001">
                                            {!! Form::text('business[0][address][street]', $client->address[$i]->street, ['class' => 'disabled rua form-control', 'data-rule-required' => 'true']) !!}
                                        </div><!-- end col -->
                                        <div class="col col-002">
                                            {!! Form::text('business[0][address][number]', $client->address[$i]->number, ['class' => 'disabled numero form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                        <div class="col col-003">
                                            {!! Form::text('business[0][address][complement]', $client->address[$i]->complement, ['class' => 'disabled complemento form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols address-cols intl_address">
                                        <div class="col col-001">
                                            {!! Form::text('business[0][intl_address][street]', null, ['class' => 'disabled rua form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                        <div class="col col-003">
                                            {!! Form::text('business[0][intl_address][complement]', null, ['class' => 'complemento disabled form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="columns">
                                        <div class="column home_address">
                                            <label for="lbl-018">Bairro<strong>*</strong></label>
                                            {!! Form::text('business[0][address][neighborhood]', $client->address[$i]->neighborhood, ['class' => 'disabled bairro form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end column -->
                                        <div class="column">
                                            <label for="lbl-019">Cidade<strong>*</strong></label>
                                            {!! Form::select('business[0][city]', [$client->address[$i]->id_city => $client->address[$i]->city], $client->address[$i]->id_city, ['class' => 'disabled businesscity', 'required' => 'required', 'disabled' => 'disabled']) !!}
                                        </div><!-- end column -->
                                        <div class="column">
                                            <label for="lbl-020">Estado<strong>*</strong></label>
                                            {!! Form::select('business[0][state]', [$client->address[$i]->id_state => $client->address[$i]->state], $client->address[$i]->id_state, ['class' => 'disabled businessstate', 'required' => 'required', 'disabled' => 'disabled']) !!}
                                        </div><!-- end column -->
                                    </div><!-- end cols -->
                                </div><!-- end duplicate-group -->
                                @endfor
                            </div>
                            <div class="table-block">
                                <h3>HISTÓRICO</h3>
                                <table>
                                    <tr>
                                        <th class="cell-001">ID</th>
                                        <th class="cell-002">CEP</th>
                                        <th class="cell-003">ENDEREÇO</th>
                                        <th class="cell-008">NÚMERO</th>
                                        <th class="cell-009">CIDADE</th>
                                        <th class="cell-006">PAÍS</th>
                                        <th class="cell-007">DATA MODIFICAÇÃO</th>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td></td>
                                        <td></td>
                                        <td>0089</td>
                                        <td></td>
                                        <td></td>
                                        <td>01/05/2015</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td></td>
                                        <td></td>
                                        <td>0028</td>
                                        <td></td>
                                        <td></td>
                                        <td>24/04/2015</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td></td>
                                        <td></td>
                                        <td>1200</td>
                                        <td></td>
                                        <td></td>
                                        <td>24/04/2015</td>
                                    </tr>
                                </table>
                                <footer>
                                    <a href="#" class="btn">VER TODOS</a>
                                </footer>
                            </div><!-- end table-block -->
                        </div><!-- end section-container -->
                    </section><!-- end section -->
                    <section class="section corporate-section">
                        <div class="section-container">
                            <h2>corporativo</h2>
                            <div class="outer-group" rel="company">
                                @for($i = 0; $i < count($client->function); $i++)
                                <div class="duplicate-group">
                                    @if($i == 0)
                                    <a href="#" class="btn-addrow adjustment-btn-addrow btn-addrow-fix">+</a>
                                    @else
                                    <a href="#" class="btn-removerow">-</a>
                                    @endif
                                    <div class="cols titles-cols">
                                        <div class="col">
                                            <label for="lbl-021">Empresa/Organização<strong>*</strong></label>
                                        </div><!-- end col -->
                                        <div class="col">
                                            <label for="lbl-022">Cargo<strong>*</strong></label>
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols address-cols">
                                        <div class="col">
                                            {!! Form::text('company['.$i.'][name]', $client->function[$i]->company, ['class' => 'form-control', 'data-rule-required' => 'true']) !!}
                                        </div><!-- end col -->
                                        <div class="col">
                                            {!! Form::select('company['.$i.'][role]', [''=> 'Selecionar', 'engineer' => 'Engineer', 'technician' => 'Technician'], $client->function[$i]->function, ['id' => 'companyrole', 'data-rule-required' => 'true']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols">
                                        <div class="col">
                                            <label for="lbl-023">Departamento<strong>*</strong></label>
                                            {!! Form::select('company['.$i.'][area]', [''=> 'Selecionar', 'engineer' => 'Engineer', 'technician' => 'Technician'], $client->function[$i]->id_department, ['id' => 'companyarea', 'data-rule-required' => 'true']) !!}
                                        </div><!-- end col -->
                                        <div class="col">
                                            <label for="lbl-024">Formação</label>
                                            {!! Form::select('company['.$i.'][career]', [''=> 'Selecionar', 'engineer' => 'Engineer', 'technician' => 'Technician'], null, ['id' => 'companycareer']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                </div><!-- end duplicate-group -->
                                @endfor
                            </div>
                            <div class="cols">
                                <div class="col">
                                    <label for="lbl-025">Tipo de sócio<strong>*</strong></label>
                                    {!! Form::select('associate_type', ['nao-associado' => 'Não Associado', 'coletivo' => 'Coletivo', 'cooperador' => 'Cooperador', 'emerito' => 'Emérito', 'entidade' => 'Entidade', 'individual' => 'Individual', 'patrimonial' => 'Patrimonial'], $client->type, ['data-rule-required' => 'true', 'required' => 'required']) !!}
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-026">Origem<strong>*</strong></label>
                                    {!! Form::select('origin', $origins, $client->id_origin, ['data-rule-required' => 'true', 'required' => 'required']) !!}
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols">
                                <div class="col">
                                    <label for="lbl-027">Código do sócio</label>
                                    <div class="input-holder type6">
                                        {!! Form::text('associate_code', $client->associate_code, ['class' => 'form-control']) !!}
                                    </div><!-- end input-holder -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="table-block">
                                <h3>HISTÓRICO</h3>
                                <table>
                                    <tr>
                                        <th class="cell-001">ID</th>
                                        <th class="cell-010">EMPRESA</th>
                                        <th class="cell-011">DEPARTAMENTO</th>
                                        <th class="cell-012">CARCO</th>
                                        <th class="cell-013">TIPO</th>
                                        <th class="cell-014">ORIGEM</th>
                                        <th class="cell-007">DATA MODIFICAÇÃO</th>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td></td>
                                        <td>Engenharia</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>24/04/2015</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td></td>
                                        <td>T.l</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>24/04/2015</td>
                                    </tr>
                                </table>
                                <footer>
                                    <a href="#" class="btn">VER TODOS</a>
                                </footer>
                            </div><!-- end table-block -->
                        </div><!-- end section-container -->
                    </section><!-- end section -->
                    <section class="section ibp-section">
                        <div class="section-container">
                            <h2>ibp</h2>
                            <ul class="ibp-list">
                                <li class="checked-block">
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Energy and Fuel"].']', 1, (in_array($interest_groups['Energy and Fuel'], $client->interestIDs) ? true : false), ['class' => 'chk']) !!}
                                        <label for="lbl-028">energia e combustíveis</label>
                                    </header>
                                    <ul>
                                        <li>Biocombustíveis</li>
                                        <li>Combustíveis</li>
                                        <li>Gás</li>
                                        <li>Economia e Política Energética</li>
                                    </ul>
                                </li>
                                <li>
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Industry"].']', 1, (in_array($interest_groups['Industry'], $client->interestIDs) ? true : false), ['class' => 'chk']) !!}
                                        <label for="lbl-029">indústria</label>
                                    </header>
                                    <ul>
                                        <li>Exploração e Produção (E&amp;P)</li>
                                        <li>Pesquisa, desenvolvimento e Inovação (PD&amp;I)</li>
                                        <li>Petroquímica</li>
                                    </ul>
                                </li>
                                <li>
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Products and Derivatives"].']', 1, (in_array($interest_groups['Products and Derivatives'], $client->interestIDs) ? true : false), ['class' => 'chk']) !!}
                                        <label for="lbl-030">produtos e derivados</label>
                                    </header>
                                    <ul>
                                        <li>Asfalto</li>
                                        <li>Refino</li>
                                        <li>Lubrificantes e Lubrificação</li>
                                    </ul>
                                </li>
                                <li class="checked-block">
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Products and Equipment"].']', 1, (in_array($interest_groups['Products and Equipment'], $client->interestIDs) ? true : false), ['class' => 'chk']) !!}
                                        <label for="lbl-031">produção e equipamentos</label>
                                    </header>
                                    <ul>
                                        <li>Equipamentos</li>
                                        <li>Inspeção de equipamentos</li>
                                        <li>Instrumentação e Automação</li>
                                        <li>Laboratório</li>
                                        <li>Exploração e produção</li>
                                        <li>SMS</li>
                                        <li>Manutenção</li>
                                    </ul>
                                </li>
                                <li class="checked-block">
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Services and Utilities"].']', 1, (in_array($interest_groups['Services and Utilities'], $client->interestIDs) ? true : false), ['class' => 'chk']) !!}
                                        <label for="lbl-032">serviços e utilidades</label>
                                    </header>
                                    <ul>
                                        <li>Logística e Distribuição</li>
                                        <li>Bens e Serviços</li>
                                        <li>Sustentabilidade</li>
                                    </ul>
                                </li>
                                <li class="checked-block">
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Laws and Regulations"].']', 1, (in_array($interest_groups['Laws and Regulations'], $client->interestIDs) ? true : false), ['class' => 'chk']) !!}
                                        <label for="lbl-033">leis e normas</label>
                                    </header>
                                    <ul>
                                        <li>Legislação e Regulamentação</li>
                                        <li>Tributação</li>
                                        <li>Normalização</li>
                                    </ul>
                                </li>
                            </ul><!-- end ibp-list -->
                            <footer class="btn-row">
                                <input value="Salvar" class="btn" type="submit">
                                <a href="#" class="btn btn-grey">Cancelar</a>
                            </footer><!-- end btn-row -->
                        </div><!-- end section-container -->
                    </section><!-- end section -->
                </form>
            </div><!-- end contacts-edit -->
        </div><!-- end contacts-cols -->
    </section><!-- end contacts-area -->
</div><!-- end container -->
@include('client.templatesinclude')
@stop
