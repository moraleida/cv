<div id="template_filter_email" style="display:none">
    <div class="duplicate-group">
        <a href="#" class="btn-removerow">-</a>
        <div class="cols">
            <div class="col">
                <div class="input-holder type2">
                    {!! Form::email('emailaddr[{0}][]', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-email' => 'true']) !!}
                </div><!-- end input-holder -->
            </div><!-- end col -->
            <div class="col">
                <div class="input-holder">
                    {!! Form::select('emailtype[{0}][]', [ '' => $strings['select'], 'pessoal' => $strings['personal'], 'comercial' => $strings['business']], null, ['id' => 'type', 'required' => 'required']) !!}
                </div><!-- end input-holder -->
            </div><!-- end col -->
        </div><!-- end cols -->
    </div><!-- end duplicate-group -->
</div>
<div id="template_filter_phone" style="display:none">
    <div class="duplicate-group">
        <a href="#" class="btn-removerow">-</a>
        <div class="cols">
            <div class="col">
                <div class="phone-number">
                    {!! Form::text('phoneint[{0}][]', null, ['class' => 'form-control phone-code', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                    {!! Form::text('phonenum[{0}][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true']) !!}
                </div><!-- end phone-number -->
            </div><!-- end col -->
            <div class="col">
                {!! Form::select('phonetype[{0}][]', ['fixo' => 'Fixo', 'celular' => 'Celular'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
            </div><!-- end col -->
        </div><!-- end cols -->
    </div><!-- end duplicate-group -->
</div>
<div id="template_filter_corporate_phone" style="display:none">
    <div class="duplicate-group">
        <a href="#" class="btn-removerow">-</a>
        <div class="cols">
            <div class="col">
                <div class="phone-number">
                    {!! Form::text('company[phoneint][{0}][]', null, ['class' => 'form-control phone-code', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                    {!! Form::text('company[phonenum][{0}][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true']) !!}
                </div><!-- end phone-number -->
            </div><!-- end col -->
            <div class="col">
                {!! Form::select('company[phonetype][{0}][]', ['fixo' => 'Fixo', 'celular' => 'Celular'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
            </div><!-- end col -->
        </div><!-- end cols -->
    </div><!-- end duplicate-group -->
</div>
<div id="template_filter_address" style="display:none;">
    <div class="duplicate-group">
        <a href="#" class="btn-removerow">-</a>
        <div class="cols">
                <div class="col">
                    <label for="lbl-013">País<strong>*</strong></label>
                    <div class="input-holder type3">
                        {!! Form::select('business[{0}][country]', App\Lib\Helpers::get_country_list(), null, ['class' => 'businesscountry', 'required' => 'required']) !!}
                    </div><!-- end input-holder -->
                </div><!-- end col -->
                <div class="col">
                    <label for="lbl-014">CEP<strong>*</strong></label>
                    <div class="input-holder type5 correcao-cep">
                        {!! Form::text('business[{0}][zip]', null, ['class' => 'form-control zip disabled', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'disabled' => 'disabled']) !!}
                    </div><!-- end input-holder -->
                </div><!-- end col -->
        </div><!-- end cols -->
        <div class="cols titles-cols address-cols home_address">
            <div class="cols"></div>
            <div class="col col-001">
                <label for="lbl-015">Endereço<strong>*</strong></label>
            </div><!-- end col -->
            <div class="col col-002">
                <label for="lbl-016">Número<strong>*</strong></label>
            </div><!-- end col -->
            <div class="col col-003">
                <label for="lbl-017">Complemento<strong>*</strong></label>
            </div><!-- end col -->
        </div><!-- end cols -->
        <div class="cols titles-cols address-cols intl_address">
            <div class="cols"></div>
            <div class="col col-001">
                <label for="lbl-015">Endereço<strong>*</strong></label>
            </div><!-- end col -->
            <div class="col col-003">
                <label for="lbl-017">Complemento<strong>*</strong></label>
            </div><!-- end col -->
        </div><!-- end cols -->
        <div class="cols address-cols home_address">
            <div class="col col-001">
                {!! Form::text('business[{0}][address][street]', null, ['class' => 'disabled rua form-control', 'data-rule-required' => 'true', 'disabled' => 'disabled']) !!}
            </div><!-- end col -->
            <div class="col col-002">
                {!! Form::text('business[{0}][address][number]', null, ['class' => 'disabled numero form-control', 'data-rule-required' => 'true', 'disabled' => 'disabled']) !!}
            </div><!-- end col -->
            <div class="col col-003">
                {!! Form::text('business[{0}][address][complement]', null, ['class' => 'disabled complemento form-control', 'disabled' => 'disabled']) !!}
            </div><!-- end col -->
        </div><!-- end cols -->
        <div class="cols address-cols intl_address">
            <div class="col col-001">
                {!! Form::text('business[{0}][intl_address][street]', null, ['class' => 'disabled rua form-control', 'data-rule-required' => 'true', 'disabled' => 'disabled']) !!}
            </div><!-- end col -->
            <div class="col col-003">
                {!! Form::text('business[{0}][intl_address][complement]', null, ['class' => 'complemento disabled form-control', 'data-rule-required' => 'true', 'disabled' => 'disabled']) !!}
            </div><!-- end col -->
        </div><!-- end cols -->
        <div class="columns">
            <div class="column home_address">
                <label for="lbl-018">Bairro<strong>*</strong></label>
                {!! Form::text('business[{0}][address][neighborhood]', null, ['class' => 'disabled bairro form-control', 'data-rule-required' => 'true', 'disabled' => 'disabled']) !!}
            </div><!-- end column -->
            <div class="column">
                <label for="lbl-019">Cidade<strong>*</strong></label>
                {!! Form::select('business[{0}][city]', ['select'], null, ['class' => 'disabled businesscity', 'required' => 'required', 'disabled' => 'disabled']) !!}
            </div><!-- end column -->
            <div class="column">
                <label for="lbl-020">Estado<strong>*</strong></label>
                {!! Form::select('business[{0}][state]', ['select'], null, ['class' => 'disabled businessstate', 'required' => 'required', 'disabled' => 'disabled']) !!}
            </div><!-- end column -->
        </div><!-- end cols -->
    </div><!-- end duplicate-group -->
</div>
<div id="template_filter_company" style="display:none;">
    <div class="duplicate-group">
        <a href="#" class="btn-removerow">-</a>
        <div class="cols titles-cols">
            <div class="col">
                <label for="lbl-021">Empresa/Organização<strong>*</strong></label>
            </div><!-- end col -->
            <div class="col">
                <label for="lbl-022">Cargo<strong>*</strong></label>
            </div><!-- end col -->
        </div><!-- end cols -->
            <div class="cols address-cols">
                <div class="col">
                    {!! Form::text('company[{0}][name]', null, ['class' => 'form-control', 'data-rule-required' => 'true']) !!}
                </div><!-- end col -->
                <div class="col">
                    {!! Form::select('company[{0}][role]', App\Lib\Helpers::get_names_list('function'), null, ['id' => 'companyrole', 'data-rule-required' => 'true']) !!}
                </div><!-- end col -->
            </div><!-- end cols -->
        <div class="cols">
            <div class="col">
                <label for="lbl-023">Departamento<strong>*</strong></label>
                {!! Form::select('company[{0}][area]', App\Lib\Helpers::get_names_list('department'), null, ['id' => 'companyarea', 'data-rule-required' => 'true']) !!}
            </div><!-- end col -->
            <div class="col">
                <label for="lbl-024">Formação</label>
                {!! Form::select('company[{0}][career]', App\Lib\Helpers::get_names_list('carreer'), null, ['id' => 'companycareer', 'data-rule-required' => 'true']) !!}
            </div><!-- end col -->
        </div><!-- end cols -->
    </div><!-- end duplicate-group -->
</div>
