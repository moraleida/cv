@extends('master')

@section('content')
    @include('sub-header')

            <div class="container">
                <div class="import-contacts">
                    <form action="#">
                        <ul class="contacts-accordion">
                            <li>
                                <header>
                                    <input id="lbl-001" name="contacts-type" value="val-001" type="radio" class="radio" checked="checked">
                                    <label for="lbl-001">Lista de E-mails (separados por virgulas)</label>
                                </header>
                                <div class="collapse-block">
                                    <textarea cols="1" rows="1"></textarea>
                                </div><!-- end collapse-block -->
                            </li>
                            <li>
                                <header>
                                    <input id="lbl-002" name="contacts-type" value="val-002" type="radio" class="radio">
                                    <label for="lbl-002">Arquivo Padrão DBM IBP (CSV)</label>
                                </header>
                                <div class="collapse-block">
                                    <input type="file">
                                </div><!-- end collapse-block -->
                            </li>
                            <li>
                                <header>
                                    <input id="lbl-003" name="contacts-type" value="val-003" type="radio" class="radio">
                                    <label for="lbl-003">Arquivo Outlook</label>
                                </header>
                                <div class="collapse-block">
                                    <input type="file">
                                </div><!-- end collapse-block -->
                            </li>
                            <li>
                                <header>
                                    <input id="lbl-004" name="contacts-type" value="val-004" type="radio" class="radio">
                                    <label for="lbl-004">Arquivo WCARD</label>
                                </header>
                                <div class="collapse-block">
                                    <input type="file">
                                </div><!-- end collapse-block -->
                            </li>
                        </ul><!-- end contacts-accordion -->
                        <input value="importar" class="btn" type="submit">
                    </form>
                </div><!-- end import-contacts -->
            </div><!-- end container -->
@stop
