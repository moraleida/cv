@extends('cadastro')

@section('form')

    @if (count($errors) > 0)
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif

    {!! Form::open() !!}


    <header class="headline container">
        <h1>{{ trans('register.main_title') }}</h1>
        <p>{{ trans('register.main_description') }}</p>
        <p>{{ trans('register.secd_description') }}</p>
        <a href="#" class="btn-linkedin" onclick="liAuth()">{{ trans('register.btn_linkedin') }}</a>
        <br>
        <a href="#" class="btn-facebook" onclick="doLoginFB()">{{ trans('register.btn_facebook') }}</a>
        <br>
        <a href="#" class="btn-form">{{ trans('register.btn_independent') }}</a>
    </header><!-- /headline -->

    <div class="container">
        <h2>{{ trans('register.personal_title') }}</h2>
        <div class="profile-photo photo-upload">
            <div class="img">
                <img src="/images/img-01.png" alt="image">
            </div>
            <div class="upload-file">
                <span class="button">{{ trans('register.btn_take_your_photo') }}</span>
                <input id="photo" title="upload your photo" name="photo" type="file">
            </div>
            <input name="uploadedpath" type="hidden">
            
        <!--     {!! Form::file('photo', ['id' => 'photo', 'title' => 'upload your photo']) !!}
            {!! Form::hidden('uploadedpath', null) !!} -->

        </div><!-- / photo-upload -->
        <div class="row space01">
            <div class="col col01">
                <label for="name">{{ trans('register.first_name') }}<span class="marked">*</span></label>
                <div class="form-holder">{!! Form::text('name', null, ['id' => 'f-name', 'class' => 'form-control', 'data-rule-required' => 'true']) !!}</div>
            </div>
<!--             <div class="col col01">
                <label for="middlename">{{ trans('register.middle_name') }}<span class="marked">*</span></label>
                <div class="form-holder">{!! Form::text('middlename', null, ['id' => 'm-name', 'class' => 'form-control']) !!}</div>
            </div> -->
            <div class="col col01">
                <label for="lastname">{{ trans('register.last_name') }}<span class="marked">*</span></label>
            <div class="form-holder">{!! Form::text('lastname', null, ['id' => 'l-name', 'class' => 'form-control']) !!}</div>
            </div>
        </div>
        <div class="row">
            <div class="col col-left">
                <label for="email">{{ trans('register.email') }}<span class="marked">*</span></label>
                <div class="add-rows email">
                    <a class="btn-add" href="#">add</a>
                    <div class="form-holder form-group">
                        <div class="inputs-row">
                            <div class="item width01">
                                {!! Form::email('emailaddr[0][]', null, ['id' => 'email', 'class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-email' => 'true']) !!}
                            </div>
                            <div class="item width02">
                                {!! Form::select('emailtype[0][]', [ '' => $strings['select'], 'pessoal' => $strings['personal'], 'comercial' => $strings['business']], null, ['id' => 'type', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-right">
                <label for="birthday">{{ trans('register.birthday') }}<span class="marked">*</span></label>
                <div class="form-holder form-group form-holder-width01">
                    <div class="inputs-row">
                        <div class="item width01">
                            {!! Form::text('birthday', null, ['id' => 'birthday', 'class' => 'form-control', 'maxlength' => '2', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                        </div>
                        <div class="item width02">
                            {!! Form::select('birthmonth', ['' => $strings['select'], '01' => trans('register.January'), '02' => trans('register.February'), '03' => trans('register.March'), '04' => trans('register.April'), '05' => trans('register.May'), '06' => trans('register.June'), '07' => trans('register.July'), '08' => trans('register.August'), '09' => trans('register.September'), '10' => trans('register.October'), '11' => trans('register.November'), '12' => trans('register.December')], null, ['id' => 'birthmonth', 'class' => 'select-required', 'data-rule-required' => 'true']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-left">
                <label for="phone">{{ trans('register.phone') }}<span class="marked">*</span></label>
                <div class="add-rows phone">
                    <a class="btn-add" href="#">add</a>
                    <div class="form-holder form-group">
                        <div class="inputs-row">
                            <div class="item width03">
                                {!! Form::text('phoneint[0][]', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '55', 'maxlength' => '3']) !!}
                            </div>
                            <div class="item width04">
                                {!! Form::text('phonenum[0][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '(21) 7575 7575']) !!}
                            </div>
                            <div class="item width05">
                                {!! Form::select('phonetype[0][]', ['' => $strings['select'], 'fixo' => 'Landline', 'celular' => 'Mobile'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-right">
                <label for="identification">{{ trans('register.identification') }}<span class="colored">*</span></label>
                <div class="form-holder form-group">
                    <div class="inputs-row">
                        <div class="item width03">
                            {!! Form::text('identification', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                        </div>
                        <div class="item width04">
                            {!! Form::select('identificationtype', ['' => $strings['select'], 'rg' => 'RG', 'cpf' => 'CPF', 'cnh' => 'CNH', 'passaporte' => 'Passport', 'social-security' => 'Social Security', 'outro' => 'Other'], null, ['id' => 'identificationtype', 'required' => 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="checkbox-holder">
                    <input name="associate" id="associate" type="checkbox" checked>
                    <label for="associate">{{ trans('register.associate') }}<a href="#"> {{ trans('register.whatisthis') }}</a></label>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h2>{{ trans('register.address') }}</h2>
        <div class="row">
            <div class="col col-left">
                <label for="businesscountry">{{ trans('register.country') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width01">
                    {!! Form::select('business[country]', App\Lib\Helpers::get_country_list(), null, ['id' => 'businesscountry', 'required' => 'required', 'class' => 'pais']) !!}
                </div>
            </div>
            <div class="col col-right">

                <label for="businesszip">{{ trans('register.zipcode') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width02">
                    <div class="inputs-row inputs-row-02">
                        <div class="item">
                            {!! Form::text('business[zip]', null, ['class' => 'zip disabled form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'maxlength' => '9', 'readonly' => 'readonly']) !!}
                        </div>
<?php /*
                        <div class="item width002">
                              {!! Form::text('business[zip]-02', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'maxlength' => '3']) !!}
                              </div>
 */ ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row home_address">
            <div class="col col-left">
                <label for="businessaddress">{{ trans('register.street') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::text('business[address][street]', null, ['class' => 'rua disabled form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                </div>
                <label for="businessaddress">{{ trans('register.neighborhood')}}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::text('business[address][neighborhood]', null, ['class' => 'bairro disabled form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                </div>
            </div>
            <div class="col col-right">
                <label for="businessaddress">{{ trans('register.number') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width02">
                    {!! Form::text('business[address][number]', null, ['class' => 'numero disabled form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                </div>
                <label for="businessaddress">{{ trans('register.complement') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::text('business[address][complement]', null, ['class' => 'complemento disabled form-control', 'readonly' => 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row intl_address" style="display: none">
            <div class="col col-left">
                <label for="businessaddress">Street<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::text('business[intl_address][street]', null, ['class' => 'rua disabled form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                </div>
            </div>
            <div class="col col-right">
                <label for="businessaddress">Complement<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::text('business[intl_address][complement]', null, ['class' => 'complemento disabled form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-left">
                <label for="businessstate">{{ trans('register.state') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width01">
                    {!! Form::select('business[state]', ['' => $strings['select']], null, ['id' => 'businessstate', 'class' => 'businessstate disabled', 'required' => 'required', 'readonly' => 'readonly']) !!}
                </div>
            </div>
            <div class="col col-right">
                <label for="bsuinesscity">{{ trans('register.city') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::select('business[city]', ['' => $strings['select']], null, ['id' => 'businesscity', 'class' => 'businesscity disabled', 'required' => 'required', 'readonly' => 'readonly']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h2>{{ trans('register.company') }}</h2>
        <div class="row">
            <div class="col col-left">
                <label for="company">{{ trans('register.company_name') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width01">
                    {!! Form::text('company[name]', null, ['id' => 'company', 'class' => 'form-control', 'data-rule-required' => 'true']) !!}
                </div>
            </div>
            <div class="col col-right">
                <label for="companyarea">{{ trans('register.departament') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::select('company[area]',  App\Lib\Helpers::get_names_list('department'), null, ['id' => 'companyarea', 'data-rule-required' => 'true']) !!}

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-left">
                <label for="companyrole">{{ trans('register.job_title') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width01">
                    {!! Form::select('company[role]',  App\Lib\Helpers::get_names_list('function'), null, ['id' => 'companyrole', 'data-rule-required' => 'true']) !!}
                </div>
            </div>
            <div class="col col-right">
                <label for="companycareer">{{ trans('register.carreer') }}</label>
                <div class="form-holder form-holder-width03">
                    {!! Form::select('company[career]',  App\Lib\Helpers::get_names_list('carreer'), null, ['id' => 'companycareer', 'data-rule-required' => 'true']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-left">
                <label for="lbl19">{{ trans('register.corporate_phone') }}<span class="marked">*</span></label>
                <div class="add-rows company-phone">
                    <a class="btn-add" href="#">add</a>
                    <div class="form-holder form-group">
                        <div class="inputs-row">
                            <div class="item width03">
                                {!! Form::text('company[phoneint][0][]', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '55', 'maxlength' => '3']) !!}
                            </div>
                            <div class="item width04">
                                {!! Form::text('company[phonenum][0][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '(21) 7575 7575']) !!}
                            </div>
                            <div class="item width05">
                                {!! Form::select('company[phonetype][0][]', ['' => $strings['select'], 'fixo' => 'Landline', 'celular' => 'Mobile'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-right">
                <label for="lbl20">{{ trans('register.extension_number') }}</label>
                <div class="form-holder form-holder-width02">
                    {!! Form::text('company[phoneext][0][]', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- template for add phone row -->
        <!-- <div id="template03" style="display: none;">
            <div class="form-holder form-group">
                <div class="inputs-row">
                    <div class="item width03">
                        {!! Form::text('company[phoneint][{0}][]', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '55', 'maxlength' => '3']) !!}
                    </div>
                    <div class="item width04">
                        {!! Form::text('company[phonenum][{0}][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'placeholder' => '(21) 7575 7575']) !!}
                    </div>
                    <div class="item width05">
                        {!! Form::select('company[phonetype][{0}][]', ['residencial' => 'Home', 'comercial' => 'Business', 'celular' => 'Mobile'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
                    </div>
                </div>
            </div>
        </div> -->
<!--         <div id="template04" style="display: none;">
            <div class="form-holder form-holder-width02" style="margin-top: 11px;">
                {!! Form::text('company[phoneext][{0}][]', null, ['class' => 'form-control']) !!}
            </div>
        </div> -->
    </div>
    <div class="container">
        <h2>{{ trans('register.create_pass') }}</h2>
        <div class="row">
            <div class="col col-left">
                <label for="password">{{ trans('register.password') }}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::password('password', ['class' => 'form-control', 'data-rule-required' => 'true']) !!}
                </div>
            </div>
            <div class="col col-right">
                <label for="password_confirmation">{{ trans('register.confirm_pass')}}<span class="marked">*</span></label>
                <div class="form-holder form-holder-width03">
                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'data-rule-required' => 'true']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h2>{{ trans('register.area_of_interest') }}</h2>
        <ul class="interests">
            <li>
                {!! Form::checkbox('interests[1]', 1, false, ['class' => 'chk']) !!}
                <label for="lbl23">{{ trans('register.fuel_n_energy') }}</label>
                <p>{{ trans('register.biofuel') }}</p>
                <p>{{ trans('register.fuel') }}</p>
                <p>{{ trans('register.gas') }}</p>
                <p>{{ trans('register.economic_n_energypolicy') }}</p>
            </li>
            <li>
                {!! Form::checkbox('interests[2]', 1, false, ['class' => 'chk']) !!}
                <label for="lbl24">{{ trans('register.industry') }}</label>
                <p>{{ trans('register.explode_n_product') }}</p>
                <p>{{ trans('register.research_dev_inovation') }}</p>
                <p>{{ trans('register.petroquimo') }}</p>
            </li>
            <li>
                {!! Form::checkbox('interests[3]', 1, false, ['class' => 'chk']) !!}
                <label for="lbl25">{{ trans('register.product_n_deriv')}}</label>
                <p>{{ trans('register.alphalt')}}</p>
                <p>{{ trans('register.refining')}}</p>
                <p>{{ trans('register.lubrification_n_lubrificate')}}</p>
            </li>
            <li>
                {!! Form::checkbox('interests[4]', 1, false, ['class' => 'chk']) !!}
                <label for="lbl26">{{ trans('register.production_n_equipment') }}</label>
                <p>{{ trans('register.equipments') }}</p>
                <p>{{ trans('register.equipment_inspection') }}</p>
                <p>{{ trans('register.instrument_n_automatic') }}</p>
                <p>{{ trans('register.laboratory') }}</p>
                <p>{{ trans('register.exploration_n_production') }}</p>
                <p>{{ trans('register.sms') }}</p>
                <p>{{ trans('register.maintenance') }}</p>
            </li>
            <li>
                {!! Form::checkbox('interests[5]', 1, false, ['class' => 'chk']) !!}
                <label for="lbl27">{{ trans('register.services_n_utilities') }}</label>
                <p>{{ trans('register.logistics_n_distribution') }}</p>
                <p>{{ trans('register.goods_n_services') }}</p>
                <p>{{ trans('register.sustainability') }}</p>
            </li>
            <li>
                {!! Form::checkbox('interests[6]', 1, false, ['class' => 'chk']) !!}
                <label for="lbl28">{{ trans('register.law_n_regulations') }}</label>
                <p>{{ trans('register.laws_n_regulations') }}</p>
                <p>{{ trans('register.taxation') }}</p>
                <p>{{ trans('register.normalization') }}</p>
            </li>
            </ul>
        {!! Form::submit(trans('register.sign_up'), ['class' => 'btn-submit']) !!}
    </div>

    {!! Form::close() !!}

@include('client.templatesinclude_ext')
@stop
