@extends('master')
@section('content')
@include('sub-header')

<div class="container">
    <header class="headline">
        <h2 class="marked">Novo</h2>
    </header><!-- end headline -->
    <section class="contacts-area">
        <div class="contacts-cols">
            <aside class="contacts-sidebar">
                <figure class="profile-name">
                    <div class="profile-photo photo-upload">
                        <a id="photo" class="img" href="#"><img src="/images/contato-profile.png" alt="image"></a>
                        <a href="#" class="btn-add">add</a>
                    </div><!-- end profile-photo -->
                </figure><!-- end profile-name -->
            </aside><!-- end contacts-sidebar -->
            <div class="contacts-edit">
                {!! Form::open() !!}
                    {!! Form::hidden('uploadedpath', null) !!}
                    {!! Form::hidden('cadastrointerno', 'true') !!}
                    {!! Form::hidden('groupsize', 'true', ['id' => 'groupsize']) !!}
                    <section class="section general-section">
                        <div class="section-container">
                            <div class="cols">
                                <div class="col">
                                    <h2>geral</h2>
                                </div><!-- end col -->
                                <div class="col">
                                    <div class="vip-box">
                                        <i class="ico-star gray-state">&nbsp;</i>
                                        <span>Contato VIP</span>
                                        {!! Form::hidden('vip', 'false') !!}
                                    </div><!-- end vip-box -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols">
                                <div class="col">
                                    <label for="name">{{ trans('register.first_name') }}<strong>*</strong></label>
                                    <div class="input-holder">{!! Form::text('name', null, ['id'=>'f-name', 'rel' => 'f-name', 'class' => 'form-control', 'data-rule-required' => 'true']) !!}</div>
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lastname">{{ trans('register.last_name') }}<strong>*</strong></label>
                                    <div class="input-holder">{!! Form::text('lastname', null, ['id'=>'l-name', 'rel' => 'l-name', 'class' => 'form-control' , 'data-rule-required' => 'true']) !!}</div>
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols titles-cols">
                                <div class="col">
                                    <label for="lbl-003">Email<strong>*</strong></label>
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-004">Tipo de email<strong>*</strong></label>
                                </div><!-- end col -->
                            </div><!-- end cols -->
                         <div class="outer-group" rel="email">
                            <div class="duplicate-group">
                                <a href="#" class="btn-addrow btn-addrow-fix">+</a>
                                <div class="cols">
                                    <div class="col">
                                        <div class="input-holder type2">
                                            {!! Form::email('emailaddr[0][]', null, ['id'=>'email', 'rel' => 'email', 'class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-email' => 'true']) !!}
                                        </div><!-- end input-holder -->
                                    </div><!-- end col -->
                                    <div class="col">
                                        <div class="input-holder">
                                            {!! Form::select('emailtype[0][]', [''=> 'Selecionar', 'pessoal' => $strings['personal'], 'comercial' => $strings['business']], ['id' => 'type', 'required' => 'required'], ['class'=> 'form-control', 'data-rule-required'=> 'true']) !!}
                                        </div><!-- end input-holder -->
                                    </div><!-- end col -->
                                </div><!-- end cols -->
                            </div><!-- end duplicate-group -->
                         </div>
                            <div class="cols titles-cols">
                                <div class="col">
                                    <label for="lbl-005">Telefone<strong>*</strong></label>
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-006">Tipo de telefone<strong>*</strong></label>
                                </div><!-- end col -->
                            </div><!-- end cols -->
                         <div class="outer-group" rel="phone">
                            <div class="duplicate-group">
                                <a href="#" class="btn-addrow btn-addrow-fix">+</a>
                                <div class="cols">
                                    <div class="col">
                                        <div class="phone-number">
                                            <div class="item width03">
                                                {!! Form::text('phoneint[0][]', null, ['class' => 'form-control phone-code', 'data-rule-number' => 'true', 'data-rule-required' => 'true', 'placeholder' => "55", 'maxlength' => '2']) !!}
                                            </div>
                                            <div class="item width04">
                                            {!! Form::text('phonenum[0][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true', 'placeholder' => "(21) 7575-7575"]) !!}
                                            </div>
                                        </div><!-- end phone-number -->
                                    </div><!-- end col -->
                                    <div class="col">
                                        {!! Form::select('phonetype[0][]', [''=> 'Selecionar', 'fixo' => 'Fixo', 'celular' => 'Celular'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
                                    </div><!-- end col -->
                                </div><!-- end cols -->
                            </div><!-- end duplicate-group -->
                        </div>
                            <div class="cols">
                                <div class="col">
                                    <label for="lbl-007">Data de nascimento<strong>*</strong></label>
                                    <div class="input-holder">
                                    <div class="item width01">
                                        {!! Form::text('birthday', null, ['id' => 'birthday', 'class' => 'form-control', 'maxlength' => '2', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                                    </div>
                                    <div class="item width02">
                                        {!! Form::select('birthmonth', [''=> 'Selecionar', '01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro'], null, ['id' => 'birthmonth', 'class' => 'select-required', 'data-rule-required' => 'true']) !!}
                                    </div>
                                    <div class="cols"></div>
                                    <label for="lbl-007">Documento<strong>*</strong></label>
                                    <div class="item width03">
                                        {!! Form::text('identification', null, ['class' => 'form-control', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                                    </div>
                                    <div class="item width04">
                                        {!! Form::select('identificationtype', [''=> 'Selecionar', 'rg' => 'RG', 'cpf' => 'CPF', 'cnh' => 'CNH', 'passaporte' => 'Passaporte', 'social-security' => 'Social Security', 'outro' => 'Outro'], null, ['id' => 'identificationtype', 'required' => 'required']) !!}
                                    </div>
                                    </div><!-- end input-holder -->
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-008">Redes Sociais</label>
                                    <div class="input-holder type4">
                                        <div class="item width02" style="width: 164px;">
                                            {!! Form::text('social[0][url]', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="item width04">
                                            {!! Form::select('social[0][type]', ['' => 'Selecionar', 'Facebook' => 'Facebook', 'LinkedIn' => 'LinkedIn'], null, []) !!}
                                        </div>
                                    </div><!-- end input-holder -->
                                    <div class="input-holder type4">
                                        <div class="item width02" style="width: 164px;">
                                            {!! Form::text('social[1][url]', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="item width04">
                                            {!! Form::select('social[1][type]', ['' => 'Selecionar', 'Facebook' => 'Facebook', 'LinkedIn' => 'LinkedIn'], null, []) !!}
                                        </div>
                                    </div><!-- end input-holder -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols">
                                <div class="col">
                                    <label>Língua de preferência</label>
                                    <ul class="radio-group">
                                        <li>
                                            <input id="lbl-009" name="language" value="pt" type="radio" class="radio" checked="checked">
                                            <label for="lbl-009">Português</label>
                                        </li>
                                        <li>
                                            <input id="lbl-010" name="language" value="en" type="radio" class="radio">
                                            <label for="lbl-010">English</label>
                                        </li>
                                    </ul><!-- end radio-group -->
                                </div><!-- end col -->
                                <div class="col">
                                    <label>Estrangeiro?</label>
                                    <ul class="radio-group">
                                        <li>
                                            <input id="lbl-011" name="foreigner" value="yes" type="radio" class="radio">
                                            <label for="lbl-011">Sim</label>
                                        </li>
                                        <li>
                                            <input id="lbl-012" name="foreigner" value="no" type="radio" class="radio" checked="checked">
                                            <label for="lbl-012">Não</label>
                                        </li>
                                    </ul><!-- end radio-group -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                        </div><!-- end section-container -->
                    </section><!-- end section -->
                    <section class="section address-section">
                        <div class="section-container">
                            <h2>endereços</h2>
                            <div class="outer-group" rel="address">
                                <div class="duplicate-group">
                                    <a href="#" class="btn-addrow btn-addrow-fix">+</a>
                                    <div class="cols">
                                            <div class="col">
                                                <label for="lbl-013">País<strong>*</strong></label>
                                                <div class="input-holder type3">
                                                    {!! Form::select('business[0][country]', App\Lib\Helpers::get_country_list(), null, ['class' => 'businesscountry', 'required' => 'required']) !!}
                                                </div><!-- end input-holder -->
                                            </div><!-- end col -->
                                            <div class="col">
                                                <label for="lbl-014">CEP<strong>*</strong></label>
                                                <div class="input-holder type5 correcao-cep">
                                                    {!! Form::text('business[0][zip]', null, ['class' => 'form-control zip disabled', 'data-rule-required' => 'true', 'data-rule-number' => 'true', 'readonly' => 'readonly']) !!}
                                                </div><!-- end input-holder -->
                                            </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols titles-cols address-cols home_address">
                                        <div class="cols"></div>
                                        <div class="col col-001">
                                            <label for="lbl-015">Endereço<strong>*</strong></label>
                                        </div><!-- end col -->
                                        <div class="col col-002">
                                            <label for="lbl-016">Número<strong>*</strong></label>
                                        </div><!-- end col -->
                                        <div class="col col-003">
                                            <label for="lbl-017">Complemento<strong>*</strong></label>
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols titles-cols address-cols intl_address">
                                        <div class="cols"></div>
                                        <div class="col col-001">
                                            <label for="lbl-015">Endereço<strong>*</strong></label>
                                        </div><!-- end col -->
                                        <div class="col col-003">
                                            <label for="lbl-017">Complemento<strong>*</strong></label>
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols address-cols home_address">
                                        <div class="col col-001">
                                            {!! Form::text('business[0][address][street]', null, ['class' => 'disabled rua form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                        <div class="col col-002">
                                            {!! Form::text('business[0][address][number]', null, ['class' => 'disabled numero form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                        <div class="col col-003">
                                            {!! Form::text('business[0][address][complement]', null, ['class' => 'disabled complemento form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols address-cols intl_address">
                                        <div class="col col-001">
                                            {!! Form::text('business[0][intl_address][street]', null, ['class' => 'disabled rua form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                        <div class="col col-003">
                                            {!! Form::text('business[0][intl_address][complement]', null, ['class' => 'complemento disabled form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="columns">
                                        <div class="column home_address">
                                            <label for="lbl-018">Bairro<strong>*</strong></label>
                                            {!! Form::text('business[0][address][neighborhood]', null, ['class' => 'disabled bairro form-control', 'data-rule-required' => 'true', 'readonly' => 'readonly']) !!}
                                        </div><!-- end column -->
                                        <div class="column">
                                            <label for="lbl-019">Cidade<strong>*</strong></label>
                                            {!! Form::select('business[0][city]', ['select'], null, ['class' => 'disabled businesscity', 'required' => 'required', 'disabled' => 'disabled']) !!}
                                        </div><!-- end column -->
                                        <div class="column">
                                            <label for="lbl-020">Estado<strong>*</strong></label>
                                            {!! Form::select('business[0][state]', ['select'], null, ['class' => 'disabled businessstate', 'required' => 'required', 'disabled' => 'disabled']) !!}
                                        </div><!-- end column -->
                                    </div><!-- end cols -->
                                </div><!-- end duplicate-group -->
                            </div>
                        </div><!-- end section-container -->
                    </section><!-- end section -->
                    <section class="section corporate-section">
                        <div class="section-container">
                            <h2>corporativo</h2>
                            <div class="outer-group" rel="company">
                                <div class="duplicate-group">
                                    <a href="#" class="btn-addrow adjustment-btn-addrow btn-addrow-fix">+</a>
                                    <div class="cols titles-cols">
                                        <div class="col">
                                            <label for="lbl-021">Empresa/Organização<strong>*</strong></label>
                                        </div><!-- end col -->
                                        <div class="col">
                                            <label for="lbl-022">Cargo<strong>*</strong></label>
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols address-cols">
                                        <div class="col">
                                            {!! Form::text('company[0][name]', null, ['class' => 'form-control', 'data-rule-required' => 'true']) !!}
                                        </div><!-- end col -->
                                        <div class="col">
                                            {!! Form::select('company[0][role]', App\Lib\Helpers::get_names_list('function'), null, ['id' => 'companyrole', 'data-rule-required' => 'true']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                    <div class="cols">
                                        <div class="col">
                                            <label for="lbl-023">Departamento<strong>*</strong></label>
                                            {!! Form::select('company[0][area]', App\Lib\Helpers::get_names_list('department'), null, ['id' => 'companyarea', 'data-rule-required' => 'true']) !!}
                                        </div><!-- end col -->
                                        <div class="col">
                                            <label for="lbl-024">Formação</label>
                                            {!! Form::select('company[0][career]', App\Lib\Helpers::get_names_list('carreer'), null, ['id' => 'companycareer']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                </div><!-- end duplicate-group -->
                            </div>
                            <div class="cols">
                                <div class="col">
                                    <label for="lbl-025">Tipo de sócio<strong>*</strong></label>
                                    <select id="lbl-025" name="associate_type" data-rule-required="true" required='required'>
                                        <option value="" checked="checked">Selecionar</option>
                                        <option value="nao-associado">Não Associado</option>
                                        <option value="coletivo">Coletivo</option>
                                        <option value="cooperador">Cooperador</option>
                                        <option value="emerito">Emérito</option>
                                        <option value="entidade">Entidade</option>
                                        <option value="individual">Individual</option>
                                        <option value="patrimonial">Patrimonial</option>
                                    </select>
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="origin">Origem<strong>*</strong></label>
                                    {!! Form::select('origin', $origins, null, ['data-rule-required' => 'true', 'required' => 'required']) !!}
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols">
                                <div class="col">
                                    <label for="lbl-027">Código do sócio</label>
                                    <div class="input-holder type6">
                                        {!! Form::text('associate_code', null, ['class' => 'form-control']) !!}
                                    </div><!-- end input-holder -->
                                </div><!-- end col -->
                            </div><!-- end cols -->
                            <div class="cols titles-cols">
                                <div class="col">
                                    <label for="lbl-005">Telefone<strong>*</strong></label>
                                </div><!-- end col -->
                                <div class="col">
                                    <label for="lbl-006">Tipo de telefone<strong>*</strong></label>
                                </div><!-- end col -->
                            </div><!-- end cols -->
                             <div class="outer-group" rel="corporatephone">
                                <div class="duplicate-group">
                                    <a href="#" class="btn-addrow">+</a>
                                    <div class="cols">
                                        <div class="col">
                                            <div class="phone-number">
                                                {!! Form::text('company[phoneint][0][]', null, ['class' => 'form-control phone-code', 'data-rule-required' => 'true', 'data-rule-number' => 'true']) !!}
                                                {!! Form::text('company[phonenum][0][]', null, ['class' => 'form-control phone01', 'data-rule-required' => 'true']) !!}
                                            </div><!-- end phone-number -->
                                        </div><!-- end col -->
                                        <div class="col">
                                            {!! Form::select('company[phonetype][0][]', [''=> 'Selecionar', 'fixo' => 'Fixo', 'celular' => 'Celular'], null, ['id' => 'phonetype', 'required' => 'required']) !!}
                                        </div><!-- end col -->
                                    </div><!-- end cols -->
                                </div><!-- end duplicate-group -->
                            </div>
                        </div><!-- end section-container -->
                    </section><!-- end section -->
                    <section class="section ibp-section">
                        <div class="section-container">
                            <h2>ibp</h2>
                            <ul class="ibp-list">
                                <li class="">
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Energy and Fuel"].']', 1, false, ['class' => 'chk']) !!}
                                        <label for="lbl-028">energia e combustíveis</label>
                                    </header>
                                    <ul>
                                        <li>Biocombustíveis</li>
                                        <li>Combustíveis</li>
                                        <li>Gás</li>
                                        <li>Economia e Política Energética</li>
                                    </ul>
                                </li>
                                <li>
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Industry"].']', 1, false, ['class' => 'chk']) !!}
                                        <label for="lbl-029">indústria</label>
                                    </header>
                                    <ul>
                                        <li>Exploração e Produção (E&amp;P)</li>
                                        <li>Pesquisa, desenvolvimento e Inovação (PD&amp;I)</li>
                                        <li>Petroquímica</li>
                                    </ul>
                                </li>
                                <li>
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Products and Derivatives"].']', 1, false, ['class' => 'chk']) !!}
                                        <label for="lbl-030">produtos e derivados</label>
                                    </header>
                                    <ul>
                                        <li>Asfalto</li>
                                        <li>Refino</li>
                                        <li>Lubrificantes e Lubrificação</li>
                                    </ul>
                                </li>
                                <li class="">
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Products and Equipment"].']', 1, false, ['class' => 'chk']) !!}
                                        <label for="lbl-031">produção e equipamentos</label>
                                    </header>
                                    <ul>
                                        <li>Equipamentos</li>
                                        <li>Inspeção de equipamentos</li>
                                        <li>Instrumentação e Automação</li>
                                        <li>Laboratório</li>
                                        <li>Exploração e produção</li>
                                        <li>SMS</li>
                                        <li>Manutenção</li>
                                    </ul>
                                </li>
                                <li class="">
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Services and Utilities"].']', 1, false, ['class' => 'chk']) !!}
                                        <label for="lbl-032">serviços e utilidades</label>
                                    </header>
                                    <ul>
                                        <li>Logística e Distribuição</li>
                                        <li>Bens e Serviços</li>
                                        <li>Sustentabilidade</li>
                                    </ul>
                                </li>
                                <li class="">
                                    <header>
                                        {!! Form::checkbox('interests['.$interest_groups["Laws and Regulations"].']', 1, false, ['class' => 'chk']) !!}
                                        <label for="lbl-033">leis e normas</label>
                                    </header>
                                    <ul>
                                        <li>Legislação e Regulamentação</li>
                                        <li>Tributação</li>
                                        <li>Normalização</li>
                                    </ul>
                                </li>
                            </ul><!-- end ibp-list -->
                            <footer class="btn-row" style="display:inline-block;float:right;">
                                {!! Form::submit('salvar', ['class' => 'btn']) !!}
                                <a href="#" class="btn btn-grey">Cancelar</a>
                            </footer><!-- end btn-row -->
                        </div><!-- end section-container -->
                    </section><!-- end section -->
                {!! Form::close() !!}
            </div><!-- end contacts-edit -->
        </div><!-- end contacts-cols -->
    </section><!-- end contacts-area -->
</div><!-- end container -->
@include('client.templatesinclude')
@stop
