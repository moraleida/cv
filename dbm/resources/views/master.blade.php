<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>DBM IBP</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400&subset=latin,latin-ext">
    <link rel="stylesheet" href="/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/admin/css/styles.css">
    <script src="/js/history.min.js"></script>
    <script src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/admin/js/messages_pt_BR.js"></script>
    <script src="/js/custom-form.js"></script>
    <script src="/js/custom-form.select.js"></script>
    <script src="/js/custom-form.radio.js"></script>
    <script src="/js/custom-form.file.js"></script>
    <script src="/js/custom-form.checkbox.js"></script>
    <script src="/js/jquery.tablesorter.js"></script>
    <script src="/js/jquery.tablesorter.pager.js"></script>
    <script src="/js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="/js/jquery.fancybox.pack.js"></script>
    <script src="/js/placeholders.min.js"></script>
    <script src="/js/circle-progress.js"></script>
    <script src="/js/jquery.scrolltabs.js"></script>
    <script src="/js/equal-height.js"></script>
    <script src="/js/Chart.min.js"></script>
    <script src="/admin/js/jquery.mask.min.js"></script>
    <script src="/admin/js/scripts.js"></script>
    <script src="/admin/js/forms.js"></script>
    @if(Route::is('client.add') || Route::is('client.edit'))
    <script src="/js/plupload/plupload.full.min.js"></script>
	<script src="/js/plupload/plupload-settings.js"></script>
    @endif
    @if(Route::is('campaign.*'))
    <script src="/admin/js/campaigns.js"></script>
    @endif
</head>
<body>
    <div class="wrapper">
        @include('sidebar')

        <div class="main">
            @include('header') 

            @yield('content')
        </div>
    </div>
</body>
</html>
