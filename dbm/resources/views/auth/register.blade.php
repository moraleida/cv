@extends('master')


@section('content')

    @if (count($errors) > 0)
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif
    <div class="heading">
        <div class="container">
            <h1><span class="marked">Usuários</span> - novo</h1>
        </div>
    </div>
    <div class="container">
        <div class="feedback-form">
            {!! Form::open(['data-remote']) !!}

                {!! Form::label('usuario', 'Nome de usuário: ') !!}
                {!! Form::text('usuario', null, ['class' => 'form-control']) !!}

                {!! Form::label('email', 'Endereço de email: ') !!}
                {!! Form::email('email', null, ['class' => 'form-control']) !!}

                {!! Form::label('nome', 'Nome: ') !!}
                {!! Form::text('nome', null, ['class' => 'form-control']) !!}

                {!! Form::label('sobrenome', 'Sobrenome: ') !!}
                {!! Form::text('sobrenome', null, ['class' => 'form-control']) !!}

                {!! Form::label('password', 'Senha: ') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}

                {!! Form::label('password_confirmation', 'Repetir Senha: ') !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}

                {!! Form::label('funcao', 'Função: ') !!}
                {!! Form::select('funcao', ['administrador' => 'Administrador', 'editor' => 'Editor', 'usuario' => 'Usuário'], ['class' => 'form-control']) !!}

                {!! Form::submit('Adicionar', ['class' => 'btn btn-submit']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@stop
