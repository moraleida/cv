@extends('gate')


@section('content')

    @if (count($errors) > 0)
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    @endif


    {!! Form::open() !!}
        
        <div class="input-group">
            {!! Form::label('email', 'Usuário: ', ['class' => 'input-group-addon ico-01']) !!}
            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Usuário', 'data-rule-required' => 'true']) !!}
        </div>

        <div class="input-group">
            {!! Form::label('password', 'Senha: ', ['class' => 'input-group-addon ico-02']) !!}
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha', 'data-rule-required' => 'true']) !!}
        </div>

        {!! Form::submit('Acessar', ['class' => 'btn btn-submit']) !!}

        <div class="add-links">
            <a class="esqueceu" href="#">Esqueceu a senha?</a>
        </div>

    {!! Form::close() !!}

@stop
