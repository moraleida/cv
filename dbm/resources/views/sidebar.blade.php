<?php $currentRoute = Route::currentRouteName(); ?>
<aside class="side-menu">
    <strong class="logo"><a href="{{ route('login') }}">ibp - instituto brasileiro de petroleo, gas e biocombustiveis</a></strong>
    <header class="head">
        <h3>dbm</h3>
    </header>
    <ul class="menu">
        <li class="{{ (str_is('*home*', $currentRoute) ? 'active' : '') }}"><a href="{{ route('home') }}" class="ico-01">INÍCIO</a></li>
        <li class="{{ (str_is('*list.*', $currentRoute) ? 'active' : '') }}"><a href="{{ route('list.list') }}" class="ico-02">LISTAS</a></li>
        <li class="{{ (str_is('*client.*', $currentRoute) ? 'active' : '') }}"><a href="{{ route('client.list') }}" class="ico-03">CONTATOS</a></li>
        <li class="{{ (str_is('*import*', $currentRoute) ? 'active' : '') }}"><a href="{{ route('import') }}" class="ico-04">IMPORTAR CONTATOS</a></li>
        <li class="{{ (str_is('*campaign.*', $currentRoute) ? 'active' : '') }}"><a href="{{ route('campaign.list') }}" class="ico-05">CAMPANHAS</a></li>
        <li class="{{ (str_is('*report.*', $currentRoute) ? 'active' : '') }}"><a href="{{ route('report.list') }}" class="ico-06">RELATÓRIOS</a></li>
    </ul>
    <ul class="user-menu">
        <li class="{{ (str_is('*user.*', $currentRoute) ? 'active' : '') }}"><a href="{{ route('user.list') }}" class="ico-01">USUÁRIOS</a></li>
        <li class="{{ (str_is('*support*', $currentRoute) ? 'active' : '') }}"><a href="{{ route('support') }}" class="ico-02">SUPORTE</a></li>
    </ul>
</aside><!-- /side-menu -->
