@extends('master')

@section('content')

    @include('sub-header')

<div class="container">
    <form action="#">
        <div class="contact-filter clearfix">
            <input class="form-control form-control2" type="text" placeholder="email específico" >
        </div>
        <div class="data zebra">
            <table>
                <thead>
                <tr>
                    <th>
                        <div class="chk-hold">
                            <input class="chk check-all" type="checkbox" >
                        </div>
                    </th>
                    <th>Nome completo</th>
                    <th>E-mail</th>
                    <th>Nome de usuário</th>
                    <th>Função</th>
                    <th class="sorter-false">
                        <div class="content-menu">
                            <a href="#" class="btn-toggle light">toggle menu</a>
                            <ul>
                                <li><a href="#">Editar</a></li>
                                <?php //                                <li><a href="#">Duplicar</a></li> ?>
                                <li class="sep"><a href="#">Excluir</a></li>
                            </ul>
                        </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($user as $u)
                    @if($u->role != 'inativo')
                        <tr>
                            <td><input class="chk" type="checkbox" ></td>
                            <td>{{ $u->name }} {{ $u->lastname }}</td>
                            <td>{{ $u->email }}</td>
                            <td>{{ $u->username }}</td>
                            <td>{{ $u->role }}</td>
                            <td>
                                <div class="content-menu">
                                    <a href="#" class="btn-toggle">toggle menu</a>
                                    <ul>
                                        <li><a href="{{ route('user.edit', $u->id) }}">Editar</a></li>
                                        <?php //                                        <li><a href="{{ route('user.duplicate', $u->id) }}">Duplicar</a></li> ?>
                                        <li class="sep"><a href="{{ route('user.delete', $u->id) }}">Excluir</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </form>
        <div class="pagination-wrapper">{!! $user->render() !!}</div>
</div><!-- /container -->
@stop
