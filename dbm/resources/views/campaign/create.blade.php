@extends('master')

@section('content')
    @include('sub-header')
<script type="text/javascript">
var loadedLists = [];
var loadedSegments = [];
var loadedEmails = [];
</script>
            <div class="container">
                <form action="#">
                    <div class="contact-filter">
                        <div class="row clearfix">
                            <label for="lbl-01">Nome da campanha</label>
                            {!! Form::text('name', null, ['id' => 'campaign', 'class' => 'form-control']) !!}
                        </div>
                        <div class="clearfix">
                            {!! Form::select('list', $lists, null, ['class' => 'cs2', 'id' => 'addList']) !!}
                            {!! Form::select('segment', $segments, null, ['class' => 'cs2', 'id' => 'addSegment']) !!}
                            <button class="form-control form-control2" placeholder="adicionar email" id="addEmail" type="button">adicionar emails</button>
                        </div>
                    </div>
                    <div class="choose-new hidden-block">
                        <header>
                            <h3>Você não selecionou um lista, segmento ou email específico.<br>Selecione abaixo um dos itens para criar um nova campanha:</h3>
                        </header>
                        <a href="#" class="btn btn-grey2 go-campaign">adicionar lista</a>
                        <a href="#" class="btn btn-grey2 go-campaign">adicionar segmento</a>
                        <a href="#" class="btn btn-grey2 go-campaign">email específico</a>
                    </div>
                    <div class="campaign-opts">
                        <ul class="tabset"></ul>
                        <div class="tab-holder"></div>
                        <a href="#" class="btn-continue">CONTINUAR NO</a>
                        <div class="preview">
                            <strong class="ttl">Prévia</strong>
                            <ul class="data-list"></ul>
                            <div class="search-form clearfix">
                                <input class="btn btn-small btn-grey btn-submit" type="submit" value="busca" >
                                <input class="form-control" type="text" placeholder="Nome ou Email" >
                            </div>
                            <div class="data zebra">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>e-mail</th>
                                            <th>Organização</th>
                                            <th>cargo</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <strong class="ttl">Prévia</strong>
                            <ul class="data-list"></ul>
                        </div>
                    </div>
                </form>
            </div><!-- /container -->
@stop
