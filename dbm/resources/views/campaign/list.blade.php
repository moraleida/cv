@extends('master')

@section('content')
@include('sub-header')

<div class="container">
    <form action="#">
        <div class="contact-filter">
            <div class="clearfix">
                <select class="cs2">
                    <option>PASTAS</option>
                    <option>PASTAS</option>
                    <option>PASTAS</option>
                </select>
                <select class="cs2 sel-02">
                    <option>filtrar por</option>
                    <option>filtrar por</option>
                </select>
                <select class="cs2 sel-02">
                    <option>mover para</option>
                    <option>mover para</option>
                </select>
            </div>
        </div>
        <ul class="data-listing">
            @foreach($campaigns as $campaign)
            <?php if($campaign->processed == '0000-00-00 00:00:00') {
                    $statusLine = 'Não enviada';
                    $status = 'Criada em '.date('d/m/Y H:i', strtotime($campaign->created_at));
                    $statusClass = '';
            } else {
                    $statusLine = 'Enviada';
                    $status = 'Enviada em '.date('d/m/Y H:i', strtotime($campaign->processed));
                    $statusClass = 'success';
            }
            ?>
            <li>
                <input class="chk chk2" type="checkbox" >
                <div class="base-inf">
                    <strong class="name">{{ $campaign->campaign }}</strong>
                    <span class="status {{ $statusClass }}">{{ $statusLine }}</span>
                </div>
                <span class="time">{{ $status }}</span>
                <div class="stat-data">
                    <strong class="value">0</strong>
                    <span class="unit">inscritos</span>
                </div>
                <div class="stat-data">
                    <strong class="value">0%</strong>
                    <span class="unit">clicados</span>
                </div>
                <div class="stat-data">
                    <strong class="value">0%</strong>
                    <span class="unit">abertos</span>
                </div>
                <div class="tools">
                    <a href="#" class="btn-graph">graph</a>
                    <div class="content-menu">
                        <a href="#" class="btn-toggle">toggle menu</a>
                        <ul>
                            <li><a href="{{ route('campaign.edit', $campaign->id) }}">Editar</a></li>
                            <li><a class="duplicate open-modal" href="#duplicate" rel="{{ route('campaign.duplicate', $campaign->id) }}">Duplicar</a></li>
                            <li class="sep"><a class="delete open-modal" href="#delete" rel="{{ route('campaign.delete', $campaign->id) }}">Excluir</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
    </form>
    <div id="pagination" class="pagination-wrapper">{!! $campaigns->render() !!}</div>
    @include('modal')
</div><!-- /container -->
@stop
