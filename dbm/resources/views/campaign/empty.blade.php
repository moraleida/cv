@extends('master')

@section('content')

<div class="create-list">
    <div class="tbl">
        <div class="tbl-cell">
            <header>
                <h1>Ops!</h1>
            </header>
            <p>Você ainda não criou uma campanha. 
                <br>As campanhas são e-mails enviados para os assinantes de uma lista. 
                Não criou uma lista ainda? Sem problemas. Clique abaixo em 
                "Criar campanha" para criar e enviar uma campanha para si mesmo.</p>
            <a href="{{ route('campaign.create') }}" class="btn">criar campanha</a>
        </div>
    </div>
</div><!-- /create-list -->

@stop
