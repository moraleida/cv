    <header id="header">
        <div class="container">
            <div class="welcome-box">
                <a href="#">
                    <?php //<img class="avatar" src="/admin/images/img-01.jpg" alt="image description"> ?>
                    <strong class="name">{{ $current_user->name .' '. $current_user->lastname }}</strong>
                </a>
                <ul>
                    <li><a href="{{ route('user.edit', $current_user->id) }}">editar perfil</a></li>
                    <li><a href="{{ route('logout') }}">sair</a></li>
                </ul>
            </div>
        </div>
    </header><!-- /header -->
