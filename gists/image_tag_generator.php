<?php 
/*
Plugin Name: Image Tag Generator
Plugin URI: 
Description: Image Handler for HTML embedding. This was conceived as an all-in-the-house alternative to managing images resizes and crops.
 It's meant to substitute TimThumb as a versatile image handler for WordPress.
Author: Ricardo Moraleida
Version: 1.0
Author URI: 
*/
/*
* Use this function where you want images to be
*/

add_action('init', '_imager_check_refresh' );
function _imager_check_refresh() {
	if( isset($_GET['_imager']) && current_user_can('edit_posts') ) {
		error_log('in');
		define('REFRESH', true);
		if($_GET['_imager'] == 'all') {
			define('REFRESH_ALL', true);
		} else {
			$refresh_ref = $_GET['_imager'];
		}
			
	} else {
		//error_log('out');
	}
}

function _imager( $fileref, $attr = '') {
	
	global $post, $refresh_ref;	

	$default = array(
		'is_meta_key' => true,
		'is_option' => false,
		'return' => false,
		'format' => 'tag',
		'postid' => $post->ID,
		'alt'	 => 'image',
		'fallback' => false,
		'class'	=> '',
		'ignore' => false,
		'caption' => false,
		'size' => array(),
        'itemprop' => false
		);
	
	$args = wp_parse_args( $attr, $default );
	extract($args);

	$post = get_post( $postid );

	try {
		$img = new Imagito( $post, $fileref, $is_meta_key, $args );
        
        if( !$return ) {
		    echo apply_filters( '_imager_output', $img->getFormat( $format ) );
		    return;
    	} else {
	    	return apply_filters( '_imager_output', $img->getFormat( $format ) );
    	}
	} catch (Exception $e) {
		error_log( $fileref . ' >>> ' . $e->getMessage() .' >>> ' . $e->getFile() . '('.$e->getLine().')' );
	}	

}

function _imager_load_from_cdn( $props, $cdn = false ) {
	if(!$cdn)
		$cdn = TYT_CDN_URL;

	$props['path'] = false;
	$props['path2x'] = false;
	$props['url'] = str_replace(home_url(), $cdn, $props['url']);
	$props['url2x'] = str_replace(home_url(), $cdn, $props['url2x']);

	return $props;
}

class Imagito {

	private $post;
	private $filename;
	private $positions;
	private $width;
	private $height;
	private $image = array();
	private $attr = array();
	private $allowed_extensions = array();

	public function __construct($post, $fileref = false, $is_meta_key = false, $args = false) {

		// Initialize
		$this->post = $post;
		if(is_array($args)) {
			foreach($args as $key => $value)
				$this->attr[$key] = $value;
		}

		// TODO: criar lista de mime types aceitos
		// $this->allowed_mime_types = get_option( 'Imagito_mime_types' );
		$mime_types = array( 'jpg', 'jpeg', 'jpe', 'gif', 'png', 'svg', 'webp', 'tif' );
		$this->allowed_mime_types = apply_filters( 'Imagito_allowed_mime_types', $mime_types );

		if( !$fileref && !has_post_thumbnail( $this->post->ID ) )
			throw new Exception( __( 'No file was referenced', 'imagito' ) );		

		// TODO: criar o dicionário, interfaces, buscar com get_option(), etc.
		$this->positions = get_option('Imagito_positions');
		

		// If it's a meta-key, grab the corresponding value
		if( !is_numeric( $fileref ) ) {
			$this->image['key'] = $fileref;
			if( $this->attr['is_option'] == true ) {
				$ref = 'option';
			} else {
				$ref = 'meta';
			}
			$this->validateKey($fileref, $ref);
		} else {
			$this->post = get_post($fileref);
			$this->validateStandaloneImage($fileref);
		}

		$this->generateImageInfo();

		// Force update images
        if( current_user_can( 'activate_plugins' )// admins only
        	&& defined('REFRESH') 
        	&& $this->image['ext'] != 'svg' ) {
        		if( !defined( 'REFRESH_ALL' && $refresh_ref == $fileref ) ) {
        			$this->resizeImage();
        			error_log('refresh single image '.$fileref);
        		} else {
        			$this->resizeImage();
        			error_log('refresh all images');
        		}
        		
        	
        }

		// If this isn't an SVG and we still don't have the right file
		// generate it			
		if( !$this->imageExistsInSize() 
			&& $this->image['ext'] != 'svg' )
			$this->resizeImage();

		// TODO: non-metakey cases

	}

	public function getFormat( $format ) {

		 $this->image = apply_filters( '_imager_image_properties', $this->image );
		
	    if($this->attr['serve'] == 'retina')
        	$this->image['url'] = $this->image['url2x'];

		if($format == 'all')
			return $this->image;

		if($format == 'url')
			return $this->image['url'];

		if($format == 'path')
			return $this->image['path'];

/*		if($format == 'tag')
			return '<img src="'.$this->image['url'].'" alt="'.$this->attr['alt'].'" class="'.$this->attr['class'].'" width="'.$this->width.'" height="'.$this->height.'" />';
*/
         if($format == 'object') {
            if($this->attr['ignore'] != false) 
                $this->{$this->attr['ignore']} = '';

            $this->width = '100%';
            $this->height = 'auto';
            
//            return '<object type="image/svg+xml" data="'.$this->image['url'].'" alt="'.$this->attr['alt'].'" class="'.$this->attr['class'].'" width="'.$this->width.'" height="'.$this->height.'" ></object>';  
           
           return '<object><embed src="'.$this->image['url'].'" width="'.$this->width.'" height="'.$this->height.'" /></object>';
        }
        if($format == 'tag') {
            if($this->attr['ignore'] != false) 
                $this->{$this->attr['ignore']} = '';    

            if($this->attr['caption'] == 'span')
            	$caption = '<span class="imgname">'.$this->caption.'</span>';

            if($this->attr['caption'] == 'title')
            	$title = 'title="'.$this->caption.'"';

            if($this->attr['id'] != false)
                $id = 'id="'.$this->attr['id'].'"';

            if($this->attr['itemprop'] != false)
                $itemprop = 'itemprop="thumbnailUrl"';

            if($this->attr['data'] == 'data-no-retina')
                $this->image['data'] = 'data-no-retina';

            if($this->attr['method'] == 'lazy')
            	return '<img '.$itemprop.' '.$title.' '.$id.' data-lazy="'.$this->image['url'].'" src="#" alt="'.$this->attr['alt'].'" class="'.$this->attr['class'].'" width="'.$this->width.'" height="'.$this->height.'" '.$this->image['data'].' />'.$caption;
            
            return '<img '.$itemprop.' '.$title.' '.$id.' src="'.$this->image['url'].'" alt="'.$this->attr['alt'].'" class="'.$this->attr['class'].'" width="'.$this->width.'" height="'.$this->height.'" '.$this->image['data'].' />'.$caption;
        }
		return;
	}

	/**
	* Validation block
	*/ 
	private function validateKey($key, $ref = 'meta') {
		if( 'option' == $ref ) {
			$value = apply_filters( '_imager_option_value', get_option( $key ) );
		} else {
			$value = apply_filters( '_imager_meta-key_value', get_post_meta( $this->post->ID, $key, true) );
		}
		
		
		if(!array_key_exists($key, $this->positions) || !$value ) {

			if (!empty($this->attr['fallback'])) {
				$this->image['key'] = $this->attr['fallback'];
				$this->validateKey($this->image['key']);
				return;
			} else {
				throw new Exception( __( 'Meta key does not exist', 'imagito' ) );	
			}			

		} 
			
		if(empty($this->positions[$key]['width']) && empty($this->positions[$key]['height']))
			throw new Exception( __( 'Meta key sizes are not set', 'imagito' ) );

		if($ref == 'option') {
			$value = get_option( $key );
			error_log( "=== option value ===" );
			error_log( $key );
			if( is_numeric( $value ) ) {
				$this->filename = get_attached_file( get_post_meta( $this->post->ID, $key, true) );	
			} else {
				$this->filename = str_replace( home_url('/'), ABSPATH, $value );
			}
			
		} elseif($ref == 'meta') {
			$obj = get_post( get_post_meta( $this->post->ID, $key, true) );
			$this->caption = $obj->post_excerpt;
			$this->filename = get_attached_file( $obj->ID );
		}

        // Regular sizes
		// TODO: alterar dicionário para guardar os tamanhos não-retina

		$this->width = ceil($this->positions[$key]['width'] / 2);
		$this->height = ceil($this->positions[$key]['height'] / 2);

		if(!empty($this->attr['size'])) {
			$this->width = $this->attr['size'][0];
			$this->height = $this->attr['size'][1];
		}

		$this->image['filter'] = $this->positions[$key]['filter'];
		$this->image['compression'] = $this->positions[$key]['compression'];
		$this->image['quality'] = $this->positions[$key]['quality'];

        if( empty( $this->filename ) ) {
			throw new Exception( __( 'Meta key has no value associated', 'imagito' ) );
        }
            
	}

	private function validateStandaloneImage($key) {
			
		if( !isset($this->attr['size']) || empty($this->attr['size']) )
			throw new Exception( __( 'Standalone image sizes are not set', 'imagito' ) );

		$this->filename = get_attached_file($key);
        if( empty( $this->filename ) ) {
			throw new Exception( __( 'ID has no file associated', 'imagito' ) );
        }

		$this->width = $this->attr['size'][0];
		$this->height = $this->attr['size'][1];

		$this->image['filter'] = 'lanczos';
		$this->image['compression'] = 'lzw';
		$this->image['quality'] = '81';
		$this->image['key'] = '_imager_file';
		// $this->image['data'] = 'data-no-retina';

		if( is_array( $this->attr['settings'] ) ) {
			foreach($this->attr['settings'] as $key => $set) {
				if( in_array( $key, array( 'filter', 'compression', 'quality' ) ) )
					$this->image[$key] = $set;
			}
		}

		error_log(var_export($image, true));
            
	}	

	private function generateImageInfo() {
		$metaref = '';
		$this->image['data'] = '';
		if($this->image['key'])
			$metaref = $this->image['key'] .'_' . $this->post->ID . '_';

		$imgparts = explode( '/', $this->filename );
		$namext = explode( '.',array_pop($imgparts) );
		$imgbase = str_replace( '@2x', '', $namext[0] );

		// Original File Extension
		$this->image['ext'] = $namext[count($namext)-1];
        // Working File Extension
        $filext = $this->image['ext'];

		if( !in_array( $this->image['ext'], $this->allowed_mime_types ) )
            throw new Exception( $this->image['ext'] . __( 'is not a valid file extension', 'imagito' ) );

        if( $this->attr['fit'] == 'noresize' ) {
			
			$imgpath = $this->filename;
			$this->image['path'] = $imgpath;
			$this->image['url'] = str_replace( ABSPATH, home_url('/'), $imgpath);			

        } elseif($this->image['ext'] == 'svg') {
			
			$imgpath = $this->filename;
			$svg = new iSVGs($imgpath);

			$this->image['path'] = $imgpath;
			$this->image['url'] = str_replace( ABSPATH, home_url('/'), $imgpath);

			// Fix SVG sizes
			//$this->width = ceil($this->width * 2);
			//$this->height = ceil($this->height * 2);

			$this->image['data'] = 'data-no-retina'; // avoid trying to find retina svg images
			if(empty($this->attr['size'])) { // don't override sizes
				$this->width = ceil($svg->getWidth());
				$this->height = ceil($svg->getHeight());
			}

			if($this->attr['svg_strip_size']) {
				$svginfo = $svg->generateStrippedFile();
				$this->image['path'] = $svginfo['path'];
				$this->image['url'] = $svginfo['url'];
			}			
		
		} else {
			// If original file is an image but is not displayable, then use it's png copy
			// This preserves the global information about the file, but makes it easy to 
			// render the correct copies for both retina and non-retina displays, making
			// sure the @2x suffix is added exactly before the file extension, as per 
			// Apple guidelines
			if($filext == 'tif')
				$filext = 'png';

			// Fix proportional resizes.
			// If the image is marked as proportional, check which side is larger and 
			// just zero the other proportion, so that the resize and img tags are built
			// correctly
			if($this->positions[$this->image['key']]['fit'] == 'proportional') {
				if($this->width > $this->height) {
					$this->height = false;
				} else {
					$this->width = false;
				}
			}

			// Regular
			$imgname = $metaref . $imgbase.'_w'.$this->width.'h'.$this->height.'px';
			$imgpath = implode('/', $imgparts) . '/' . $imgname . '.' . $filext;
			$this->image['path'] = $imgpath;
			$this->image['url'] = str_replace( ABSPATH, home_url('/'), $imgpath);

			// Retina
			$imgname2x = $imgname.'@2x';
			$imgpath2x = implode('/', $imgparts) . '/' . $imgname2x . '.' . $filext;
			$this->image['path2x'] = $imgpath2x;
			$this->image['url2x'] = str_replace( ABSPATH, home_url('/'), $imgpath2x);

		}

	}

	private function imageExistsInSize() {

		if( !file_exists( $this->image['path'] ) )
			return false;

		return true;
	}

	/**
	* Actual resizing
	*/
	private function resizeImage($retina = false) {

		$image = new Imagick();
		$image->setBackgroundColor(new ImagickPixel('transparent'));
		$image->readImage($this->filename);
	//	$image->setImageFormat('png24');

		$filters = array( 
			'undefined' => Imagick::FILTER_UNDEFINED,
			'point' => Imagick::FILTER_POINT,
			'box' => Imagick::FILTER_BOX,
			'triangle' => Imagick::FILTER_TRIANGLE,
			'hermite' => Imagick::FILTER_HERMITE,
			'hanning' => Imagick::FILTER_HANNING,
			'hamming' => Imagick::FILTER_HAMMING,
			'blackman' => Imagick::FILTER_BLACKMAN,
			'gaussian' => Imagick::FILTER_GAUSSIAN,
			'quadratic' => Imagick::FILTER_QUADRATIC,
			'cubic' => Imagick::FILTER_CUBIC,
			'catrom' => Imagick::FILTER_CATROM,
			'mitchell' => Imagick::FILTER_MITCHELL,
			'lanczos' => Imagick::FILTER_LANCZOS,
			'bessel' => Imagick::FILTER_BESSEL,
			'sinc' => Imagick::FILTER_SINC
		);
		
		$compressions = array(
			'undefined' => Imagick::COMPRESSION_UNDEFINED,
			'no' => Imagick::COMPRESSION_NO,
			'bzip' => Imagick::COMPRESSION_BZIP,
			'fax' => Imagick::COMPRESSION_FAX,
			'group4' => Imagick::COMPRESSION_GROUP4,
			'jpeg' => Imagick::COMPRESSION_JPEG,
			'jpeg2000' => Imagick::COMPRESSION_JPEG2000,
			'losslessjpeg' => Imagick::COMPRESSION_LOSSLESSJPEG,
			'lzw' => Imagick::COMPRESSION_LZW,
			'rle' => Imagick::COMPRESSION_RLE,
			'zip' => Imagick::COMPRESSION_ZIP,
			'dxt1' => Imagick::COMPRESSION_DXT1,
			'dxt3' => Imagick::COMPRESSION_DXT3,
			'dxt5' => Imagick::COMPRESSION_DXT5
		);

		$resizefilter = $filters[$this->image['filter']];
		$compressionalgorithm = $compressions[$this->image['compression']];
		$compressionquality = $this->image['quality'];

		if($this->image['ext'] == 'png') {
			// PNG Images have a maximum quality of 9
			$compressionquality = ceil( ( $compressionquality / 10 ) * 0.9 );
		}

		if(!$retina) {
			$w = $this->width;
			$h = $this->height;
			$path = $this->image['path'];
		} else {
			$w = $this->width * 2;
			$h = $this->height * 2;
			$path = $this->image['path2x'];
		}

		error_log('===== NEW IMAGE '.$this->filename.' ====');
		error_log('Ext: '.$this->image['ext'].' W: '.$w.' H: '.$h.' Filter: '.$resizefilter.' Compression: '.$compressionalgorithm.' Quality: '.$compressionquality );

		$image->resizeImage( $w, $h, $resizefilter, 1 );
		$image->setImageCompression( $compressionalgorithm );
		$image->setImageCompressionQuality( $compressionquality );
		$image->stripImage();
		$image->writeImage($path);

		$image->clear();

		if(!$retina)
			$this->resizeImage(true);

		return;
	}

}

add_action( 'admin_menu', '_imager_dictionary_menu' );
function _imager_dictionary_menu() {
	add_options_page( 'Imagito Dictionary', 'Imagito Dictionary', 'activate_plugins', 'imagito-dic', '_imager_dictionary_page' );
}

function _imager_build_select($field,$value = false) {

	$fields = array(
		'filter' => array( 'undefined', 'point', 'box', 'triangle', 'hermite', 'hanning', 'hamming', 'blackman',
		'gaussian', 'quadratic', 'cubic', 'catrom', 'mitchell', 'lanczos', 'bessel', 'sinc'	),

		'compression' => array(
		'undefined', 'no', 'bzip', 'fax', 'group4', 'jpeg', 'jpeg2000', 'losslessjpeg', 'lzw', 'rle', 'zip',
		'dxt1', 'dxt3', 'dxt5' ),

		'fit' => array( 'fixed', 'proportional' )
	);

	if(!$value) {
		if($field == 'filter')
			$value = 'lanczos';
		if($field == 'compression')
			$value = 'lzw';
		if($field == 'fit')
			$value = 'fixed';
		if($field == 'quality')
			$value = '100';
	}

	$fselect .= '<select name="'.$field.'">';

	if($field == 'quality') {
		
		for($f=1; $f<101; $f++) {
			$selected = '';
			if($f == $value)
				$selected = 'selected="selected"';
			$fselect .= '<option value="'.$f.'" '.$selected.'>'.$f.'</option>'; 
		}
		return $fselect;
	}

	
	foreach($fields[$field] as $f) { 
		$selected = '';
		if($f == $value)
			$selected = 'selected="selected"';
		$fselect .= '<option value="'.$f.'" '.$selected.'>'.$f.'</option>'; 
	}
	
	$fselect .= '</select>';

	return $fselect;
}

function _imager_dictionary_page() {
	
	$positions = get_option( 'Imagito_positions' );
	ksort($positions);
	
	$cols = array('position', 'width', 'height', 'compression', 'filter', 'quality', 'fit');

	$filters = array( 'undefined', 'point', 'box', 'triangle', 'hermite', 'hanning', 'hamming', 'blackman',
		'gaussian', 'quadratic', 'cubic', 'catrom', 'mitchell', 'lanczos', 'bessel', 'sinc'	);
	
	$compressions = array(
		'undefined', 'no', 'bzip', 'fax', 'group4', 'jpeg', 'jpeg2000', 'losslessjpeg', 'lzw', 'rle', 'zip',
		'dxt1', 'dxt3', 'dxt5' );

	$fits = array( 'fixed', 'proportional' );

	echo '<h2>Imager Dictionary</h2>';
	echo '<p>Available values, please fill in with any of these values:</p>';
	echo '<ul>
			<li><strong>Width</strong>: Number</li>
			<li><strong>Height</strong>: Number</li>
			<li><strong>Compression</strong>: undefined, no, bzip, fax, group4, jpeg, jpeg2000, losslessjpeg, lzw, rle, zip,
			dxt1, dxt3, dxt5 </li>
			<li><strong>Filter</strong>: undefined, point, box, triangle, hermite, hanning, hamming, blackman,
			gaussian, quadratic</li>
			<li><strong>Quality</strong>: 0 to 100</li>
			<li><strong>Fit</strong>: fixed, proportional</li></ul>';
		
	echo '<table>';
	echo '<tr>
			<td>'.__('Position (meta_key)').'</td>
			<td>'.__('Width').'</td>
			<td>'.__('Height').'</td>
			<td>'.__('Compression').'</td>
			<td>'.__('Filter').'</td>
			<td>'.__('Quality').'</td>
			<td>'.__('Fit').'</td>
			</tr>';

	foreach($positions as $key => $value) {
		echo '<tr>';
		echo '<td class="position">'.$key.'</td>';
		$i=1;
		foreach($value as $col) {
			if($cols[$i] == 'filter' || $cols[$i] == 'compression' || $cols[$i] == 'fit' || $cols[$i] == 'quality') {
				echo '<td class="'.$cols[$i].'">'._imager_build_select( $cols[$i], $col ).'</td>';	
			} else {
				echo '<td class="'.$cols[$i].'"><input type="text" value="'.$col.'" name="'.$key.'['.$i.']" size="10" /></td>';	
			}
			
			$i++;
		}
		echo '<td class="save" style="display: none;"><input class="button button-primary" type="button" value="'.__('Save', 'imagito').'" size="10" /></td>';
		echo '<td class="save remove" style="display: none;"><input class="button button-secondary" type="button" value="'.__('Remove', 'imagito').'" size="10" /></td>';
		echo '<td class="resp"></td>';
		echo '</tr>';
	}

		echo '<tr>';
		echo '<td class="position input"><input class="key" type="text" value="" name="'.$key.'" size="10" /></td>';
		$i=1;
		foreach($value as $col) {
			if($cols[$i] == 'filter' || $cols[$i] == 'compression' || $cols[$i] == 'fit' || $cols[$i] == 'quality') {
				echo '<td class="'.$cols[$i].'">'._imager_build_select( $cols[$i] ).'</td>';	
			} else {
				echo '<td class="'.$cols[$i].'"><input type="text" value="" name="'.$key.'['.$i.']" size="10" /></td>';
			}
			
			$i++;
		}
		echo '<td class="save" style="display: none;"><input class="button button-primary" type="button" value="'.__('Save', 'imagito').'" size="10" /></td>';
		echo '<td class="resp"></td>';
		echo '</tr>';

	echo '</table>';
?>
<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('tr').hover(
		function() {
		jQuery(this).find('.save').show();
	}, function() {
		jQuery(this).find('.save').hide();
	});

	jQuery('.save').find('.button').on('click', function() {
		var $values = [];
		var $this = jQuery(this);
		var $position = $this.parents('tr').find('.position');
		var $key = $position.text();
		var $action = 'save';

		if($this.parents('.save').hasClass('remove'))
			$action = 'remove';

		if($position.hasClass('input'))
			$key = jQuery(this).parents('tr').find('.position > input').val();

		jQuery.each(jQuery(this).parents('tr').find('td > input, td > select').not('.key'), function() {
			$values.push(jQuery(this).val());
		})

		jQuery.post(ajaxurl, 
		{			
			action: '_imager_dictionary_save',
			key: $key,
			values: $values, 
			act: $action
		},
		function(data, textStatus, xhr) {
			$this.parents('tr').find('td.resp').html(data.msg);

			if(data.msg == 'New key added to dictionary' || data.status == 'removed')
				window.location.reload();
		}
		);
	});

});
</script>
<?php
}

add_action( 'wp_ajax__imager_dictionary_save', '_imager_dictionary_save' );

function _imager_dictionary_save() {

	$key = esc_attr( $_POST['key'] );
	$values = $_POST['values'];
	$dic = get_option( 'Imagito_positions' );
	$response = array( 'status' => 'success', 'msg' => '' );

		$filters = array( 'undefined', 'point', 'box', 'triangle', 'hermite', 'hanning', 'hamming', 'blackman',
			'gaussian', 'quadratic', 'cubic', 'catrom', 'mitchell', 'lanczos', 'bessel', 'sinc'	);
		
		$compressions = array(
			'undefined', 'no', 'bzip', 'fax', 'group4', 'jpeg', 'jpeg2000', 'losslessjpeg', 'lzw', 'rle', 'zip',
			'dxt1', 'dxt3', 'dxt5' );

		$fits = array( 'fixed', 'proportional' );

	// Data integrity

	if($_POST['act'] == 'remove') {
		unset($dic[$key]);

		update_option( 'Imagito_positions', $dic );
		$response = array( 'status' => 'removed', 'msg' => 'Key removed' );

	} elseif( !is_numeric( $values[0] ) 
		|| !is_numeric( $values[1] )
		|| !is_numeric( $values[4] )
		|| !in_array( $values[2], $compressions )
		|| !in_array( $values[3], $filters )
		|| !in_array( $values[5], $fits )
		) {
	    
	    $response = array( 'status' => 'error', 'msg' => 'Untrusted data' );
	} else {

		$msg = 'Dictionary updated';
		if( ! array_key_exists($key, $dic) )
			$msg = 'New key added to dictionary';

		$dic[$key]['width'] = $values[0];
		$dic[$key]['height'] = $values[1];
		$dic[$key]['compression'] = $values[2];
		$dic[$key]['filter'] = $values[3];
		$dic[$key]['quality'] = $values[4];
		$dic[$key]['fit'] = $values[5];

		if( update_option( 'Imagito_positions', $dic ) ) {
			$response = array( 'status' => 'success', 'msg' => $msg );
		} else {
			$response = array( 'status' => 'success', 'msg' => 'Values were not updated' );
		}
			
	}

    header( "Content-Type: application/json" );
    echo json_encode($response);
	exit();
}

?>
