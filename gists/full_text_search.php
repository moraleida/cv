<?php
/*
Plugin Name: Client Search
Plugin URI: 
Description: Full-text-search scanner and indexer. 
    This plugin reads the HTML output of the site pages to build a fulltext 
    database relating every text-bit to it's parent page so that content 
    stored in several different Post-Types and rendered in a single page can
    be searched-for and lead to the final URL where it is used.
Author: Ricardo Moraleida
Version: 1.0
Author URI: 
*/

/**
 * Sample Database structure to use with this plugin (creating the 
 * DB is not handled at this point)
 */
/*
CREATE TABLE clt_index (
    id INT(24) NOT NULL AUTO_INCREMENT,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified TIMESTAMP NOT NULL,
    priority INT(24) NOT NULL,
    url VARCHAR(255) NOT NULL,
    hash VARCHAR(255) NOT NULL,
    parent VARCHAR(255),
    filter VARCHAR(255) NOT NULL,
    section VARCHAR(255) NOT NULL,  
    title TEXT NOT NULL,
    text LONGTEXT NOT NULL,
    PRIMARY KEY (id)
);
*/


/**
 * clt_fulltext_search_goto
 * Little snippet to scroll the search-result page to the searched text
 *
 * @access public
 * @return void
 */
add_action( 'wp_head', 'clt_fulltext_search_goto' );
function clt_fulltext_search_goto() {

    if( !is_search() && !is_page( 'busca' ) && isset( $_GET['busca'] ) && !clt_is_mobile() ) {
        echo '<script type="text/javascript">jQuery(window).load(function() { jQuery.scrollTo("#'.esc_attr( $_GET['busca'] ).'", 850, {offset: -150, axis:"y"}); return false; });  </script>';
    }
}

/**
 * clt_search_for
 * Template function to perform the serach based on the get values from the URL
 * This implementation bypasses the original WP search entirely
 *
 * @param mixed $term
 * @param mixed $page
 * @return array of search results
 */
function clt_search_for($term, $page) {
    $termo = stripslashes( str_replace('"', '', $term ) );
    if( strlen( $termo ) > 2 ) {
        if( isset($page) && $page > 1 ) {
            $from = ( $page - 1 ) * 10;
            $to = $from + 10;
        } else {
            $from = 0;
            $to = 10;
        }
        $search = clt_fulltext_search($term, 0, 100);  
        $calc = $search['calc'][0]->{'FOUND_ROWS()'};
    } else {
        $search = 'Por favor, use no mínimo 3 caracteres para busca';
    }

    return $search;
}

/**
 * clt_fulltext_search
 * The actual search action. Treats the input and searches the Database.
 *
 * @param mixed $term
 * @param int $from
 * @param int $to
 * @return array
 */
function clt_fulltext_search($term, $from = 0, $to = 10) {
    global $wpdb;
    
    $term = stripslashes( trim( $term ) );

    // If search query is quoted, treat it as a phrase
    // else, prepare to break it in separate WHERE clauses
    if( substr($term, 0, 1) == '"' && substr($term, -1) == '"' ) {
        $terms = array( str_replace('"', '', $term ) );
    } else {
        $terms = explode(' ', $term);
    }

    foreach($terms as $t) {
        // If search term starts with a negative sign -, treat it as an exclusion
        // Return all matching entries except for those
        if( substr($t, 0, 1) == '-' ) {
            $t = substr($t, 1);
            $where[] = "( ( {$wpdb->prefix}index.title NOT LIKE \"%$t%\" ) AND ( {$wpdb->prefix}index.text NOT LIKE \"%$t%\" ) AND ( {$wpdb->prefix}index.url NOT LIKE \"%$t%\" ) )";
        } else {
            $where[] = "( ( {$wpdb->prefix}index.title LIKE \"%$t%\" ) OR ( {$wpdb->prefix}index.text LIKE \"%$t%\" ) OR ( {$wpdb->prefix}index.url LIKE \"%$t%\" ) )"; 
        }
        
    }
    $clause = join(' AND ', $where);
    $results['set'] = $wpdb->get_results( "SELECT SQL_CALC_FOUND_ROWS * FROM {$wpdb->prefix}index WHERE $clause ORDER BY priority, date(modified), title ASC" ); // LIMIT $from, $to
    $results['calc'] = $wpdb->get_results( "SELECT FOUND_ROWS()" );

    return $results;
}


/**
 * clt_fulltext_search_context
 * Assesses the context on every page load and fires the build functions if it's time
 *
 * @access public
 * @return void
 */
add_action( 'wp', 'clt_fulltext_search_context' );
function clt_fulltext_search_context() {

    global $post;

    if( clt_is_mobile() ) // only index desktop pages
        return;

    if( !is_search() && !is_page( 'busca' ) ) {

        if ( is_post_type_archive( 'faq' ) )
            $index = 'faq';
        if ( is_post_type_archive( 'glossary' ) )
            $index = 'glossary';
        if ( is_post_type_archive( 'dealership' ) ) 
            $index = 'dealership';
        if( is_singular() )
            $index = $post->ID;

        if( !$index )
            return;

        $lastIndexed = get_transient( 'clt_fulltext_search_last_index_'.$index );
        
        if( $lastIndexed != false )
            return;

    }

        if( 'dealership' == $index && $lastIndexed === false ) {
            $cs = get_posts( array( 'post_type' => $index, 'posts_per_page' => -1 ) );
            foreach($cs as $c) {
                clt_fulltext_search_build_dealership_index( $c->ID );
            }
        } elseif ( 'faq' == $index && !$lastIndexed ) {
            $cs = get_posts( array( 'post_type' => $index, 'posts_per_page' => -1 ) );
            foreach($cs as $c) {
                clt_fulltext_search_build_faq_index( $c->ID );
            }
        } elseif (  $lastIndexed === false  ) {
            add_action( 'template_redirect', function(){
                ob_start();
            }, 999 );

            add_action( 'wp_footer', function() {
                $src = ob_get_contents();
                ob_end_flush();
                clt_fulltext_search_build_index($src);
            }, 9999 );
        }

        set_transient( 'clt_fulltext_search_last_index_'.$index, time(), 60*60*24 ); // Update daily

}

/**
 * clt_fulltext_search_build_dealership_index
 * build the index for 'dealership' type pages
 *
 */
function clt_fulltext_search_build_dealership_index( $id ) {
    wp_reset_query();
    $dealer = get_post($id);

    $index_entry = array();
    $index_entry['time'] = time();
    $index_entry['url'] = '/concessionarias/';
    $index_entry['filter'] = 'concessionarias';
    $index_entry['section'] = 'concessionarias';
    $index_entry['hash'] = false;
    $index_entry['parent'] = $id;
    $index_entry['priority'] = 50;
    $index_entry['title'] = get_post_meta( $id, 'clt_dealership_trade_name', true );
    $index_entry['text'][] = get_post_meta( $id, 'clt_dealership_address', true );
    $index_entry['text'][] = ', '.get_post_meta( $id, 'clt_dealership_district', true );
    $index_entry['text'][] = '. '.get_post_meta( $id, 'clt_dealership_city', true );
    $index_entry['text'][] = ' - '.get_post_meta( $id, 'clt_dealership_state', true );
    $index_entry['text'][] = '. CEP: '.substr(get_post_meta( $id, 'clt_dealership_zip_code', true ), 0, 5).'-'.substr(get_post_meta( $id, 'clt_dealership_zip_code', true ), 5, 3);
    $index_entry['text'][] = '. Telefone: '.get_post_meta( $id, 'clt_dealership_phone', true );
    $index_entry['text'][] = '. E-mail: '.get_post_meta( $id, 'clt_dealership_email', true );


    clt_fulltext_search_save( array( $index_entry ) );

}

/**
 * clt_fulltext_search_build_faq_index
 * build the index for 'faq' type pages
 *
 */
function clt_fulltext_search_build_faq_index( $id ) {
    wp_reset_query();
    $dealer = get_post($id);

    $index_entry = array();
    $index_entry['time'] = time();
    $index_entry['url'] = '/servicos/relacionamento-com-o-cliente/perguntas-mais-frequentes/';
    $index_entry['filter'] = 'client';
    $index_entry['section'] = 'perguntas frequentes';
    $index_entry['hash'] = false;
    $index_entry['parent'] = $id;
    $index_entry['priority'] = 21;
    $index_entry['title'] = get_post_meta( $id, 'clt_faq_question', true );
    $index_entry['text'][] = get_post_meta( $id, 'clt_faq_answer', true );

    clt_fulltext_search_save( array( $index_entry ) );

}

/**
 * clt_fulltext_search_build_index
 * generic index builder
 *
 */
function clt_fulltext_search_build_index( $src ) {
    wp_reset_query();
    global $post;

    $doc = new DOMDocument();
    $doc->loadHTML( $src );
    $index_result = array();

    $xpath = new DOMXPath($doc);
    $index = $xpath->query("//*[@data-index='true']");

    $uri = str_replace('?'.$_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI'] );
    
    if($post->post_type == 'car')
        $filter = 'modelos';
    elseif($post->post_type == 'service')
        $filter = 'servicos';
    elseif($post->post_type == 'offer')
        $filter = 'promocao';   
    elseif($post->post_type == 'dealership')
        $filter = 'concessionarias';        
    else
        $filter = 'client'; 

    $i=0;
    foreach($index as $entry) {
        $index_entry = array();
        $index_entry['time'] = time();
        $index_entry['url'] = $uri;
        $index_entry['filter'] = ( $entry->getAttribute('data-index-filter') ? $entry->getAttribute('data-index-filter') : $filter );
        $index_entry['section'] = ( $entry->getAttribute('data-index-section') ? $entry->getAttribute('data-index-section') : $filter );
        $index_entry['hash'] = $entry->getAttribute('id');
        $index_entry['parent'] = $post->ID;

        if( $index_entry['filter'] == 'modelos' ) {

            if( $index_entry['section'] == 'motivos para ter um' )
                $index_entry['priority'] = 02;
            if( $index_entry['section'] == 'versoes' )
                $index_entry['priority'] = 03;
            if( $index_entry['section'] == 'comparativos' ) {
                $index_entry['filter'] = 'acessorios';
                $index_entry['priority'] = 20;
                $index_entry['parent'] = (int) $post->ID.'100';         
                $comparativo = true;
            }
            if( $index_entry['section'] == 'diferenciais' ) {
                $index_entry['filter'] = 'acessorios';
                $index_entry['priority'] = 10;
            }
                
        } elseif( $index_entry['filter'] == 'servicos' ) {
            $index_entry['priority'] = 30;
        } elseif( $index_entry['filter'] == 'promocao' ) {
            $index_entry['priority'] = 60;
        } elseif( $index_entry['filter'] == 'concessionarias' ) {
            $index_entry['priority'] = 50;
        } elseif( $index_entry['filter'] == 'client' ) {

            if( $index_entry['section'] == 'perguntas frequentes' )
                $index_entry['priority'] = 21;
            if( $index_entry['section'] == 'noticias' )
                $index_entry['priority'] = 40;
            if( $index_entry['section'] == 'eventos' )
                $index_entry['priority'] = 41;
            if( $index_entry['section'] == 'mundo client' )
                $index_entry['priority'] = 42;
            if( $index_entry['section'] == 'glossario' )
                $index_entry['priority'] = 61;

        } else {
            $index_entry['priority'] = 70;
        }

        
        $children = $entry->childNodes;

        while($children->length > 1) {
            foreach($children as $child) {
                if($child->nodeType == 1 && $child->hasAttribute('data-index') ) {
                    $data = $child->getAttribute('data-index');
                    if($data == 'text') {
                        $index_entry[$data][] = trim($child->nodeValue);
                    } else {
                        $index_entry[$data] = trim($child->nodeValue);
                    }                       
                }
                    // var_dump($child->getAttribute('data-index'));

                $children = $child->childNodes;
            }
        }

        $i++;
        $index_result[] = $index_entry;
    }
    if( is_singular( 'car' ) && !$comparativo ) {
        $meta_index['priority'] = 01;
        $meta_index['time'] = time();
        $meta_index['url'] = $uri;
        $meta_index['filter'] = $filter;
        $meta_index['hash'] = '';
        $meta_index['parent'] = $post->ID;
        $meta_index['title'] = get_the_title();
        $meta_index['text'] = array( get_the_content() );

        array_unshift($index_result, $meta_index);
    }

    clt_fulltext_search_save($index_result);
    return;
}

/**
 * clt_fulltext_search_save
 * prepares values and saves them to the database
 *
 */
function clt_fulltext_search_save( $index ) {
    global $wpdb;

    date_default_timezone_set('America/Sao_Paulo');

    // Flush results for this page ID (should we do it by the URL instead?)
    $wpdb->query( $wpdb->prepare("DELETE FROM {$wpdb->prefix}index 
                                    WHERE parent = %d", $index[0]['parent']) );

    foreach($index as $entry) {
        extract($entry);

        $wpdb->query( $wpdb->prepare("INSERT INTO 
                                    {$wpdb->prefix}index (modified, url, hash, filter, section, title, text, parent, priority) 
                                    VALUES ( %s, %s, %s, %s, %s, %s, %s, %s, %d )", 
                                    date( "Y-m-d H:i:s", $time ), 
                                    home_url( $url ), 
                                    $hash,
                                    $filter,
                                    $section,
                                    $title, 
                                    join( ' ', $text ),
                                    $parent,
                                    $priority ) );
    }

}

?>
