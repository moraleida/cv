# Hello, this is my résume
## My name is Ricardo Moraleida,
##### I'm a Web Developer and you can hire my services. I'm 31 and based in Rio de Janeiro, Brazil.

##### This is how you can contact me
* E-mail: ricardo@moraleida.me (preferred)
* Skype: ricardomoraleida
* Phone: +55 21 3286 1110
* Mobile: +55 21 983 836 373

## What I do on a day-to-day basis
* Back-end development of websites and services in **WordPress** and plain PHP
* Front-end development of websites and systems, with or without boilerpĺate 
frameworks and javascript libraries.
* Continuous maintenance and development of large websites

## WordPress is my *alma mater*
Development of themes and plugins for WordPress has been my main source of 
income for the past 5 years. It is my *alma mater*, where all of this started, 
and where I always end up returning to.

I have managed and developed WordPress websites for global brands such as 
Whirlpool, Toyota and L'óreal, as well as for brazilian websites of all sizes. 
I have also spoken at WordCamp São Paulo and at the iMasters WordPress Dev 
Conference. I am also an active member on several different WordPress forums 
online.

## Technologies I feel comfortable working in
* PHP (procedural and object oriented)
* Javascript (vanilla and jQuery)
* CSS (also Sass and Compass)
* HTML (html5 and xhtml)
* SQL
* Git
* Linux (GUI and Command-line)

##### Relevant technologies I have worked with but not extensively
* Python
* Subversion

## Languages I feel comfortable working in
* Portuguese (native)
* English (perfectly comfortable)
* Spanish (very comfortable)
* French (somewhat comfortable)

## Work Samples
Please check the folders within this repository for selected code samples. 
Please feel free to ask for more samples, if necessary.

## Wait, but what about university? Where did you study?
Well, long story short: from 2002 to 2005 I studied Communications and majored
in Public Relations from the Catholic University in my home state, Minas Gerais, 
Brazil. Then after that, from 2007 to 2009 I studied Project Management in this
super fancy Business School called Fundação Dom Cabral - FDC.

While that was happening, I worked in various positions inside the 
Communications environment. From 2002 to 2008 I tried journalism, public 
relations, graphic design, marketing, internal communications and 
client & investor relations. I tried media companies, education businesses, NGOs, 
even a Metalworks plant (which was actually pretty cool).

Then came the day in 2008 when I started as a Project Manager for a Web 
Development team of 5. And for two whole years, day in and day out and for every 
awesome project we delivered to our clients, I kept thinking: 
####*"their job is so much cooler than mine!"*

And the rest is history...